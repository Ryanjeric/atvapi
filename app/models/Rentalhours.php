<?php

namespace Models;

use Controllers\ControllerBase as CB;

class Rentalhours extends \Phalcon\Mvc\Model {

    public function initialize() {
    }

    public function groupAndSort() {

    	foreach(Rentalhours::find([order => 'duration, session']) as $key => $r) {

    		$data[$key] = [];

    		// $sessions = Rentalhours_ext::find('rentalhoursid ="' . $r->id . '"');

            $sessions = CB::atvQuery("SELECT *, CASE WHEN starttime LIKE '%AM' THEN 1 ELSE 2 END AS ampm, CASE WHEN starttime LIKE '12%' THEN 1 ELSE 2 END AS twelve FROM rentalhours_ext WHERE rentalhoursid = '" . $r->id . "'ORDER BY ampm, twelve, starttime");

    		foreach($sessions as $s) {
    			array_push($data[$key], [
    				sessionId => $s['id'],
    				starttime => $s['starttime'],
    				endtime => $s['endtime'],
    				session => $r->session,
    				duration => $r->duration,
    				description => $s['description']
    			]); 
    		}
    	}
    	return $data;
    }

    public function sort() {
        
        return CB::atvQuery("SELECT rentalhours.session, rentalhours.duration, rentalhours_ext.*, CASE WHEN rentalhours_ext.starttime LIKE '%AM' THEN 1 ELSE 2 END AS ampm, CASE WHEN rentalhours_ext.starttime LIKE '12%' THEN 1 ELSE 2 END AS twelve FROM rentalhours RIGHT JOIN rentalhours_ext ON rentalhours.id = rentalhours_ext.rentalhoursid ORDER BY rentalhours.duration, rentalhours.session, ampm, twelve, rentalhours_ext.starttime, rentalhours_ext.datecreated DESC");

    }

}
