<?php

/**
 * @author Jete O'Keeffe
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.

$routes[] = [
 	'method' => 'post',
	'route' => '/api/update',
	'handler' => 'myFunction'
];

[. .]

 */

$routes[] = [
	'method' => 'post',
	'route' => '/ping',
	'handler' => ['Controllers\ExampleController', 'pingAction'],
    'authentication' => FALSE
];


$routes[] = [
	'method' => 'get',
	'route' => '/test',
	'handler' => ['Controllers\ExampleController', 'testAction'],
    'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/skip/{name}',
	'handler' => ['Controllers\ExampleController', 'skipAction'],
    'authentication' => FALSE
];
/* Validation for username exist */
$routes[] = [
    'method' => 'get',
    'route' => '/validate/username/{name}',
    'handler' => ['Controllers\UserController', 'userExistAction'],
    'authentication' => FALSE
];
/* Validation for email exist */
$routes[] = [
    'method' => 'get',
    'route' => '/validate/useremail/{email}',
    'handler' => ['Controllers\UserController', 'emailExistAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/user/register',
    'handler' => ['Controllers\UserController', 'registerUserAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/user/add',
    'handler' => ['Controllers\UserController', 'addUserAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/user/activation',
    'handler' => ['Controllers\UserController', 'activationAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/user/login',
    'handler' => ['Controllers\UserController', 'loginAction'],
    'authentication' => FALSE
];

/* List all User */
$routes[] = [
    'method' => 'get',
    'route' => '/user/list/{num}/{off}/{keyword}/{role}',
    'handler' => ['Controllers\UserController', 'userListAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/user/changestatus/{id}/{status}',
    'handler' => ['Controllers\UserController', 'changestatusAction'],
    'authentication' => FALSE
];

/* DELETE User */
$routes[] = [
    'method' => 'get',
    'route' => '/user/delete/{id}',
    'handler' => ['Controllers\UserController', 'deleteUserAction'],
    'authentication' => FALSE
];

//USER IMAGE UPLOAD AND LIST
$routes[] = [
    'method' => 'post',
    'route' => '/user/saveimage',
    'handler' => ['Controllers\UserController', 'saveimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/user/listimages',
    'handler' => ['Controllers\UserController', 'listimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/user/deleteimage/{id}',
    'handler' => ['Controllers\UserController', 'deleteimageAction'],
    'authentication' => FALSE
];




$routes[] = [
    'method' => 'get',
    'route' => '/user/info/{id}',
    'handler' => ['Controllers\UserController', 'userInfoction'],
    'authentication' => FALSE
];



$routes[] = [
    'method' => 'post',
    'route' => '/user/update',
    'handler' => ['Controllers\UserController', 'userUpdateAction'],
    'authentication' => FALSE
];

//SIMULA NG ROTA ng FORGOT PASSWORD
$routes[] = [
    'method' => 'post',
    'route' => '/forgotpassword/send/yo/{email}',
    'handler' => ['Controllers\UserController', 'forgotpasswordAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/checktoken/check/{email}/{token}',
    'handler' => ['Controllers\UserController', 'checktokenAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/updatepassword/token',
    'handler' => ['Controllers\UserController', 'updatepasswordtokenAction'],
    'authentication' => FALSE
];

/**
    Main Page Slider - ROTA ni Ryanjeric
 */

$routes[] = [
    'method' => 'get',
    'route' => '/gallery/list',
    'handler' => ['Controllers\GalleryController', 'galleryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/gallery/saveslider',
    'handler' => ['Controllers\GalleryController', 'uploadimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/gallery/delete/{img}',
    'handler' => ['Controllers\GalleryController', 'deleteimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/gallery/vidlist',
    'handler' => ['Controllers\GalleryController', 'listvideoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/gallery/vidsave',
    'handler' => ['Controllers\GalleryController', 'savevideoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/gallery/deletevideo',
    'handler' => ['Controllers\GalleryController', 'deletevideoAction'],
    'authentication' => FALSE
];

//CONTACT US FE

$routes[] = [
    'method' => 'post',
    'route' => '/fe/contact/send',
    'handler' => ['Controllers\ContactusController', 'sendAction'],
    'authentication' => FALSE
];

//CONTACT US BE

$routes[] = [
    'method' => 'get',
    'route' => '/managecontacts/list/{num}/{off}/{keyword}/{stat}',
    'handler' => ['Controllers\ContactusController', 'listcontactsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/managecontacts/messagedelete/{id}',
    'handler' => ['Controllers\ContactusController', 'messagedeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/managecontacts/listreplies/{id}',
    'handler' => ['Controllers\ContactusController', 'listreplyAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/message/view/{id}',
    'handler' => ['Controllers\ContactusController', 'viewreplyAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/message/reply',
    'handler' => ['Controllers\ContactusController', 'replycontactAction'],
    'authentication' => FALSE
];

//END OF R.I ROUTES//

/*neil routes*/
$routes[] = [
    'method' => 'get',
    'route' => '/news/add/category/{asset}',
    'handler' => ['Controllers\NewsController', 'addcategoryAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/news/list/category/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'listcategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/update/category',
    'handler' => ['Controllers\NewsController', 'updatecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/add/tag/{asset}',
    'handler' => ['Controllers\NewsController', 'addtagAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/list/tags/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'listtagsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/update/tag',
    'handler' => ['Controllers\NewsController', 'updatetagAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/remove/category/{id}',
    'handler' => ['Controllers\NewsController', 'removecategoryAction'],
    'authentication' => FALSE
];
//FREEBIES & Deliver Serv
$routes[] = [
    'method' => 'get',
    'route' => '/freebies/show',
    'handler' => ['Controllers\FreebiesController', 'getMsgAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/freebies/savedata',
    'handler' => ['Controllers\FreebiesController', 'saveAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/deliver/show',
    'handler' => ['Controllers\FreebiesController', 'getMsg1Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/deliver/savedata',
    'handler' => ['Controllers\FreebiesController', 'save1Action'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/special/show',
    'handler' => ['Controllers\FreebiesController', 'getMsg2Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/special/savedata',
    'handler' => ['Controllers\FreebiesController', 'save2Action'],
    'authentication' => FALSE
];
//REQUIREMENTS

$routes[] = [
    'method' => 'post',
    'route' => '/requirements/save',
    'handler' => ['Controllers\RequirementsController', 'saveAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/requirements/list/{num}/{off}/{key}',
    'handler' => ['Controllers\RequirementsController', 'listreqAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/requirements/felist',
    'handler' => ['Controllers\RequirementsController', 'felistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/requirements/update',
    'handler' => ['Controllers\RequirementsController', 'updateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/remove/tag/{id}',
    'handler' => ['Controllers\NewsController', 'removetagAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/requirements/delete/{id}',
    'handler' => ['Controllers\RequirementsController', 'deleteAction'],
    'authentication' => FALSE
];
//RENTAL

$routes[] = [
    'method' => 'post',
    'route' => '/rentalhours/savedata',
    'handler' => ['Controllers\RentalhoursController', 'saveAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/rental/total/{start}/{end}',
    'handler' => ['Controllers\RentalhoursController', 'computetotalAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/rental/list/{num}/{off}/{key}',
    'handler' => ['Controllers\RentalhoursController', 'listrAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/rental/delete/{id}',
    'handler' => ['Controllers\RentalhoursController', 'deleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/rental/edit/{id}',
    'handler' => ['Controllers\RentalhoursController', 'editAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/rental/update',
    'handler' => ['Controllers\RentalhoursController', 'updateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/rentalmsg/show',
    'handler' => ['Controllers\RentalhoursController', 'getMsgAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/rentalmsg/savedata',
    'handler' => ['Controllers\RentalhoursController', 'savemsgAction'],
    'authentication' => FALSE
];

//ATV IMAGE UPLOAD AND LIST
$routes[] = [
    'method' => 'post',
    'route' => '/atv/saveimage',
    'handler' => ['Controllers\AtvpricesController', 'saveimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/atv/listimages',
    'handler' => ['Controllers\AtvpricesController', 'listimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/atv/deleteimage/{id}',
    'handler' => ['Controllers\AtvpricesController', 'deleteimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/atv/listrentals',
    'handler' => ['Controllers\AtvpricesController', 'listrentalsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/atv/listrentalhours',
    'handler' => ['Controllers\AtvpricesController', 'listrentalhoursAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/atv/savedata',
    'handler' => ['Controllers\AtvpricesController', 'saveAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/atv/felist',
    'handler' => ['Controllers\AtvpricesController', 'feInfoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/atv/fefilterlist/{rentalhoursid}',
    'handler' => ['Controllers\AtvpricesController', 'filter_atv_if_it_has_priceAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/atv/reservation/{id}',
    'handler' => ['Controllers\AtvpricesController', 'feInfoResAction'],
    'authentication' => FALSE
];

//MANAGE ATV
$routes[] = [
    'method' => 'get',
    'route' => '/atv/list/{num}/{off}/{key}',
    'handler' => ['Controllers\AtvpricesController', 'ListAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/atv/delete/{id}',
    'handler' => ['Controllers\AtvpricesController', 'deleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/atv/info/{id}',
    'handler' => ['Controllers\AtvpricesController', 'Infoction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/atv/update',
    'handler' => ['Controllers\AtvpricesController', 'UpdateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/author/uploadimage',
    'handler' => ['Controllers\NewsController', 'authoruploadimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/author/images',
    'handler' => ['Controllers\NewsController', 'authorimagesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/author/create',
    'handler' => ['Controllers\NewsController', 'authorcreateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/validation/author/uniquename',
    'handler' => ['Controllers\NewsController', 'validationauthoruniquenameAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/authors/pagination',
    'handler' => ['Controllers\NewsController', 'authorspaginationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/author/delete',
    'handler' => ['Controllers\NewsController', 'authordeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/atv/getprice/{atvid}/{rentalid}',
    'handler' => ['Controllers\AtvpricesController', 'fegetpriceAction'],
    'authentication' => FALSE
];

//Trails

$routes[] = [
    'method' => 'post',
    'route' => '/trails/savedata',
    'handler' => ['Controllers\TrailsController', 'saveAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/trails/list/{num}/{off}/{key}',
    'handler' => ['Controllers\TrailsController', 'ListAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/trails/delete/{id}',
    'handler' => ['Controllers\TrailsController', 'deleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/trails/felist',
    'handler' => ['Controllers\TrailsController', 'feInfoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/trails/info/{id}',
    'handler' => ['Controllers\TrailsController', 'Infoction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/trails/update',
    'handler' => ['Controllers\TrailsController', 'UpdateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/reservation/savedata',
    'handler' => ['Controllers\ReservationController', 'saveAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservation/getevent/{atvid}',
    'handler' => ['Controllers\ReservationController', 'listeventAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservation/belist/{num}/{page}/{keyword}',
    'handler' => ['Controllers\ReservationController', 'ListAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservation/getreservationbe',
    'handler' => ['Controllers\ReservationController', 'getreservationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservation/viewreservation/{customerid}',
    'handler' => ['Controllers\ReservationController', 'viewreservationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservation/changestatus1/{id}',
    'handler' => ['Controllers\ReservationController', 'changestatus1Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservation/changestatus2/{id}',
    'handler' => ['Controllers\ReservationController', 'changestatus2Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservation/changestatus3/{id}',
    'handler' => ['Controllers\ReservationController', 'changestatus3Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservation/changestatus4/{id}',
    'handler' => ['Controllers\ReservationController', 'changestatus4Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/author/load',
    'handler' => ['Controllers\NewsController', 'authorloadAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/author/ensureuniquename',
    'handler' => ['Controllers\NewsController', 'authorensureuniquenameAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/author/update',
    'handler' => ['Controllers\NewsController', 'authorupdateAction'],
    'authentication' => FALSE
];




///////////////////////DUMMY RESERVATION ///////////////////////////////////

$routes[] = [
    'method' => 'get',
    'route' => '/news/create/resources',
    'handler' => ['Controllers\NewsController', 'newscreateresourcesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/loadcategories',
    'handler' => ['Controllers\NewsController', 'newsloadcategoriesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/loadtags',
    'handler' => ['Controllers\NewsController', 'newsloadtagsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/reservation/savedata1',
    'handler' => ['Controllers\ReservationController', 'save1Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservation/belist1/{num}/{page}/{keyword}',
    'handler' => ['Controllers\ReservationController', 'List1Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/uploadimage',
    'handler' => ['Controllers\NewsController', 'newsuploadimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/images',
    'handler' => ['Controllers\NewsController', 'newsimagesAction'],
		'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservation/status0/{id}',
    'handler' => ['Controllers\ReservationController', 'status1Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/deleteimage',
    'handler' => ['Controllers\NewsController', 'newsdeleteimageAction'],
		'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservation/status1/{id}',
    'handler' => ['Controllers\ReservationController', 'status2Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/saveembed',
    'handler' => ['Controllers\NewsController', 'newssaveembedAction'],
		'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservation/status2/{id}',
    'handler' => ['Controllers\ReservationController', 'status3Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/videos',
    'handler' => ['Controllers\NewsController', 'newsvideosAction'],
		'authentication' => FALSE
];

$routes[] = [
	  'method' => 'get',
    'route' => '/reservation/status3/{id}',
    'handler' => ['Controllers\ReservationController', 'status4Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/deletevideo',
    'handler' => ['Controllers\NewsController', 'newsdeletevideoAction'],
		'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservation/status4/{id}',
    'handler' => ['Controllers\ReservationController', 'status5Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/create',
    'handler' => ['Controllers\NewsController', 'newscreateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservation/viewreservation1/{customerid}',
    'handler' => ['Controllers\ReservationController', 'viewreservation1Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/validation/news/uniqueslugs',
    'handler' => ['Controllers\NewsController', 'validationnewsuniqueslugsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/pagination',
    'handler' => ['Controllers\NewsController', 'newspaginationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/delete',
    'handler' => ['Controllers\NewsController', 'newsdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/info',
    'handler' => ['Controllers\NewsController', 'newsinfoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/ensureuniqueslugs',
    'handler' => ['Controllers\NewsController', 'newsensureuniqueslugsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/saveupdate',
    'handler' => ['Controllers\NewsController', 'newssaveupdateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/author/deleteimage',
    'handler' => ['Controllers\NewsController', 'authordeleteimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/blog/view/{newsslugs}',
    'handler' => ['Controllers\NewsController', 'feblogviewbynewsslugsAction'],
    'authentication' => FALSE
];

////NEWS/blog FRONTEND
$routes[] = [
    'method' => 'get',
    'route' => '/blog/listall/{id}/{archieve}/{tags}',
    'handler' => ['Controllers\NewsController', 'felistallnewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/blog/listarchieve',
    'handler' => ['Controllers\NewsController', 'listarchiveAction'],
    'authentication' => FALSE
];

///////////METADATA
$routes[] = [
    'method' => 'get',
    'route' => '/metadata/list/{num}/{page}/{keyword}',
    'handler' => ['Controllers\MetadataController', 'ListAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/blog/author/{authorid}/{items}',
    'handler' => ['Controllers\NewsController', 'feblogauthorAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/metadata/edit/{id}',
    'handler' => ['Controllers\MetadataController', 'editAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/metadata/update',
    'handler' => ['Controllers\MetadataController', 'updateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/home/blogs',
    'handler' => ['Controllers\NewsController', 'homeblogsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/metatrails/list/{num}/{page}/{keyword}',
    'handler' => ['Controllers\TrailsController', 'METAListAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/metatrails/edit/{id}',
    'handler' => ['Controllers\TrailsController', 'METAeditAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/metatrails/update',
    'handler' => ['Controllers\TrailsController', 'METAupdateAction'],
    'authentication' => FALSE
];


///Trails

$routes[] = [
    'method' => 'get',
    'route' => '/trails/fecatinfo/{cat}',
    'handler' => ['Controllers\TrailsController', 'fecatinfoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/trail/infobyslugs/{slugs}',
    'handler' => ['Controllers\TrailsController', 'fetrailinfobyslugsAction'],
    'authentication' => FALSE
];

/**
    Settings - Simula ng ROTA ni Ryanjeric
 */

$routes[] = [
    'method' => 'get',
    'route' => '/settings/managesettings',
    'handler' => ['Controllers\SettingsController', 'managesettingsAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/settings/maintenanceon',
    'handler' => ['Controllers\SettingsController', 'maintenanceonAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/settings/maintenanceoff',
    'handler' => ['Controllers\SettingsController', 'maintenanceoffAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/settings/uploadlogo',
    'handler' => ['Controllers\SettingsController', 'uploadlogoAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/settings/logolist',
    'handler' => ['Controllers\SettingsController', 'listlogoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/settings/delete/{img}',
    'handler' => ['Controllers\SettingsController', 'deletelogoAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/settings/savedefaultlogo',
    'handler' => ['Controllers\SettingsController', 'savedefaultlogoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/settings/googleanalytics',
    'handler' => ['Controllers\SettingsController', 'scriptAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/settings/loadscript',
    'handler' => ['Controllers\SettingsController', 'loadscriptAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/settings/script',
    'handler' => ['Controllers\SettingsController', 'displaytAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/blog/preview',
    'handler' => ['Controllers\NewsController', 'previewAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/author/attemptdelete',
    'handler' => ['Controllers\NewsController', 'authorattemptdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/changestatus',
    'handler' => ['Controllers\NewsController', 'newschangestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/adventure/show',
    'handler' => ['Controllers\GalleryController', 'getMsg1Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/adventure/savedata',
    'handler' => ['Controllers\GalleryController', 'save1Action'],
    'authentication' => FALSE
];

//ReservationnoticeController

$routes[] = [
    'method' => 'post',
    'route' => '/reservationnotice/save',
    'handler' => ['Controllers\ReservationnoticeController', 'saveAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservationnotice/list/{num}/{off}/{key}',
    'handler' => ['Controllers\ReservationnoticeController', 'listAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservationnotice/felist',
    'handler' => ['Controllers\ReservationnoticeController', 'felistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/reservationnotice/update',
    'handler' => ['Controllers\ReservationnoticeController', 'updateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/reservationnotice/delete/{id}',
    'handler' => ['Controllers\ReservationnoticeController', 'deleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/latest/rss',
    'handler' => ['Controllers\NewsController', 'rssfeedAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updateauthor',
    'handler' => ['Controllers\NewsController', 'newsupdateauthorAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/category/attemptdelete',
    'handler' => ['Controllers\NewsController', 'categoryattemptdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatecategory',
    'handler' => ['Controllers\NewsController', 'newsupdatecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/tag/attemptdelete',
    'handler' => ['Controllers\NewsController', 'tagattemptdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatetag',
    'handler' => ['Controllers\NewsController', 'newsupdatetagAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/rentalhours/sessions',
    'handler' => ['Controllers\RentalhoursController', 'sessionsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/rentalhours/sessionduration',
    'handler' => ['Controllers\RentalhoursController', 'sessiondurationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/validation/trail/uniqueslugs',
    'handler' => ['Controllers\TrailsController', 'validationtrailuniqueslugsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/trail/ensureuniqueslugs',
    'handler' => ['Controllers\TrailsController', 'trailensureuniqueslugsAction'],
    'authentication' => FALSE
];

// page details
$routes[] = [
    'method' => 'get',
    'route' => '/pagedetails/getcontact',
    'handler' => ['Controllers\PageController', 'getcontactAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pagedetails/update',
    'handler' => ['Controllers\PageController', 'updatecontactinfoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pagedetails/imglist',
    'handler' => ['Controllers\PageController', 'reservationimglistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pagedetails/deleteimage/{imgid}',
    'handler' => ['Controllers\PageController', 'deleteimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pagedetails/saveimage',
    'handler' => ['Controllers\PageController', 'saveimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pagedetails/gethome',
    'handler' => ['Controllers\PageController', 'gethomeAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pagedetails/updatehomeimg/{no}',
    'handler' => ['Controllers\PageController','updatehomeimgAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pagedetails/getbanner',
    'handler' => ['Controllers\PageController', 'getbannerAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pagedetails/banner/imglist',
    'handler' => ['Controllers\PageController', 'bannerlistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pagedetails/banner/deleteimage/{id}',
    'handler' => ['Controllers\PageController', 'deletebannerAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post', 
    'route' => '/pagedetails/savebanner',
    'handler' => ['Controllers\PageController', 'savebannerAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pagedetails/updatebanner',
    'handler' => ['Controllers\PageController', 'updatepagebannerAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/gethome',
    'handler' => ['Controllers\PageController', 'getfehomeAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/getbanner/{page}',
    'handler' => ['Controllers\PageController', 'getfebannerAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/get/atv/sessions',
    'handler' => ['Controllers\AtvpricesController', 'getAtvSessions'],
    'authentication' => FALSE
];

return $routes;
