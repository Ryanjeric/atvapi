<?php

namespace Controllers;
use \Models\Customer as Customer;
use \Models\Reservationlist as Reservationlist;
use \Models\Atvprices as Atvprices;
use \Models\Atvrentalprices as Atvrentalprices;
use \Models\Customerlist as Customerlist;
use \Models\Customersatv as Customersatv;
use \Models\Rentalhours as Rentalhours;
use \Models\Rentalhours_ext as Rentalhours_ext;
use Models\Contactinfo;
use \Controllers\ControllerBase as CB;
use PHPMailer as PHPMailer;

class ReservationController extends \Phalcon\Mvc\Controller {

     public function saveAction(){
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $details = $request->getPost('customer');
            $reslist = $request->getPost('reslist');
            $invoiceno = hexdec(substr(uniqid(),0,9));
            $guid = new \Utilities\Guid\Guid();
            $id = $guid->GUID();
            $usave = new Customer();
            $usave->id = $id;
            $usave->invoiceno = $invoiceno;
            $usave->fname = $details['fname'];
            $usave->lname = $details['lname'];
            $usave->phonenum = $details['phonenum'];
            $usave->email = $details['email'];
            $usave->numpeople = $details['numpeople'];
            $usave->numpeopledrive = $details['numpeopledrive'];
            $usave->kids = $details['kids'];
            $usave->request = $details['request'];
            $usave->deliver = $details['deliver'];

            if (!$usave->save()) {
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data['error'] =  $errors;
            } else {
                $list = array();
                $list = $reslist;

                foreach($list as $key => $value){
                    $save = new Reservationlist();
                    $save->assign(array(
                        'customerid' => $id,
                        'atvid' => $list[$key]['atvid'],
                        'rentalid' => $list[$key]['rentalid'],
                        'reservationdate' => $list[$key]['datereserve'],
                        'status' =>  0,
                        'datecreated' =>  date('Y-m-d H:i:s'),
                        'dateupdated' =>  date('Y-m-d H:i:s'),
                        ));
                    if (!$save->save()) {
                        $errors = array();
                        foreach ($save->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        $data['error'] =  $errors;
                    } else {

                        $data['success'] = "Success";
                    }
                }
            }
        }
    }

    public function listeventAction($atvid){
        $list = Reservationlist::find("atvid='".$atvid."' AND status=1");
        $count = count($list);

        if($count == 0){
            $data = 0;
        }else{
            foreach ($list as $li)
            {
                $data[] = array(
                    'atvid'=>$li->atvid,
                    'rentalid'=>$li->rentalid,
                    'reservationdate' =>$li->reservationdate
                    );
            }
        }


        echo json_encode($data);
    }

    public function ListAction($num, $page, $keyword) {
        $offsetfinal = ($page * 10) - 10;
        $db = \Phalcon\DI::getDefault()->get('db');
        if($keyword == undefined || $keyword == null){
          $stmt = $db->prepare("SELECT * FROM customer LEFT JOIN reservationlist ON customer.id = reservationlist.customerid LEFT JOIN  atvprices on reservationlist.atvid = atvprices.id ORDER BY reservationlist.datecreated DESC LIMIT " . $offsetfinal . ",10");
          $db1 = \Phalcon\DI::getDefault()->get('db');
          $stmt1 = $db1->prepare("SELECT * FROM customer LEFT JOIN reservationlist ON customer.id = reservationlist.customerid LEFT JOIN atvprices on reservationlist.atvid = atvprices.id");
        }else{
           $stmt = $db->prepare("SELECT * FROM customer LEFT JOIN reservationlist ON customer.id = reservationlist.customerid WHERE
            customer.fname LIKE '%" . $keyword . "%'
            or customer.lname LIKE '%" . $keyword. "%'
            or customer.email LIKE '%" . $keyword. "%'
            or customer.invoiceno LIKE '%". $keyword ."%'
            or reservationlist.reservationdate LIKE '%" . $keyword. "%'
               ORDER BY reservationlist.datecreated DESC LIMIT " . $offsetfinal . ",10");
           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT * FROM customer LEFT JOIN reservationlist ON customer.id = reservationlist.customerid WHERE
            customer.fname LIKE '%" . $keyword . "%'
            or customer.lname LIKE '%" . $keyword. "%'
            or customer.email LIKE '%" . $keyword. "%'
            or reservationlist.reservationdate LIKE '%" . $keyword. "%'");
        }

        $stmt->execute();
        $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $stmt1->execute();
        $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);
        $totalreportdirty = count($searchresult1);

        echo json_encode(array('list' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty));
    }

    public function getreservationAction(){
        $db1 = \Phalcon\DI::getDefault()->get('db');
        $stmt1 = $db1->prepare("SELECT * FROM reservationlist
            LEFT JOIN customer ON reservationlist.customerid = customer.id
            LEFT JOIN atvprices on reservationlist.atvid = atvprices.id");
        $stmt1->execute();
        $searchresult = $stmt1->fetchAll(\PDO::FETCH_ASSOC);
        $count = count($searchresult);
        if($count == 0){
            $data = 0;
        }else{
            $data = $searchresult;
        }

        echo json_encode($data);
    }


    public function viewreservationAction($invoiceno){
        $customer = Customer::findFirst('invoiceno="'. $invoiceno .'"');


        $data = array(
            'customername' => $customer->fname .' '. $customer->lname,
            'phone'        => $customer->phonenum,
            'email'        => $customer->email,
            'numpeople'    => $customer->numpeople,
            'numpdrive'    => $customer->numpeopledrive,
            'kids'         => $customer->kids,
            'request'      => $customer->request,
            'deliver'      => $customer->deliver,
            'invoiceno'    => $customer->invoiceno,
        );

        $reservation = Reservationlist::find('customerid="'.$customer->id.'"');

        foreach ($reservation as $li)
        {
           $atvprice = Atvprices::findFirst('id="'.$li->atvid.'"');
           $rental = Atvrentalprices::findFirst('atvid="'.$li->atvid.'" AND rentalid="'.$li->rentalid.'"');

           $data2[] = array(
            'reservationid'   => $li->id,
            'atv'             => $atvprice->title,
            'price'           => $rental->price,
            'reservationdate' => $li->reservationdate,
            'datecreated'     => $li->datecreated,
            'status'          => $li->status
            );
        }
        echo json_encode(array('data'=>$data,'data2'=>$data2));
    }

    public function changestatus1Action($id){
        $getInfo = Reservationlist::findFirst('id="'. $id .'"');
        $getInfo->status = 0;
        $getInfo->save();
        $data=array('success' => 'New');
        echo json_encode($data);
    }
    public function changestatus2Action($id){
        $getInfo = Reservationlist::findFirst('id="'. $id .'"');
        $getInfo->status = 1;
        $getInfo->save();
        $data=array('success' => 'Reserved');
        echo json_encode($data);
    }
    public function changestatus3Action($id){
        $getInfo = Reservationlist::findFirst('id="'. $id .'"');
        $getInfo->status = 2;
        $getInfo->save();
        $data=array('success' => 'Done');
        echo json_encode($data);
    }
    public function changestatus4Action($id){
        $getInfo = Reservationlist::findFirst('id="'. $id .'"');
        $getInfo->status = 3;
        $getInfo->save();
        $data=array('success' => 'Void');
        echo json_encode($data);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////


    public function save1Action(){
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $webInfo = Contactinfo::findFirst();

            $details = $request->getPost('customer');

            $invoiceno = hexdec(substr(uniqid(),0,9));

            $guid = new \Utilities\Guid\Guid();
            $id = $guid->GUID();
            $usave = new Customerlist();
            $usave->id = $id;
            $usave->fname = $details['fname'];
            $usave->lname = $details['lname'];
            $usave->phonenumber = $details['phonenum'];
            $usave->email = $details['email'];
            $usave->rentaldate = $details['rentaldate'];
            $usave->duration = $details['rental'];
            $usave->numpeople = $details['numpeople'];
            $usave->numpeopledrive = $details['numpeopledrive'];
            $usave->kids = $details['kids'];
            $usave->request = $details['request'];
            $usave->deliver = $details['deliver'];
            $usave->cardtype = $details['cardtype'];
            $usave->cardnum = $details['cardnum'];
            $usave->cardexp = $details['cardexp'];
            $usave->securitycode = $details['securitycode'];
            $usave->invoiceno = $invoiceno;
            $usave->status = 0;
            $usave->datecreated = date('Y-m-d H:i:s');

            if (!$usave->save()) {
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data['error'] =  $errors;
            } else {
                $atv = array();
                $atv = $details['atv'];

                foreach($atv as $key => $value){
                    if($atv[$key] != 'false') {

                        $atvprice = Atvprices::findFirst("id='" . $atv[$key] . "'");
                        $atvselected .= $atvprice->title. "<br>";

                        $save = new Customersatv();
                        $save->assign(array(
                            'customerid' => $id,
                            'atvselected' =>  $atv[$key]
                            ));
                        if (!$save->save()) {
                            $errors = array();
                            foreach ($save->getMessages() as $message) {
                                $errors[] = $message->getMessage();
                            }
                            $data['error'] =  $errors;
                        } else {

                            $data['success'] = "Success";
                        }
                    }
                }

                // $duration = Rentalhours::findFirst('id="'.$details['rental'].'"');
                $query = "SELECT rentalhours.session, rentalhours.duration, rentalhours_ext.* FROM rentalhours LEFT JOIN rentalhours_ext ON rentalhours.id = rentalhours_ext.rentalhoursid WHERE rentalhours_ext.id = '".$details['rental']."'";
                $duration = CB::atvQueryFirst($query);
                $dc = new CB();
                // $hour = $dc->timeconverter($duration->starttime,$duration->endtime);
                $hour = $duration['duration'] . " HR";
                $body = '<div style="background-color: #eee;padding:20px;margin-bottom:10px;">

                <br>___________________________________________<br><br>

                Thank you for choosing Vortex Healing ATV Rental.<br>
                We are processing your reservation and will send you a confirmation email once this is completed.<br><br>

                You reservation request is:<br><br>

                Name: '.$details['fname'].' '.$details['lname'].'<br><br>

                Rental Date: '.$details['rentaldate'].'<br>
                Duration: '.$duration['session'].': '.$duration['starttime'].'-'.$duration['endtime'].'('.$hour.')<br>
                Vehicle: <br>'.$atvselected.'<br>

                People will be in the vehicle : '.$details['numpeople'].'<br>
                People will drive : '.$details['numpeopledrive'].'<br>
                Kids : '.$details['kids'].'<br>
                Special Request : '. $details['request'].'<br>
                Delivery Service : '.$details['deliver'].'<br><br>

                For more concerns please contact us immediately: <br>
                Phone: ' . $webInfo->contactno . ' <br>
                Email: ' . $webInfo->email . ' <br><br>

                Best Regards,<br><br>

                Vortex Healing ATV Rental<br><br>
                <br>
                ___________________________________________
                <br>
                <small>NOTE: do not reply to this message</small>
                </div>';

                $message = '<div style="background-color: #eee;padding:20px;margin-bottom:10px;">
                Someone reserve at this day ('.$details['rentaldate'].')<br><br>

                Invoiceno : '.$invoiceno.'<br>
                Name : '.$details['fname'].' '.$details['lname'].'<br>
                Phone number : '.$details['phonenum'].'<br>
                Email : '.$details['email'].'<br>
                Rental Date : '.$details['rentaldate'].'<br>
                Duration : '.$duration['session'].': '.$duration['starttime'].'-'.$duration['endtime'].'('.$hour.')<br>
                <br>
                ATV<br>'.$atvselected.'<br>
                People will be in the vehicle : '.$details['numpeople'].'<br>
                People will drive : '.$details['numpeopledrive'].'<br>
                Kids : '.$details['kids'].'<br>
                Request : '.$details['request'].'<br>
                Delivery Service : '.$details['deliver'].'<br><br>
                ___________________________________________<br>

                <strong>Credit Card info</strong><br><br>
                Credit Card type : '.strtoupper($details['cardtype']).'<br>
                Credit card number : '.$details['cardnum'].'<br>
                Expiration date : '.$details['cardexp'].'<br>
                Security code : '.$details['securitycode'].'<br>
                </div>';

                CB::sendMail($webInfo->email, 'Vortex Healing ATV', $message); //email to admin
                CB::sendMail($details['email'], 'Vortex Healing ATV', $body); //email to customer

                // $sendtoadmin = new PHPMailer();

                // $sendtoadmin->isSMTP();
                // $sendtoadmin->Host = 'smtp.mandrillapp.com';
                // $sendtoadmin->SMTPAuth = true;
                // $sendtoadmin->Username = 'efrenbautistajr@gmail.com';
                // $sendtoadmin->Password = '6Xy18AJ15My59aQNTHE5kA';
                //         //$mail->SMTPSecure = 'ssl';
                // $sendtoadmin->Port = 587;
                // $sendtoadmin->From = 'rioverdeatv@gmail.com';
                // $sendtoadmin->FromName = 'rioverdeatv@gmail.com';
                // $sendtoadmin->addAddress('rioverdeatv@gmail.com');
                // $sendtoadmin->isHTML(true);
                // $sendtoadmin->Subject = 'Rio Verde ATV';
                // $sendtoadmin->Body = $message;

                // $sendtocustomer = new PHPMailer();

                // $sendtocustomer->isSMTP();
                // $sendtocustomer->Host = 'smtp.mandrillapp.com';
                // $sendtocustomer->SMTPAuth = true;
                // $sendtocustomer->Username = 'efrenbautistajr@gmail.com';
                // $sendtocustomer->Password = '6Xy18AJ15My59aQNTHE5kA';
                //         //$mail->SMTPSecure = 'ssl';
                // $sendtocustomer->Port = 587;
                // $sendtocustomer->From = 'rioverdeatv@gmail.com';
                // $sendtocustomer->FromName = 'rioverdeatv@gmail.com';
                // $sendtocustomer->addAddress($details['email']);
                // $sendtocustomer->isHTML(true);
                // $sendtocustomer->Subject = 'Rio Verde ATV';
                // $sendtocustomer->Body = $body;

                // if (!$sendtoadmin->send()) {
                //     $data = array('error' => $sendtoadmin->ErrorInfo);
                // }
                // elseif (!$sendtocustomer->send()) {
                //      $data = array('error' => $sendtocustomer->ErrorInfo);
                // }
                // else {
                //     $data['success'] ="SAVE";
                // }
           }
        }else{
            $data['error'] =  "NO POST DATA";
        }
        echo json_encode($data);
    }



    public function List1Action($num, $page, $keyword) {
        $offsetfinal = ($page * 10) - 10;
        $db = \Phalcon\DI::getDefault()->get('db');
        if($keyword == undefined || $keyword == null){
          $stmt = $db->prepare("SELECT * FROM customerlist ORDER BY datecreated DESC LIMIT " . $offsetfinal . ",10");
          $db1 = \Phalcon\DI::getDefault()->get('db');
          $stmt1 = $db1->prepare("SELECT * FROM customerlist");
        }else{
           $stmt = $db->prepare("SELECT * FROM customerlist WHERE
            fname LIKE '%" . $keyword . "%'
            or lname LIKE '%" . $keyword. "%'
            or email LIKE '%" . $keyword. "%'
            or invoiceno LIKE '%" . $keyword. "%'
            ORDER BY datecreated DESC LIMIT " . $offsetfinal . ",10");
           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT * FROM customerlist WHERE
            fname LIKE '%" . $keyword . "%'
            or lname LIKE '%" . $keyword. "%'
            or email LIKE '%" . $keyword. "%'
            or invoiceno LIKE '%" . $keyword. "%'");
        }

        $stmt->execute();
        $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $stmt1->execute();
        $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);
        $totalreportdirty = count($searchresult1);

        echo json_encode(array('list' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty));
    }


     public function status1Action($id){
        $getInfo = Customerlist::findFirst('id="'. $id .'"');
        $getInfo->status = 0;
        $getInfo->save();
        $data=array('success' => 'New');
        echo json_encode($data);
    }
    public function status2Action($id){
        $getInfo = Customerlist::findFirst('id="'. $id .'"');
          $duration = Rentalhours_ext::findFirst("id = '".$getInfo->duration. "'");
          $session = Rentalhours::findFirst("id = '".$duration->rentalhoursid."'");
            $sessionid = $session->id;
            $sessionname = $session->session;

        $getatv = Customersatv::find('customerid="'.$id.'"');

        $dc = new CB();

        // $duration = Rentalhours::findFirst('id="'.$getInfo->duration.'"');
        // $duration = Rentalhours::findFirst('id="'.$session.'"');
        // $hour = $dc->timeconverter($duration->starttime,$duration->endtime);
        $hour = $session->duration;

        $totalprice = 0;
        foreach ($getatv as $atv) {
            $atvprice = Atvprices::findFirst("id='".$atv->atvselected."'");
            $atvselected .= $atvprice->title. "<br>";

            // $Atvrentalprices = Atvrentalprices::findFirst('atvid="'.$atv->atvselected.'" AND rentalid="'.$getInfo->duration.'"');
            $Atvrentalprices = Atvrentalprices::findFirst('atvid="'.$atv->atvselected.'" AND rentalid="'.$sessionid.'"');

            $totalprice += $Atvrentalprices->price;

        }
        $countdeposit = count($getatv) * 20;
        $tax = $totalprice * 0.0935;
        $Balance = $tax + $totalprice + $countdeposit;
        $getInfo->status = 1;
        $getInfo->save();
        $data=array('success' => 'RESERVED');

        $dc = new CB();
        $body = '<div style="background-color: #eee;padding:20px;margin-bottom:10px;">
        Your reservation is now confirmed.<br><br>

        Your reservation details is:<br><br>

        Confirmation No.: '.$getInfo->invoiceno.'<br>
        Name:'.$getInfo->fname.' '.$getInfo->lname.'<br><br>

        Rental Date:'.$getInfo->rentaldate.'<br>
        Duration: '.$sessionname.': '.$duration->starttime.'-'.$duration->endtime.'('.$hour.')Hr<br>
        Vehicle:<br>'.$atvselected.'<br><br>

        People will be in the vehicle : '.$getInfo->numpeople.'<br>
        People will drive : '.$getInfo->numpeopledrive.'<br>
        Kids : '.$getInfo->kids.'<br>
        Special Request : '.$getInfo->request.'<br>
        Delivery Service : '.$getInfo->deliver.'<br>
        <br><br>
        Your Payment Summary<br><br>
        Rental Rate: $'.number_format($totalprice, 2).'<br>
        Tax: $'.number_format($tax,2).'<br>
        Reservation Deposit: $'.$countdeposit.'.00<br>
        Your Balance: $'.number_format($Balance,2).'<br><br>
        * Reservation/Cancellation Guidelines<br><br>

        To guarantee your ATV spot, $'.$countdeposit.' of deposit is charged to your credit card when your reservation is made.<br><br>

        2 days cancellation notice is required. Notify us by 4 p.m. two days before your scheduled riding. Otherwise, $100 of cancellation fee will be charged.<br><br>

        Please check in 30 minutes before your riding for paperwork and orientation.<br><br>

        We look forward to seeing you.<br><br>

        Best Regards,<br><br>

        Vortex Healing ATV
        </div>';

        CB::sendMail($getInfo->email, 'Vortex Healing ATV', $body); //email to customer
        // $sendtocustomer = new PHPMailer();

        // $sendtocustomer->isSMTP();
        // $sendtocustomer->Host = 'smtp.mandrillapp.com';
        // $sendtocustomer->SMTPAuth = true;
        // $sendtocustomer->Username = 'efrenbautistajr@gmail.com';
        // $sendtocustomer->Password = '6Xy18AJ15My59aQNTHE5kA';
        //                 //$mail->SMTPSecure = 'ssl';
        // $sendtocustomer->Port = 587;
        // $sendtocustomer->From = 'rioverdeatv@gmail.com';
        // $sendtocustomer->FromName = 'rioverdeatv@gmail.com';
        // $sendtocustomer->addAddress($getInfo->email);
        // $sendtocustomer->isHTML(true);
        // $sendtocustomer->Subject = 'Rio Verde ATV';
        // $sendtocustomer->Body = $body;

        // if (!$sendtocustomer->send()) {
        //     $data = array('error' => $sendtocustomer->ErrorInfo);
        // } else {
        //     $data['success'] ="SAVE";
        // }

        echo json_encode($data);
    }
    public function status3Action($id){
        $getInfo = Customerlist::findFirst('id="'. $id .'"');
        $getInfo->status = 2;
        $getInfo->save();
        $data=array('success' => 'Done');
        echo json_encode($data);
    }
    public function status4Action($id){
        $getInfo = Customerlist::findFirst('id="'. $id .'"');
        $getInfo->status = 3;
        $getInfo->save();
        $data=array('success' => 'Void');
        echo json_encode($data);
    }

    public function status5Action($id){
        $getInfo = Customerlist::findFirst('id="'. $id .'"');
        $getInfo->status = 4;
        $getInfo->save();
        $data=array('success' => 'Unattended');
        echo json_encode($data);
    }


    public function viewreservation1Action($id){
        $customer = Customerlist::findFirst('id="'. $id .'"');
        $dc = new CB();

        // $duration = Rentalhours::findFirst('id="'.$customer->duration.'"');
        // $hour = $dc->timeconverter($duration->starttime,$duration->endtime);
        $query = "SELECT rentalhours_ext.*, rentalhours.duration, rentalhours.session FROM rentalhours_ext LEFT JOIN rentalhours ON rentalhours_ext.rentalhoursid = rentalhours.id WHERE rentalhours_ext.id = '".$customer->duration."'";
        $session = CB::atvQueryFirst($query);
        $duration = $session['session'] . ": " .$session['starttime']. " - " .$session['endtime']. " " .$session['duration']. "(HR)";

        // 'duration'     => $duration->session.': '.$duration->starttime.'-'.$duration->endtime.'('.$hour.')Hr',

        $data = array(
            'invoiceno'    => $customer->invoiceno,
            'customername' => $customer->fname .' '. $customer->lname,
            'phone'        => $customer->phonenumber,
            'email'        => $customer->email,
            'rentaldate'   => $customer->rentaldate,
            'duration'     => $duration,
            'numpeople'    => $customer->numpeople,
            'numpdrive'    => $customer->numpeopledrive,
            'kids'         => $customer->kids,
            'request'      => $customer->request,
            'deliver'      => $customer->deliver,
            'cardtype'     => $customer->cardtype,
            'cardnum'      => $customer->cardnum,
            'cardexp'      => $customer->cardexp,
            'securitycode' => $customer->securitycode,
            'status'       => $customer->status,
        );

        $reservation = Customersatv::find('customerid="'.$id.'"');

        foreach ($reservation as $li)
        {
            $atvprice = Atvprices::findFirst("id='".$li->atvselected."'");
            $data2[] = array(
                'atvselected'   => $atvprice->title,
                );
        }
        echo json_encode(array('data'=>$data,'data2'=>$data2));
    }



}
