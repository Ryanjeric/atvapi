<?php

namespace Controllers;

use \Models\Atvprices as Atvprices;
use \Models\Atvrentalprices as Atvrentalprices;
use \Models\Atvimages as Atvimages;
use \Models\Rentalhours as Rentalhours;
use \Controllers\ControllerBase as CB;

class AtvpricesController extends \Phalcon\Mvc\Controller {

    public function listrentalsAction(){
        $time = new CB();
        $final = array();

        $query = "SELECT rentalhours.id, rentalhours.session, rentalhours.duration, rentalhours_ext.starttime, rentalhours_ext.endtime, rentalhours_ext.description FROM rentalhours RIGHT JOIN rentalhours_ext ON rentalhours.id = rentalhours_ext.rentalhoursid GROUP BY session ORDER BY session, rentalhours_ext.dateupdated";
        $list = CB::atvQuery($query);

        foreach ($list as $li) {
            $li['hours'] = str_replace(':00', '', ltrim($time->timeconverter($li['starttime'],$li['endtime']), '0'));
            $final[]=$li;
        }

        echo json_encode($final);
    }

    public function listrentalhoursAction() {
      // $query = "SELECT CASE WHEN rentalhours.session LIKE 'Half%' THEN 1 ELSE 2 END AS sessionname,
      //                 CASE WHEN rentalhours_ext.starttime LIKE '%AM%' THEN 1 ELSE 2 END AS amorpm,
      //                 rentalhours.session, rentalhours.duration, rentalhours_ext.*
      //                 FROM rentalhours RIGHT JOIN rentalhours_ext ON rentalhours.id = rentalhours_ext.rentalhoursid
      //                 ORDER BY sessionname, amorpm, rentalhours_ext.starttime";
      // $list = CB::atvQuery($query);
      // echo json_encode($list);
        echo json_encode(Rentalhours::groupAndSort());
    }

    public function getAtvSessions() {
        echo json_encode(Rentalhours::sort());
    }

    public function atvExistAction($title) {
        if(!empty($title)) {
            $user = Atvprices::findFirst('title="' . $title . '"');
            if ($user) {
                echo json_encode(array('exists' => true));
            } else {
                echo json_encode(array('exists' => false));
            }
        }

    }

    //IMAGE UPLOAD
    public function saveimageAction() {

        $filename = $_POST['imgfilename'];

        $picture = new Atvimages();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }

    }
    //IMAGE DELETE
    public function deleteimageAction($imgid) {
        $img = Atvimages::findFirst('id="'. $imgid.'"');
        $filename = $img->filename;
        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }


    public function listimageAction() {

        $getimages = Atvimages::find(array("order" => "id DESC"));
        if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
        foreach ($getimages as $getimages)
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
        }
        echo json_encode($data);

    }




    public function saveAction(){
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $details = $request->getPost('atv');
            $price = $request->getPost('price');
            $checkedprice = $request->getPost('checkedprice');

             $guid = new \Utilities\Guid\Guid();
             $id = $guid->GUID();
             $usave = new Atvprices();
             $usave->id = $id;
             $usave->title = $details['title'];
             $usave->subtitle = $details['subtitle'];
             $usave->features = $details['features'];
             $usave->description = $details['desc'];
             $usave->picture = $details['picture'];
             $usave->datecreated = date("Y-m-d H:i:s");
             $usave->dateupdated = date("Y-m-d H:i:s");

             if (!$usave->save()) {
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data['error'] =  $errors;
            } else {
                $rentalhr = array();
                $rentalprice = array();
                $rentalhr = $checkedprice;
                $rentalprice = $price;


                if($rentalhr == NULL || $rentalprice == NULL){
                    $data['error'] = "BOOM";
                }else{
                    foreach($rentalhr as $key => $value){
                        if($rentalhr[$key] == 'false' || $rentalprice[$key]==''){
                            $data['WEW'] = "Success";
                        }
                        else{
                            $save = new Atvrentalprices();
                            $save->assign(array(
                                'atvid' => $id,
                                'rentalid' => $rentalhr[$key],
                                'price' =>  $rentalprice[$key]
                                ));
                            if (!$save->save()) {
                                $errors = array();
                                foreach ($save->getMessages() as $message) {
                                    $errors[] = $message->getMessage();
                                }
                                $data['error'] =  $errors;
                            } else {

                                $data['success'] = "Success";
                            }
                        }
                    }
                }

           }
        }else{
            $data['error'] =  "NO POST DATA";
        }

        echo json_encode($data);
    }

     public function ListAction($num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined') {
            $list = Atvprices::find(array("order"=>"datecreated"));
        } else {
            $conditions = "title LIKE '%" . $keyword . "%'
            or subtitle LIKE '%" . $keyword . "%'
            or features LIKE '%" . $keyword . "%'";
            $list= Atvprices::find(array($conditions,"order"=>"datecreated"));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $list,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'title' => $m->title,
                'subtitle' => $m->subtitle,
                'features' => $m->features
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }



    public function deleteAction($id){
        $dlt = Atvprices::findFirst('id="' . $id . '"');
        if ($dlt) {
            if($dlt->delete()){
                $del = Atvrentalprices::find('atvid="'.$id.'"');
                $del->delete();
                echo json_encode(array("type" => "success", "msg" => "ATV Deleted"));
            }else {
                $errors = array();
                foreach($dlt->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array("type" => "danger", "msg" => $errors));
            }
        }

    }

    public function Infoction($id) {
        $getInfo = Atvprices::findFirst('id="'. $id .'"');
        $data = array(
            'id' =>  $getInfo->id,
            'title' =>  $getInfo->title,
            'subtitle' =>  $getInfo->subtitle,
            'features' =>  $getInfo->features,
            'desc' =>  $getInfo->description,
            'picture' =>  $getInfo->picture
            );

        // $getInfo2 = Atvrentalprices::find();
        $getInfo2 = Atvrentalprices::find("atvid = '".$id."'");

        foreach ($getInfo2 as $getInfo2)
        {
            $data2[] = array(
                'atvid'=>$getInfo2->atvid,
                'rentalid'=>$getInfo2->rentalid,
                'price' =>$getInfo2->price
                );
        }

        // $sessionprices = Atvrentalprices::find('atvid="'.$id.'"');
        // $prices = [];
        // foreach($sessionprices as $price) {
        //   array_push($prices,$price->price);
        // }

        echo json_encode(array("data"=>$data,"data2"=>$data2));
    }

    public function fegetpriceAction($atvid,$rentalid){
        $getInfo2 = Atvrentalprices::findFirst('atvid="'.$atvid.'" AND rentalid="'.$rentalid.'"');

        $data = array(
            'price' =>$getInfo2->price,
            );

        echo json_encode($data);
    }



    public function UpdateAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            //VARIABLE
            $details = $request->getPost('atv');
            $price = $request->getPost('price');
            $checkedprice = $request->getPost('checkedprice');

            $usave = Atvprices::findFirst('id="' . $details['id'] . '"');
            $usave->title = $details['title'];
            $usave->subtitle = $details['subtitle'];
            $usave->features = $details['features'];
            $usave->description = $details['desc'];
            $usave->picture = $details['picture'];
            $usave->dateupdated = date("Y-m-d H:i:s");

            if(!$usave->save()) {
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
               $data['error'] =  $errors;
            } else {
                $del = Atvrentalprices::find('atvid="'.$details['id'].'"');
                if($del->delete()){
                    $rentalhr = array();
                    $rentalprice = array();
                    $rentalhr = $checkedprice;
                    $rentalprice = $price;


                    if($rentalhr == NULL || $rentalprice == NULL) {
                        $data['error'] = "BOOM";
                    } else {
                        foreach($rentalhr as $key => $value){
                            if($rentalhr[$value] == 'false' || $rentalprice[$value]==''){
                                $data['WEW'] = "Success";
                            }
                            else{
                                  $save = new Atvrentalprices();
                                  $save->assign(array(
                                    'atvid' => $details['id'],
                                    'rentalid' => $rentalhr[$value],
                                    'price' =>  $rentalprice[$value]
                                    ));

                                  if (!$save->save()) {
                                    $errors = array();
                                    foreach ($save->getMessages() as $message) {
                                        $errors[] = $message->getMessage();
                                    }
                                    $data['error'] =  $errors;
                                } else {

                                   $data['success'] = "Success";
                               }
                            }

                        }
                    }
                }
            }
        }
        echo json_encode($data);
    }


     public function feInfoAction() {
        $getInfo = Atvprices::find(array("order"=>"title","order"=>"subtitle"));
        $time = new CB();
        foreach ($getInfo as $key => $value){

            $data[] = array(
            'id' =>  $value->id,
            'title' =>  $value->title,
            'subtitle' =>  $value->subtitle,
            'features' =>  $value->features,
            'desc' =>  $value->description,
            'picture' =>  $value->picture,
            );

            // $getInfo2 = CB::atvQuery("SELECT atvrentalprices.*, CASE WHEN rentalhours.session LIKE 'Half%' THEN 1 ELSE 2 END AS sessionname FROM atvrentalprices LEFT JOIN rentalhours ON atvrentalprices.rentalid = rentalhours.id  WHERE atvrentalprices.atvid = '".$value->id."' ORDER BY sessionname, rentalhours.session, rentalhours.duration ");

            $getInfo2 = CB::atvQuery("SELECT atvrentalprices.*, CASE WHEN rentalhours.session LIKE '%Special' THEN 1 ELSE 2 END AS sessionname FROM atvrentalprices LEFT JOIN rentalhours ON atvrentalprices.rentalid = rentalhours.id  WHERE atvrentalprices.atvid = '" . $value->id . "' ORDER BY rentalhours.duration, sessionname");

            foreach ($getInfo2 as $get2) {
                $rental = Rentalhours::findFirst('id="' . $get2['rentalid'] . '"');

                $query = "SELECT * FROM rentalhours WHERE id = '".$get2['rentalid']."'";
                $rental = CB::atvQueryFirst($query);

                if(stripos($rental['session'], "Special") !== FALSE){
                  $totalhr = $rental['duration'] . ' HR - ' . $rental['session'];
                } else {
                  $totalhr = $rental['duration'] . " HR";
                }

                $data2[] = array(
                    'session'=>$rental['session'],
                    'atvid'=>$get2['atvid'],
                    'rentalid'=>$get2['rentalid'],
                    'price' =>$get2['price'],
                    'totalhr' => $totalhr
                    );
                    // 'totalhr'=> str_replace(':00', '', ltrim($time->timeconverter($rentalstarttime,$rental->endtime), '0'))
            }
        }

        echo json_encode(array("data"=>$data,"data2"=>$data2));
    }

    public function filter_atv_if_it_has_priceAction($rentalhoursid) {
      $query1 = "SELECT rentalhours.id FROM rentalhours LEFT JOIN rentalhours_ext ON rentalhours.id = rentalhours_ext.rentalhoursid WHERE rentalhours_ext.id = '".$rentalhoursid."'";
      $sessionid = CB::atvQueryFirst($query1)['id']; //REKTA kong kinuha ID kasi isang data lang siya

      $query2 = "SELECT atvprices.* FROM atvrentalprices
                  LEFT JOIN atvprices ON atvprices.id = atvrentalprices.atvid
                  WHERE atvrentalprices.rentalid = '".$sessionid."'";
      $getInfo = CB::atvQuery($query2);

      foreach ($getInfo as $key => $value){
          $data[] = array(
          'id' =>  $value['id'],
          'title' =>  $value['title'],
          'subtitle' =>  $value['subtitle'],
          'features' =>  $value['features'],
          'desc' =>  $value['description'],
          'picture' =>  $value['picture'],
          );
      }
      echo json_encode(array("data"=>$data));
    }

    public function feInfoResAction($id) {
        $getInfo = Atvprices::findFirst('id="'.$id.'"');
        $time = new CB();

            $data = array(
            'id' =>  $getInfo->id,
            'title' =>  $getInfo->title,
            'subtitle' =>  $getInfo->subtitle,
            'features' =>  $getInfo->features,
            'desc' =>  $getInfo->description,
            'picture' =>  $getInfo->picture,
            );

            $getInfo2 = Atvrentalprices::find('atvid="'.$id.'"');
            foreach ($getInfo2 as $get2)
            {
                $rental = Rentalhours::findFirst('id="' . $get2->rentalid . '"');
                $data2[] = array(
                    'session'=>$rental->session,
                    'atvid'=>$get2->atvid,
                    'rentalid'=>$get2->rentalid,
                    'price' =>$get2->price,
                    'totalhr'=> str_replace(':00', '', ltrim($time->timeconverter($rental->starttime,$rental->endtime), '0'))
                    );
            }
            echo json_encode(array("data"=>$data,"data2"=>$data2));
    }

}
