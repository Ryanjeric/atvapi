<?php

namespace Controllers;

use \Models\Metadata as Metadata;
use \Controllers\ControllerBase as CB;

class MetadataController extends \Phalcon\Mvc\Controller {

     public function ListAction($num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined') {
            $list = Metadata::find(array("order"=>"dateupdated"));
        } else {
            $conditions = "title LIKE '%" . $keyword . "%' 
            or dlf LIKE '%" . $keyword . "%'
            or dlt LIKE '%" . $keyword . "%'
            or distance LIKE '%" . $keyword . "%'";
            $list= Metadata::find(array($conditions,"order"=>"dateupdated"));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $list,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'module' => $m->module,
                'title'=> $m->title,
                'metatitle'=> $m->metatitle,
                'metakeyword' => $m->metakeyword,
                'metadescription' => $m->metadescription,
                'dateupdated' => $m->dateupdated
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }


    public function editAction($id){
        $m = Metadata::findFirst('id="' . $id . '"');
        if ($m) {
            $data = array(
               'id' => $m->id,
                'module' => $m->module,
                'title'=> $m->title,
                'metatitle'=> $m->metatitle,
                'metakeyword' => $m->metakeyword,
                'metadescription' => $m->metadescription,
                'dateupdated' => $m->dateupdated
                );
        }else{
            $data = 'nodata';
        }

        echo json_encode($data);
    }

    public function updateAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $id = $request->getPost('id');
            $title = $request->getPost('title');
            $metatitle = $request->getPost('metatitle');
            $metakeyword = $request->getPost('metakeyword');
            $metadescription = $request->getPost('metadescription');

            $update = Metadata::findFirst("id = '$id' ");
            if($update) {
                $update->assign(array(
                    'metatitle' => $metatitle,
                    'title' => $title,
                    'metakeyword' => $metakeyword,
                    'metadescription' =>$metadescription,
                    'dateupdated' => date('Y-m-d H:i:s')
                    ));
                if($update->save()) {
                    echo json_encode(array("type" => "success", "msg" => "Successfully updated!"));
                } else {
                    $errors = array();
                    foreach($update->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array("type" => "danger", "msg" => $errors));
                }
                } 
            }
    }
}