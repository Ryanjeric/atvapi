<?php

namespace Controllers;

use \Models\Gallery as Gallery;
use \Models\Videos as Videos;
use \Models\Adventure as Adventure;
use \Controllers\ControllerBase as CB;

class GalleryController extends \Phalcon\Mvc\Controller {
    public function galleryAction() {
        $getimages = Gallery::find(array("order" => "id DESC"));
        if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
            foreach ($getimages as $getimages) 
            {
                $data[] = array(
                    'id'=>$getimages->id,
                    'filename'=>$getimages->filename
                    );
            }
        }
        echo json_encode($data);
    }
    public function deleteimageAction($imgid) {
        $img = Gallery::findFirst('id="'. $imgid.'"');
        $filename = $img->filename;
        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");

            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');

        }
        echo json_encode($data);
    }
    public function uploadimageAction() {
        $filename = $_POST['imgfilename'];
        $picture = new Gallery();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
          $data['error']=array('Something went wrong saving the data, please try again.');
      } else {
          $data['success']=array('Images has been uploaded');
      }
      echo json_encode($data);
  }

  public function savevideoAction() {
    $video = $_POST['vid'];

    $vid = new Videos();
    $vid->assign(array(            
        'embed' => $video
        ));

    if (!$vid->save()) {
        $data['error'] = "Something went wrong saving the data, please try again.";
    } else {
        $data['success'] = "Success";
    }

    echo json_encode($data);

}

public function listvideoAction() {

    $getvid = Videos::find(array("order" => "id DESC"));

    if(count($getvid) == 0){
        $data['error']=array('NOVIDEOS');
    }else{
        foreach ($getvid as $getvid) 
        {
            $data[] = array(
                'id'=>$getvid->id,
                'embed'=>$getvid->embed
                );
        }
    }
    echo json_encode($data);

}
public function deletevideoAction()
    {
        $conditions = 'id="' . $_POST['videoid'] . '"';
        $Videos = Videos::findFirst(array($conditions));
        if ($Videos) {
            if ($Videos->delete()) {
                $data = array('success' => 'Videos Deleted');
            }
            else
            {
                $data = array('error' => 'Videos Not Deleted');
            }
        }
        echo json_encode($data);
    }

    public function save1Action(){
        $request = new \Phalcon\Http\Request();
        $msg = Adventure::find();
        $title= $request->getPost('title');
        $content= $request->getPost('content');
        if(count($msg)==0){
            $add = new Adventure();
            $add->assign(array(
                'title' => $title,
                'content' => $content,
                'datecreated' => date('Y-m-d H:i:s'),
                'dateupdated' => date('Y-m-d H:i:s')
                ));
                    // $add->save();
            if (!$add->save()) {
                $errors = array();
                foreach ($add->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] ="!SAVE";
            } 

            else{
                $data['success'] ="SAVE";
            }
        }else{
            $save = Adventure::findFirst(array("id=1"));
            $save->title    = $title;
            $save->content    = $content;
            $save->dateupdated   = date("Y-m-d H:i:s");

            if(!$save->save()){
                $errors = array();
                foreach ($save->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            }else{
                $data[]=array('success' => 'UPDATED');
            }
        }
        echo json_encode(array($data));
    }

    public function getMsg1Action() {
       $msg = Adventure::find(array("id=1"));

        if(count($msg)!=0){

            $data=array(
                'title' => $msg[0]->title,
                'content'  => $msg[0]->content
                );
        }else{
         $data=array(
            'content'  => ""
            );
        }
   echo json_encode($data);
    }
    public function fegalleryAction($limit) {
        $getimages = Gallery::find(array("order" => "id DESC","LIMIT"=> $limit));
        if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
            foreach ($getimages as $getimages) 
            {
                $data[] = array(
                    'id'=>$getimages->id,
                    'filename'=>$getimages->filename
                    );
            }
        }
        echo json_encode($data);
    }

}