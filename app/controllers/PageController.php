<?php

namespace Controllers;

use \Phalcon\Http\Request;
use \Models\Contactinfo as Contactinfo;
use \Models\Reservationimg as Reservationimg;
use \Models\Homepage as Homepage;
use \Models\Pagebanner as Pagebanner;
use \Models\Banners as Banners;
use Models\Settings as Settings;
use \Controllers\ControllerBase as CB;

class PageController extends \Phalcon\Mvc\Controller {

    public function getcontactAction() {
        echo json_encode(Contactinfo::findFirst()->toArray());
    }

    public function updatecontactinfoAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $c = Contactinfo::findFirst();
            $c->email = $request->getPost("email");
            $c->address = $request->getPost("address");
            $c->contactno = $request->getPost("contactno");
            $c->longitude = $request->getPost("longitude");
            $c->latitude = $request->getPost("latitude");
            $c->facebook = $request->getPost("facebook");
            $c->gplus = $request->getPost("gplus");
            $c->youtube = $request->getPost("youtube");
            $c->reservationtext = $request->getPost('reservationtext');
            $c->reservationimg = $request->getPost('reservationimg');

            if(!$c->save()) {
                $errors = array();
                foreach ($c->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                echo json_encode(array('success' => "Contact Information has been successfully updated"));
            }
        }
    }

    public function reservationimglistAction() {
        echo json_encode(Reservationimg::find()->toArray(), JSON_NUMERIC_CHECK);
    }

    public function deleteimageAction($imgid) {
        $img = Reservationimg::findFirst('id="'. $imgid.'"');
        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }

    public function saveimageAction() {

        $filename = $_POST['imgfilename'];

        $picture = new Reservationimg();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
    }

    public function gethomeAction() {
        echo json_encode(Homepage::findFirst()->toArray());
    }

    public function updatehomeimgAction($no) {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $hp = Homepage::findFirst();

            if($no == "1"){
                $hp->img1 = $request->getPost('filename');
                $hp->title1 = $request->getPost('title');
                $hp->link1 = $request->getPost('link');
            } else if($no == "2") {
                $hp->img2 = $request->getPost('filename');
                $hp->title2 = $request->getPost('title');
                $hp->link2 = $request->getPost('link');
            } else {
                $hp->img3 = $request->getPost('filename');
                $hp->title3 = $request->getPost('title');
                $hp->link3 = $request->getPost('link');
            }

            if(!$hp->save()) {
                $errors = array();
                foreach ($hp->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                echo json_encode(array('success' => "success"));
            }
        }
    }

    public function getbannerAction() {
        echo json_encode(Pagebanner::find()->toArray(), JSON_NUMERIC_CHECK);
    }

    public function bannerlistAction() {
        echo json_encode(Banners::find()->toArray());
    }

    public function deletebannerAction($img) {
        $img = Banners::findFirst('id="'. $img.'"');
        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }

    public function savebannerAction() {

        $filename = $_POST['imgfilename'];

        $picture = new Banners();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
    }

    public function updatepagebannerAction() {
        $request = new \Phalcon\Http\Request();

        $b = Pagebanner::findFirst("id='" . $request->getPost('id') . "'");
        if($b){
            $b->banner = $request->getPost("banner");
            $b->title = $request->getPost("title");
            $b->subtitle = $request->getPost("subtitle");
            $b->alignment = $request->getPost("alignment");
            $b->bgcolor = $request->getPost("bgcolor");
            $b->txtcolor = $request->getPost("txtcolor");
            $b->btntxt = $request->getPost("btntxt");
            $b->link = $request->getPost("link");
            
            if(!$b->save()) {
                $errors = array();
                foreach ($b->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                echo json_encode(array('success' => "Page banner has been updated."));
            }
        }
    }

    public function getfehomeAction() {
        $data['home'] = Homepage::findFirst()->toArray();
        $data['banner'] = Pagebanner::findFirst("page='Home'")->toArray();

        $data['logo'] = Settings::findFirst()->logo;

        echo json_encode($data);
    }

    public function getfebannerAction($page) {
        echo json_encode(Pagebanner::findFirst("id='" . $page . "'")->toArray());
    }
}