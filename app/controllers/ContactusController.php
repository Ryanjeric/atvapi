<?php

namespace Controllers;

use Phalcon\Http\Request;
use Utilities\Guid\Guid;
use Models\Contactus as Contactus;
use Models\Contactinfo;
use Models\Repliedmessage as Message;
use Controllers\ControllerBase as CB;
use Phalcon\Mvc\Model\Transaction\Failed as TransactionFailed;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use PHPMailer as PHPMailer;

class ContactusController extends \Phalcon\Mvc\Controller {

    public function sendAction(){

        $request = new Request();

        if($request->isPost()) {

            try {
                $manager     = new TransactionManager();
                $transaction = $manager->get();

                $fname= $request->getPost('fname');
                $lname= $request->getPost('lname');
                $email= $request->getPost('email');
                $content= $request->getPost('content');

                $add = new Contactus();
                $add->setTransaction($transaction);
                $add->id = Guid::GUID();
                $add->fname = $fname;
                $add->lname = $lname;
                $add->email = $email;
                $add->content = $content;
                $add->datesubmitted = date('Y-m-d');
                $add->status = 0;
                $add->datecreated = date('Y-m-d H:i:s');
                $add->dateupdated = date('Y-m-d H:i:s');

                if (!$add->save()) {

                    $transaction->rollback($add->getMessages()[0]->getMessage());

                } else {
                        ///MAIL 
                    $body = '<div style="background-color: #eee;padding:20px;margin-bottom:10px;">
                    Hi '.$fname.' '.$lname.',<br><br>
                    Thank you for contacting Vortex Healing ATV, your message was sent successfully.<br>
                    We will review your inquiry and get back to you shortly.<br><br>
                    ___________________________________________<br><br>
                    Your Message:<br>
                    '.$content.'</div>';


                    $toadmin = '<div style="background-color: #eee;padding:20px;margin-bottom:10px;">
                    Name : '.$fname.' '.$lname.'<br>
                    Email : '.$email.'<br><br>
                    Message : <br><br>
                    <p>'.nl2br($content).'</p>

                    </div>'; 

                    $emailAdmin = CB::sendMail(Contactinfo::findFirst()->email, 'Contact: Vortex Healing ATV', $toadmin);
                    $emailClient = CB::sendMail($email, 'Vortex Healing ATV', $body);

                    if($emailAdmin['Message'] != 'OK') {

                        $transaction->rollback($emailAdmin['Message']);

                    } elseif ($emailClient['Message'] != 'OK') {

                        $transaction->rollback($emailAdmin['Message']);

                    } else {
                        $transaction->commit(); //all is well
                    }

                }

            } catch (TransactionFailed $e) {
                $data['err'] = $e->getMessage();
            }

        } else {
            $data['err'] = "NO POST DATA";
        }
        
        echo json_encode($data);
    }

    public function listcontactsAction($num, $page, $keyword ,$stat ) {
        $countnew = count(Contactus::find("status = 0 "));
        if ($keyword == 'undefined' && $stat=='undefined') {
            $contacts = Contactus::find(array("order" => "datecreated desc, status"));
        }
        else if($keyword == 'undefined' && $stat!='undefined'){
             $contacts = Contactus::find(array("status LIKE '%" . $stat . "%'","order" => "datecreated desc"));
        }
        else if($keyword != 'undefined' && $stat!='undefined'){
              $conditions = "email LIKE '%" . $keyword . "%' and status LIKE '%" . $stat . "%' OR 
                  fname LIKE '%" . $keyword . "%' and status LIKE '%" . $stat . "%' OR 
                  lname LIKE '%" . $keyword . "%' and status LIKE '%" . $stat . "%' OR
                  datesubmitted LIKE '%" . $keyword . "%' and status LIKE '%" . $stat . "%'";
             $contacts = Contactus::find(array($conditions,"order" => "datecreated desc"));
        }
        else {
            $conditions = "email LIKE '%" . $keyword . "%' OR 
                          fname LIKE '%" . $keyword . "%' OR 
                          lname LIKE '%" . $keyword . "%' OR 
                          datesubmitted LIKE '%" . $keyword . "%'";
            $contacts = Contactus::find(array($conditions,"order" => "datecreated desc"));
        }

        $currentPage = (int) ($page);

            // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $contacts,
                "limit" => 10,
                "page" => $currentPage
                )
            );

            // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'name' => $m->fname ." ".$m->lname,
                'email' => $m->email,
                'content' => $content,
                'status' => $m->status,
                'datesubmitted' => $m->datesubmitted
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }

        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items,'countnew' => $countnew));
    }

    public function messagedeleteAction($id) {
        $data = $id;
        $request = Contactus::findFirst('id="'. $id.'"');
        if ($request) {
            if ($request->delete()) {
                $data = array('success' => 'Contact Deleted');
                $deletemessage = Message::find('rid="'.$id.'"');
                $deletemessage->delete();
            }
        }
        echo json_encode($data);
    }
    public function listreplyAction($id){
        $data = array();
        $getreply= Message::find('rid="' . $id . '" ');
        foreach ($getreply as $getreply) {
            $data[] = array(
                'id'=>$getreply->id,
                'rid'=>$getreply->rid,
                'message'=>$getreply->message,
                'date'=>$getreply->date
                );
        }
        echo json_encode($data);

    }
    public function viewreplyAction($id) {
        $data = array();
        $viewrequest = Contactus::findFirst('id="' . $id .'"');
        
        if ($viewrequest) {
            $viewrequest = array(                
                'id' => $viewrequest->id,
                'name' =>$viewrequest->fname ." ". $viewrequest->lname ,
                'email' =>$viewrequest->email,
                'content' =>$viewrequest->content,
                'status' => $viewrequest->status,
                'datesubmitted' =>$viewrequest->datesubmitted
                );
        }
        echo json_encode($viewrequest);
        $read = Contactus::findFirst('id="' . $id .'"');
        if($read->status == 0){
            $read->status = 1;

            if (!$read->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            }
        }
    }
     public function replycontactAction() {
        $data = array();

        $id = $_POST['rid'];
        $request = Contactus::findFirst('id="' . $id .'"');
        $fname = $request->fname;
        $lname = $request->lname;
        $request->status = 2;
        if (!$request->save()) {
            
            $data['error'] = "Something went wrong saving the data, please try again.";

        } else {
                ///MAIL 
                $body = '<div style="background-color: #eee;padding:20px;margin-bottom:10px;">
                Hi '.$fname.' '.$lname.',<br><br>
                This is the reply to your message: <br>"'.$_POST['content'].'"</div>' . '<br><br> Admin Reply: ' .$_POST['messages'];

                $email = CB::sendMail($_POST['email'], 'Vortex Healing ATV', $body);
                if($email['Message'] == 'OK') {
                    $data['success'] ="SAVE";

                    $repliedmessage = new Message();
                    $repliedmessage->assign(array(
                        'rid'  => $id,
                        'message' => $_POST['messages'],
                        'datereplied' => date('Y-m-d H:i:s')
                        ));
                    if (!$repliedmessage->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                    }else{
                        $data['success'] = "Success";
                    }
                } else {

                    $data['error'] = $email['Message'];

                }

                // $mail = new PHPMailer();
                // $mail->isSMTP();
                // $mail->Host = 'smtp.mandrillapp.com';
                // $mail->SMTPAuth = true;
                // $mail->Username = 'efrenbautistajr@gmail.com';
                // $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                //$mail->SMTPSecure = 'ssl';                    
                // $mail->Port = 587;
                // $mail->From = 'rioverdeatv@gmail.com';
                // $mail->FromName = 'rioverdeatv@gmail.com';
                // $mail->addAddress($_POST['email']);
                // $mail->isHTML(true);
                // $mail->Subject = 'Rio Verde ATV';
                // $mail->Body = $body;

                // if (!$mail->send()) {
                //     $data = array('error' => $mail->ErrorInfo);
                // } else {
                //     $data['success'] ="SAVE";
                // }
        }
        echo json_encode($data);
    }
}