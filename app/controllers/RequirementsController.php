<?php

namespace Controllers;
use \Models\Requirements as Requirements;
use \Controllers\ControllerBase as CB;

class RequirementsController extends \Phalcon\Mvc\Controller {

    public function saveAction(){
        $request = new \Phalcon\Http\Request();
        $guid = new \Utilities\Guid\Guid();
        $id = $guid->GUID();
        $add = new Requirements();
        $content = $request->getPost('reqasset');

        $find = Requirements::findFirst('requirements="'.$content.'"');
        if($find){
            echo json_encode(array("type" => "danger", "msg" => "Warning! That Requirements already exist!"));
        }else{
            $add->assign(array(
                'id' =>$id,
                'requirements' => $content,
                'datecreated' => date('Y-m-d H:i:s'),
                'dateupdated' => date('Y-m-d H:i:s')
                ));
                    // $add->save();
            if (!$add->save()) {
                $errors = array();
                foreach ($add->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            }
            else{
                echo json_encode(array("type" => "success", "msg" => "Requirements successfully added!"));
            }
        }
    }

    public function listreqAction($num,$off,$keyword) {
        $db = \Phalcon\DI::getDefault()->get('db');
        $offsetfinal = ($off * $num) - $num;
        if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {
            $stmt = $db->prepare("SELECT * FROM requirements ORDER BY datecreated LIMIT $offsetfinal, $num");
            $stmt->execute();
            $list = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            $totallist = Requirements::find();
            $totalNumber = count($totallist);

        } else {
            $stmt = $db->prepare("SELECT * FROM requirements WHERE requirements LIKE '%$keyword%' ORDER BY datecreated LIMIT $offsetfinal, $num");
            $stmt->execute();
            $list = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            $stmt = $db->prepare("SELECT * FROM requirements WHERE requirements LIKE '%$keyword%' ");
            $stmt->execute();
            $totallist = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $totalNumber = count($totallist);
        }

        echo json_encode(array('list' => $list, 'index' => $off, 'total_items' => $totalNumber));
    }
    public function felistAction(){
         $list = Requirements::find(array("order"=>"datecreated asc"));
        if(count($list) == 0){
            $data['error']=array('No!');
        }else{
          foreach ($list as $li)
          {
              $data[] = array(
                  'id'=>$li->id,
                  'requirements'=>$li->requirements
                  );
          }
        }
        echo json_encode($data);
    }
    public function updateAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $id = $request->getPost('id');
            $Requirements = $request->getPost('name');

            $find = Requirements::findFirst('id != "'.$id.'" AND requirements = "'.$Requirements.'"');
            if($find) { //return false
                echo json_encode(array("type" => "danger", "msg" => "Warning! That Requirements already exist!"));
            } else {
                $update = Requirements::findFirst("id = '$id' ");
                if($update) {
                    $update->assign(array(
                        'requirements' => $Requirements,
                        'dateupdated' => date('Y-m-d H:i:s')
                        ));
                    if($update->save()) {
                        echo json_encode(array("type" => "success", "msg" => "Requirements successfully updated!"));
                    } else {
                        $errors = array();
                        foreach($update->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        echo json_encode(array("type" => "danger", "msg" => $errors));
                    }
                }
            }
        }
    }
    public function deleteAction($id){
        $find = Requirements::findFirst("id = '$id'");
        if($find->delete()){
            echo json_encode(array("type" => "success", "msg" => "Requirements Deleted"));
        }else{
            $errors = array();
            foreach($find->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array("type" => "danger", "msg" => $errors));
        }
    }
}
