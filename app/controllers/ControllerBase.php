<?php

namespace Controllers;

class ControllerBase extends \Phalcon\Mvc\Controller {

    public function atvQuery($phql) {
      $dbatv = \Phalcon\DI::getDefault()->get('db');
      $stmt = $dbatv->prepare($phql);
      $stmt->execute();
      $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      return $searchresult;
    }

    public function atvQueryFirst($phql) {
      $dbatv = \Phalcon\DI::getDefault()->get('db');
      $stmt = $dbatv->prepare($phql);
      $stmt->execute();
      $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);
      return $searchresult;
    }

    public function modelsManager($phql) {
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        return $result = $app->modelsManager->executeQuery($phql);
    }

    public function getConfig(){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        return $app->config;
    }

    public function sendMail($email, $subject, $content){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        $json = json_encode(array(
            'From' => $app->config->postmark->signature,
            'To' => $email,
            'Subject' => $subject,
            'HtmlBody' => $content
        ));

        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $app->config->postmark->url);
        curl_setopt($ch2, CURLOPT_POST, true);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'X-Postmark-Server-Token: '.$app->config->postmark->token
        ));
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
        $response = json_decode(curl_exec($ch2), true);
        $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        curl_close($ch2);
        // $response = json_decode(curl_exec($ch2), true);
        return $response;
    }

    public function timeconverter($start,$end){
        $str1= explode(":",$start);
        $ampm = explode(" ",$start);
        $min = explode(" ",$str1[1]);

        $str2= explode(":",$end);
        $ampm2 = explode(" ",$end);
        $min2 = explode(" ",$str2[1]);

        if($ampm[1] =="PM" || $ampm[1] =="MN"){
            $pm= array('1' => '13', '2' => '14', '3' => '15', '4' => '16', '5' => '17', '6' => '18', '7' => '19', '8' => '20', '9' => '21', '10' => '22', '11' => '23', '12' => '24');
            $start = $pm[$str1[0]].":".$min[0];
        }
        else{
            $start = $str1[0].":".$min[0];
        }


        if($ampm2[1] =="PM" || $ampm2[1] =="MN"){
            $pm= array('1' => '13', '2' => '14', '3' => '15', '4' => '16', '5' => '17', '6' => '18', '7' => '19', '8' => '20', '9' => '21', '10' => '22', '11' => '23', '12' => '24');
            $endtime = $pm[$str2[0]].":".$min2[0];
        }else{
            $endtime = $str2[0].":".$min2[0];
        }

        $difference =  strtotime($endtime.":00") - strtotime($start.":00");

        if($difference / 3600 > 0){
         $hour = $difference / 3600;
         $hour = (int)$hour;

         $difference = $difference - ($hour * 3600);

     }else{
         $hour = "00";
     }

     if($difference / 60 > 0){
       $min = $difference / 60;

       $difference = $difference - ($min * 60);

   }else{
       $min = "00";
   }

    $data = str_replace('"',' ',$this->checkstring($hour).':'.$this->checkstring($min));
    if($endtime == $start){
        return '24:00';
    }
    return $data;
    }

    public function checkstring($str){
        if(strlen($str)==1){
            return "0".$str;
        }
        return $str;
    }

    public function dateFormat($date) {
      $newDate =date_create($date);
        return date_format($newDate, 'Y-m-d');
    }

    public function timeFormat($time) {
      $newDate =date_create($time);
        return date_format($newDate, 'H:i:s');
    }

    public function dateTimeFormat($date) {
      $newDate =date_create($date);
        return date_format($newDate, 'Y-m-d H:i:s');
    }

    public function monthEndDate($date) {
        $newDate =date_create($date);
        return date_format($newDate, 'Y-m-t');
    }

}
