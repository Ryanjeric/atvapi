<?php
namespace Controllers;
use \Models\Settings as Settings;
use \Models\Logoimage as Logo;
use \Models\Googlescript as Script;
use \Controllers\ControllerBase as CB;
class SettingsController extends \Phalcon\Mvc\Controller{

    public function managesettingsAction(){
       $setting = Settings::findFirst("id=" . 1);
         if ($setting) {
        $data = array(
            'id' => $setting->id,
            'value1' =>$setting->value1,
            'value2' =>$setting->value2,
            'value3' =>$setting->value3,
            'logo'   =>$setting->logo,
            );
    }
    echo json_encode($data);

    }

    public function maintenanceonAction() {

        var_dump($_POST);
        $data = array();
        $setting = Settings::findFirst("id=" . 1);

                $setting->value1 = 1;
                $setting->value2 = $_POST['maintenance_msg'];
                $setting->value3 = $_POST['maintenance_time'];

        if (!$setting->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                } else {
                    $data['success'] = "Success";
                     //START Log
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Settings", /*//Examaple News, Create Center, Slider, Events etc...*/
                        "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                        "title" => "Maintenance ON - Message : ". $_POST['maintenance_msg'] ."", /*// Maybe some info here (confuse) XD*/
                        ));
                    //END Audit Log
                }

        echo json_encode($setting);


    }

    public function maintenanceoffAction() {

        var_dump($_POST);
        $data = array();

        $setting = Settings::findFirst("id=" . 1);

                $setting->value1 = 0;

        if (!$setting->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                } else {
                    $data['success'] = "Success";
                     //START Log
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Settings", /*//Examaple News, Create Center, Slider, Events etc...*/
                        "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                        "title" => "Maintenance off", /*// Maybe some info here (confuse) XD*/
                        ));
                    //END Audit Log
                }
        echo json_encode($setting);

    }

    public function savedefaultlogoAction() {
        $request = new \Phalcon\Http\Request();
        $data = array();
        $setting = Settings::findFirst("id=" . 1);
        $logoto = $setting->logo;
        $setting->logo =  $request->getPost('logo');

        if (!$setting->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }

        echo json_encode($data);
    }



    public function uploadlogoAction() {
        $filename = $_POST['imgfilename'];

        $picture = new Logo();
        $picture->assign(array(
            'filename' => "$filename"
            ));

        if (!$picture->save()) {
          $data[]=array('error' => 'Something went wrong saving the data, please try again.');
      } else {

          $data[]=array('success' => 'Images has been uploaded');
            // //START Log
            //         $audit = new CB();
            //         $audit->auditlog(array(
            //             "module" =>"Settings", /*//Examaple News, Create Center, Slider, Events etc...*/
            //             "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
            //             "title" => "Add Logo - ". $filename ."", /*// Maybe some info here (confuse) XD*/
            //             ));
            //         //END Audit Log
      }
      echo json_encode($data);
    }

     public function deletelogoAction($imgid) {
            $img = Logo::findFirst('id="'. $imgid.'"');
            $filename = $img->filename;
            if ($img) {
                if ($img->delete()) {

                    $data[]=array('success' => "");
                    //START Log
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Settings", /*//Examaple News, Create Center, Slider, Events etc...*/
                        "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                        "title" => "Delete Logo - ". $filename ."", /*// Maybe some info here (confuse) XD*/
                        ));
                    //END Audit Log

                }else{
                    $data[]=array('error' => '');
                }
            }else{
                $data[]=array('error' => '');
            }
            echo json_encode($data);
        }
    public function listlogoAction() {
        $getimages = Logo::find(array("order" => "id DESC"));
        if(count($getimages) == 0){
                $data['error']=array('NOIMAGE');
            }else{
                foreach ($getimages as $getimages)
                    {
                        $data[] = array(
                            'id'=>$getimages->id,
                            'filename'=>$getimages->filename
                            );
                    }
             }
            echo json_encode($data);
    }
    public function scriptAction(){

        $dlttemp = Script::find();
        if ($dlttemp) {
            if($dlttemp->delete()){
            }
        }

        $scipt = new Script();
        $scipt->assign(array(
            'script' => $_POST['data']
            ));
        if (!$scipt->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
    }

    public function loadscriptAction(){
        $getscript = Script::find();
        foreach ($getscript as $m) {
            $data = array(
                'script' => $m->script
                );
        }
        echo json_encode($data);

    }
    public function displaytAction() {
        $script = Script::findFirst();
        if ($script) {
            $data = array(
                'script' => $script->script,
                );
        }
        echo json_encode($data);
    }
}
