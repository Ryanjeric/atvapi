<?php

namespace Controllers;
use \Models\Freebies as Freebies;
use \Models\Deliver as Deliver;
use \Models\Specialoffers as Specialoffers;
use \Controllers\ControllerBase as CB;

class FreebiesController extends \Phalcon\Mvc\Controller {

    public function saveAction(){
        $request = new \Phalcon\Http\Request();
        $msg = Freebies::find();
        $content= $request->getPost('content');
        if(count($msg)==0){
            $add = new Freebies();
            $add->assign(array(
                'content' => $content,
                'datecreated' => date('Y-m-d H:i:s'),
                'dateupdated' => date('Y-m-d H:i:s')
                ));
                    // $add->save();
            if (!$add->save()) {
                $errors = array();
                foreach ($add->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] ="!SAVE";
            } 

            else{
                $data['success'] ="SAVE";
            }
        }else{
            $save = Freebies::findFirst(array("id=1"));
            $save->content    = $content;
            $save->dateupdated   = date("Y-m-d H:i:s");

            if(!$save->save()){
                $errors = array();
                foreach ($save->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            }else{
                $data[]=array('success' => 'UPDATED');
            }
        }
        echo json_encode(array($data));
    }

    public function getMsgAction() {
       $msg = Freebies::find(array("id=1"));

        if(count($msg)!=0){

            $data=array(
                'content'  => $msg[0]->content
                );
        }else{
         $data=array(
            'content'  => ""
            );
        }
   echo json_encode($data);
    }

    public function save1Action(){
        $request = new \Phalcon\Http\Request();
        $msg = Deliver::find();
        $content= $request->getPost('content');
        if(count($msg)==0){
            $add = new Deliver();
            $add->assign(array(
                'content' => $content,
                'datecreated' => date('Y-m-d H:i:s'),
                'dateupdated' => date('Y-m-d H:i:s')
                ));
                    // $add->save();
            if (!$add->save()) {
                $errors = array();
                foreach ($add->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] ="!SAVE";
            } 

            else{
                $data['success'] ="SAVE";
            }
        }else{
            $save = Deliver::findFirst(array("id=1"));
            $save->content    = $content;
            $save->dateupdated   = date("Y-m-d H:i:s");

            if(!$save->save()){
                $errors = array();
                foreach ($save->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            }else{
                $data[]=array('success' => 'UPDATED');
            }
        }
        echo json_encode(array($data));
    }

    public function getMsg1Action() {
       $msg = Deliver::find(array("id=1"));

        if(count($msg)!=0){

            $data=array(
                'content'  => $msg[0]->content
                );
        }else{
         $data=array(
            'content'  => ""
            );
        }
   echo json_encode($data);
    }


    public function save2Action(){
        $request = new \Phalcon\Http\Request();
        $msg = Specialoffers::find();
        $content= $request->getPost('content');
        if(count($msg)==0){
            $add = new Specialoffers();
            $add->assign(array(
                'content' => $content,
                'datecreated' => date('Y-m-d H:i:s'),
                'dateupdated' => date('Y-m-d H:i:s')
                ));
                    // $add->save();
            if (!$add->save()) {
                $errors = array();
                foreach ($add->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] ="!SAVE";
            } 

            else{
                $data['success'] ="SAVE";
            }
        }else{
            $save = Specialoffers::findFirst(array("id=1"));
            $save->content    = $content;
            $save->dateupdated   = date("Y-m-d H:i:s");

            if(!$save->save()){
                $errors = array();
                foreach ($save->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            }else{
                $data[]=array('success' => 'UPDATED');
            }
        }
        echo json_encode(array($data));
    }

    public function getMsg2Action() {
       $msg = Specialoffers::find(array("id=1"));

        if(count($msg)!=0){

            $data=array(
                'content'  => $msg[0]->content
                );
        }else{
         $data=array(
            'content'  => ""
            );
        }
   echo json_encode($data);
    }


}