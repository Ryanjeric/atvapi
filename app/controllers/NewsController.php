<?php

namespace Controllers;

use \Phalcon\Http\Request;
use \Utilities\Guid\Guid;
use \Models\News as News;
use \Models\Newscategory as Newscategory;
use \Models\Newstags as Newstags;
use \Models\Newsselectedcategories as Newsselectedcategories;
use \Models\Newsselectedtags as Newsselectedtags;
use \Models\Images as Images;
use \Models\Videocollection as Videocollection;
use \Models\Authors as Authors;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Query;

class NewsController extends \Phalcon\Mvc\Controller {
	public function addcategoryAction($asset) {
		$categoryname = $asset;
		if($categoryname) {
				// $find = Newscategory::findFirst("categoryname = '$asset'");

			$find = Newscategory::find(array(
				"conditions" => "categoryname = ?1",
				"bind"       => array(1 => $asset)
				));

				if(count($find) > 0) { //return false
					echo json_encode(array("type" => "danger", "msg" => "Warning! That category name already exist!"));
				} else { //return true
					$guid = new \Utilities\Guid\Guid();
					$categoryid = $guid->GUID();
					$add = new Newscategory();
					$add->assign(array(
						'id' => $categoryid,
						'categoryname' => $categoryname,
						'datecreated' => date('Y-m-d'),
						'dateupdated' => date('Y-m-d H:i:s')
						));
					if($add->create()) {
						echo json_encode(array("type" => "success", "msg" => "News category successfully added!"));
					} else {
						$errors = array();
						foreach($add->getMessages() as $message) {
							$errors[] = $message->getMessage();
						}
						echo json_encode(array("type" => "danger", "msg" => $errors));
					}
				}
			}
		}

		public function listcategoryAction($num,$off,$keyword) {
			$db = \Phalcon\DI::getDefault()->get('db');
			$offsetfinal = ($off * $num) - $num;
			if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {
				$stmt = $db->prepare("SELECT * FROM newscategory ORDER BY categoryname LIMIT $offsetfinal, $num");
				$stmt->execute();
				$list = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$totallist = Newscategory::find();
				$totalNumber = count($totallist);
			} else {
				$stmt = $db->prepare("SELECT * FROM newscategory WHERE categoryname LIKE '%$keyword%' ORDER BY categoryname LIMIT $offsetfinal, $num");
				$stmt->execute();
				$list = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$stmt = $db->prepare("SELECT * FROM newscategory WHERE categoryname LIKE '%$keyword%' ");
				$stmt->execute();
				$totallist = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$totalNumber = count($totallist);
			}
			echo json_encode(array('list' => $list, 'index' => $off, 'total_items' => $totalNumber));
		}

		public function updatecategoryAction() {
			$request = new \Phalcon\Http\Request();
			if($request->isPost()) {
				$id = $request->getPost('id');
				$categoryname = $request->getPost('name');

				$find = Newscategory::find(array(
					"conditions" => "id != ?1 AND categoryname = ?2",
					"bind" => array(1 => $id, 2 => $categoryname)
					));

			if(count($find) > 0 ) { //return false
				echo json_encode(array("type" => "danger", "msg" => "Warning! That category name already exist!"));
			} else {
				$update = Newscategory::findFirst("id = '$id' ");
				if($update) {
					$update->assign(array(
						'categoryname' => $categoryname,
						'dateupdated' => date('Y-m-d H:i:s')
						));
					if($update->save()) {
						echo json_encode(array("type" => "success", "msg" => "News Category successfully saved!"));
					} else {
						$errors = array();
						foreach($update->getMessages() as $message) {
							$errors[] = $message->getMessage();
						}
						echo json_encode(array("type" => "danger", "msg" => $errors));
					}
				}
			}
		}
	}

	public function addtagAction($asset) {
		$tagname = $asset;
		if($tagname) {
			$find = Newstags::find(array(
				"conditions" => "tagname = ?1",
				"bind"       => array(1 => $asset)
				));
			if(count($find) > 0) {
				echo json_encode(array("type" => "danger", "msg" => "Warning! That tag name already exist!"));
			} else {
					// $guid = new \Utilities\Guid\Guid();
				$guid = new Guid();
				$tagid = $guid->GUID();
				$add = new Newstags();

				$add->id = $tagid;
				$add->tagname = $tagname;
				$add->datecreated = date('Y-m-d');
				$add->dateupdated = date('Y-m-d H:i:s');

				if($add->create()) {
					echo json_encode(array("type" => "success", "msg" => "News Tag successfully added!"));
				} else {
					$errors = array();
					foreach($add->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					echo json_encode(array("type" => "danger", "msg" => $errors[0]));
				}
			}
		}
	}

	public function listtagsAction($num,$off,$keyword) {
		$db = \Phalcon\DI::getDefault()->get('db');
		$offsetfinal = ($off * $num) - $num;
		if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {
			$totalNumber = Newstags::count();
			$orayt = "this";

			if($off > ceil($totalNumber/$num) && $off > 1) {
				$off = $off - 1;
				$offsetfinal = $offsetfinal - $num;
			}

			$stmt = $db->prepare("SELECT * FROM newstags ORDER BY tagname LIMIT $offsetfinal, $num");
			$stmt->execute();
			$list = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		} else {
			$stmt = $db->prepare("SELECT * FROM newstags WHERE tagname LIKE '%$keyword%' ");
			$stmt->execute();
			$totallist = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			$totalNumber = count($totallist);

			if($off > ceil(count($totalNumber)/$num) && $off > 1) {
				$off = $off - 1;
				$offsetfinal = $offsetfinal - $num;
			}

			$stmt = $db->prepare("SELECT * FROM newstags WHERE tagname LIKE '%$keyword%' ORDER BY tagname LIMIT $offsetfinal, $num");
			$stmt->execute();
			$list = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		}
		echo json_encode(array('list' => $list, 'index' => $off, 'total_items' => $totalNumber, 'offsetfinal' => $offsetfinal, 'this' => $num));
	}

	public function updatetagAction() {
		$request = new \Phalcon\Http\Request();
		if($request->isPost()) {
			$id = $request->getPost('id');
			$tagname = $request->getPost('name');

			$find = Newstags::find(array(
				"conditions" => "id != ?1 AND tagname = ?2",
				"bind"       => array(1 => $id, 2 => $tagname)
				));
			if(count($find) > 0) { //return false
				echo json_encode(array("type" => "danger", "msg" => "Warning! That tag name already exist!"));
			} else {
				$update = Newstags::findFirst("id = '".$id."' ");
				if($update) {
					$update->assign(array(
						'tagname' => $tagname,
						'dateupdated' => date('Y-m-d H:i:s')
						));
					if($update->save()) {
						echo json_encode(array("type" => "success", "msg" => "News Tag successfully saved!"));
					} else {
						$errors = array();
						foreach($update->getMessages() as $message) {
							$errors[] = $message->getMessage();
						}
						echo json_encode(array("type" => "danger", "msg" => $errors));
					}
				}
			}
		}
	}

	public function removecategoryAction($id) {
		$errors = array();
		$find = Newscategory::findFirst("id = '".$id."' ");
		if($find) {
			if($find->delete()) {
				$data['type'] = "success";
				$data['msg'] = "News Category successfully remove.";

				$deleteall = Newsselectedcategories::find("categoryid = '".$id."'");
				if($deleteall->delete()){
					$data['type'] = "success";
					$data['dltall_msg'] = "All selectedcategories deleted";
				} else {
					foreach($deleteall->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					$data['type'] = "danger";
					$data['dltall_err_msg'] = $errors;
				}

			} else {
				foreach($find->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				$data['type'] = "danger";
				$data['msg'] = $errors[0];
			}
		} else {
			foreach($find->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}
			$data['type'] = "danger";
			$data['msg'] = $errors[0];
		}
		echo json_encode($data);
	}

	public function removetagAction($id) {
		$errors = array();
		$find = Newstags::findFirst("id = '".$id."' ");
		if($find) {
			if($find->delete()) {
				$data['type'] = "success";
				$data['msg'] = "News Tag successfully remove.";

				$deleteall = Newsselectedtags::find("tagid = '".$id."'");
				if($deleteall->delete()){
					$data['type'] = "success";
					$data['dltall_msg'] = "All selectedtags deleted";
				} else {
					foreach($deleteall->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					$data['type'] = "danger";
					$data['dltall_err_msg'] = $errors;
				}

			} else {
				foreach($find->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				$data['type'] = "danger";
				$data['msg'] = $errors[0];
			}
		} else {
			foreach($find->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}
			$data['type'] = "danger";
			$data['msg'] = $errors[0];
		}
		echo json_encode($data);
	}

	public function authoruploadimageAction() {
		$request = new \Phalcon\Http\Request();;
		if($request->isPost()) {
			$guid = new \Utilities\Guid\Guid();
			$genguid = $guid->GUID();

			$imagename = $request->getPost('imgfilename');
			$category = $request->getPost('category');

			$add = new Images();
			$add->assign(array(
				'id' => $genguid,
				'imagename' => $imagename,
				'category' => $category,
				'datecreated' => date('Y-m-d H:i:s')
				));
			if($add->create()) {
				$data['type'] = "success";
				$data['msg'] = "New Author's image added";
			} else {
				$errors = array();
				foreach($add->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				$data['type'] = "danger";
				$data['msg'] = $errors[0];
			}
			echo json_encode($data);
		}
	}

	public function authorimagesAction() {
		$images = Images::find("category = 'author' ORDER BY datecreated desc");
		echo json_encode($images->toArray());
	}

	public function authorcreateAction() {
		$request = new \Phalcon\Http\Request();
		if($request->isPost()) {
			$guid = new \Utilities\Guid\Guid();
			$genguid = $guid->GUID();

			$name = $request->getPost('name');
			$location = $request->getPost('location');
			$occupation = $request->getPost('occupation');
			$date = $request->getPost('since');
							// DATE FORMATTING
	            /*if user used the default date and didn't change the date in frontend
	            Already ('Y-m-d') format if unchange */
	            if(strlen($date) == 10) {
	            	$since = $date;
	            }
	            /* If date was changed
	             The format is like this "Thu Sep 17 2015 08:00:00 GMT+0800 (Taipei Standard Time)"
	             and need to convert to ('Y-m-d') before saving */
	             else {
	             	$mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
	             	$dates = explode(" ", $date);
	             	$since = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
	             }
	            //DATE FORMATTING ends
	             $about = $request->getPost('about');
	             $photo = $request->getPost('photo');

	             $create = new Authors();
	             $create->assign(array(
	             	"id" => $genguid,
	             	"name" => $name,
	             	"location" => $location,
	             	"occupation" => $occupation,
	             	"since" => $since,
	             	"about" => $about,
	             	"photo" => $photo,
	             	"datecreated" => date('Y-m-d'),
	             	"dateupdated" => date('Y-m-d H:i:s')
	             	));

	             if($create->create()) {
	             	$data['type'] = "success";
	             	$data['msg'] = "New Author Added.";
	             } else {
	             	foreach($create->getMessages() as $message) {
	             		$errors[] = $message->getMessage();
	             	}
	             	$data['type'] = "danger";
	             	$data['msg'] = $errors[0];
	             }
	             echo json_encode($data);
		} // isPost end
	}

	public function validationauthoruniquenameAction() {
		$request = new \Phalcon\Http\Request();
		if($request->isPost()) {
			$name =  $request->getPost('field');
			$find = Authors::findFirst("name = '".$name."' ");
			if($find == true) {
				$data['isUnique'] = false;
			} else {
				$data['isUnique'] = true;
			}
			echo json_encode($data);
		}
	}

	public function validationauthoruniquenameexceptmeAction() {
		$request = new \Phalcon\Http\Request();
		if($request->isPost()) {
			$name =  $request->getPost('field');
			$find = Authors::findFirst("name = '".$name."' ");
			if($find == true) {
				$data['isUnique'] = false;
			} else {
				$data['isUnique'] = true;
			}
			echo json_encode($data);
		}
	}

	public function authorspaginationAction() {
		$request = new Request();
		if($request->isPost()) {
			$items = $request->getPost('items');
			$page = $request->getPost('page');
			$keyword = $request->getPost('keyword');

			$finaloffset = ($page * $items) - $items;

			if($keyword == null || $keyword == "") {
				$findall = Authors::find();
				$data['TotalItems'] = count($findall);
				if($page > ceil(count($findall)/$items) && $page > 1) {
					$page = $page - 1;
					$finaloffset = $finaloffset - $items;
				}

				$conditions = "id != '' ORDER BY since DESC, name ASC LIMIT ".$finaloffset.", ".$items." ";
				$find = Authors::find(array("conditions"=>$conditions));
				$data['authors'] = $find->toArray();

			} else {
				$conditions = "name LIKE '%".$keyword."%'";
				$findall = Authors::find(array("conditions"=>$conditions));
				$data['TotalItems'] = count($findall);
				if($page > ceil(count($findall)/$items) && $page > 1) {
					$page = $page - 1;
					$finaloffset = $finaloffset - $items;
				}

				$conditions = "name LIKE '%".$keyword."%' ORDER BY since DESC, name ASC LIMIT ".$finaloffset.", ".$items." ";
				$find = Authors::find(array("conditions"=>$conditions));
				$data['authors'] = $find->toArray();
			}

			$data['index'] = $page;
			echo json_encode($data);
		}
	}

	public function authordeleteAction()
	{
		$request = new Request();
		if($request->isPost()) {
			$authorid = $request->getPost('authorid');
			$delete = Authors::findFirst("id = '".$authorid."'");
			if($delete) {
				if($delete->delete()) {
					$data['type'] = "success";
					$data['msg'] = "Author Successfully Deleted";
				} else {
					foreach($delete->getMessages() as $message) {
						$error[] = $message->getMessage();
					}
					$data['type'] = "danger";
					$data['msg'] = $error[0];
				}
			}
			echo json_encode($data);
		}
	}

	public function authorloadAction() {
		$request = new Request();
		if($request->isPost()) {
			$authorid = $request->getPost('authorid');
			$find = Authors::findFirst("id = '".$authorid."'");
			if($find) {
				$data['author'] = $find;
			} else {
				$data['author'] = $find;
				$data['type'] = 'danger';
				$data['msg'] = 'Something went wrong my friend.';
			}
			echo json_encode($data);
		}
	}

	public function authorensureuniquenameAction() {
		$request = new Request();
		if($request->isPost()) {
			$name = $request->getPost('name');
			$id = $request->getPost('id');

			$data['name'] = $name;

			$find = Authors::find("id != '".$id."' AND name = '".$name."'");
			if(count($find) > 0) {
				$data['unique'] = false;
			} else {
				$data['unique'] = true;
			}
			echo json_encode($data);
		}
	}

	public function authorupdateAction()
	{
		$request = new Request();
		if($request->isPost()) {
			$id = $request->getPost('id');
			$name = $request->getPost('name');
			$location = $request->getPost('location');
			$occupation = $request->getPost('occupation');
			$date = $request->getPost('since');
					// DATE FORMATTING
								/*if user used the default date and didn't change the date in frontend
								Already ('Y-m-d') format if unchange */
								if(strlen($date) == 10) {
									$since = $date;
								}
								/* If date was changed
								 The format is like this "Thu Sep 17 2015 08:00:00 GMT+0800 (Taipei Standard Time)"
								 and need to convert to ('Y-m-d') before saving */
								 else {
								 	$mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
								 	$dates = explode(" ", $date);
								 	$since = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
								 }
								//DATE FORMATTING ends
								 $about = $request->getPost('about');
								 $photo = $request->getPost('photo');

								 $update = Authors::findFirst("id = '".$id."'");
								 if(count($update) == 1) {
								 	$update->name = $name;
								 	$update->location = $location;
								 	$update->occupation = $occupation;
								 	$update->since = $since;
								 	$update->about = $about;
								 	$update->photo = $photo;
								 	$update->dateupdated = date('Y-m-d H:i:s');

								 	if($update->save()) {
								 		$data['type'] = "success";
								 		$data['msg'] = "Author Successfully Updated";
								 	} else {
								 		foreach($update->getMessages() as $message) {
								 			$errors[] = $message->getMessage();
								 		}
								 		$data['type'] = "danger";
								 		$data['msg'] = $errors[0];
								 	}
								 }

								 echo json_encode($data);
			} // isPost end
	}

	public function newscreateresourcesAction() {
			$findauthors = Authors::find(array("order" => "name"));
			$data['authors'] = $findauthors->toArray();

			$findcategories = Newscategory::find(array("order" => "categoryname"));
			$data['categories'] = $findcategories->toArray();

			$findtags = Newstags::find(array("order" => "tagname"));
			$data['tags'] = $findtags->toArray();
			echo json_encode($data);
	}

	public function newsloadcategoriesAction() {
			$findcategories = Newscategory::find(array("order" => "categoryname"));
			$data['categories'] = $findcategories->toArray();

			echo json_encode($data);
	}

	public function newsloadtagsAction() {
			$findtags = Newstags::find(array("order" => "tagname"));
			$data['tags'] = $findtags->toArray();

		echo json_encode($data);
	}

	public function newsuploadimageAction() {
			$request = new Request();
			if($request->isPost()) {
				$guid = new Guid();
				$genguid = $guid->GUID();

				$imagename = $request->getPost('imgfilename');
				$category = $request->getPost('category');

				$add = new Images();
				$add->assign(array(
					'id' => $genguid,
					'imagename' => $imagename,
					'category' => $category,
					'datecreated' => date('Y-m-d H:i:s')
					));
				if($add->create()) {
					$data['type'] = "success";
					$data['msg'] = "News image uploaded";
				} else {
					$errors = array();
					foreach($add->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					$data['type'] = "danger";
					$data['msg'] = $errors[0];
				}
				echo json_encode($data);
			}
	}

	public function newsimagesAction() {
			$images = Images::find("category = 'news' ORDER BY datecreated desc");
			echo json_encode($images->toArray());
	}

	public function newsdeleteimageAction() {
			$request = new Request();
			if($request->isPost()) {
				$imgid = $request->getPost('imgid');
				$find = Images::findFirst("id = '".$imgid."' ");
				if($find->delete()) {
					$data['type'] = "success";
					$data['msg'] = "News image deleted";
				} else {
					$data['type'] = "danger";
					$data['msg'] = "Something went wrong deleting the news image";
				}
			}
			echo json_encode($data);
	}

	public function newssaveembedAction() {
			$request = new Request();
			$guid = new Guid();
			if($request->isPost()) {
				$newguid = $guid->GUID();
				$embed = $request->getPost('embed');
				$category = $request->getPost('category');

				$new = new Videocollection();
				$new->id = $newguid;
				$new->embed = $embed;
				$new->category = $category;
				$new->datecreated = date("Y-m-d H:i:s");

				if($new->create()) {
					$data['type'] = "success";
					$data['msg'] = "New Video has been saved.";
				} else {
					foreach($new->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					$data['type'] = "danger";
					$data['msg'] = $errors[0];
				}
			}
			echo json_encode($data);
	}

	public function newsvideosAction() {
		$videos = Videocollection::find("category = 'news' ORDER BY datecreated desc");
			echo json_encode($videos->toArray());
	}

	public function newsdeletevideoAction() {
			$request = new Request();
			if($request->isPost()) {
				$vidid = $request->getPost('vidid');
				$find = Videocollection::findFirst("id = '".$vidid."' ");
				if($find->delete()) {
					$data['type'] = "success";
					$data['msg'] = "News video deleted";
				} else {
					$data['type'] = "danger";
					$data['msg'] = "Something went wrong deleting the news video";
				}
			}
			echo json_encode($data);
	}

	public function newscreateAction() {
			$request = new Request();
			$guid = new Guid();
			$newsid = $guid->GUID();
			if($request->isPost()) {
				$title = $request->getPost('title');
				$slugs = $request->getPost('slugs');
				$author = $request->getPost('author');
				$body = $request->getPost('body');
				$summary = $request->getPost('summary');
				$metatitle = $request->getPost('metatitle');
				$metadesc = $request->getPost('metadesc');
				$metakeyword = $request->getPost('metakeyword');
				$datepublished = $request->getPost('datepublished');
					// DATE FORMATTING
					/*if user used the default date and didn't change the date in frontend
					Already ('Y-m-d') format if unchange */
					if(strlen($datepublished) > 10) {
						$mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
						$dates = explode(" ", $datepublished);
						$datepublished = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
					}
					//DATE FORMATTING ends
					$featimage = $request->getPost('featimage');
					$featvideo = $request->getPost('featvideo');
					$featuredtype = $request->getPost('featuredtype');
					$categories = $request->getPost('categories');
					$tags = $request->getPost('tags');
					$status = $request->getPost('status');

					$addnews = new News();
					$addnews->id = $newsid;
					$addnews->title = $title;
					$addnews->slugs = $slugs;
					$addnews->author = $author;
					$addnews->body = $body;
					$addnews->summary = $summary;
					$addnews->metatitle = $metatitle;
					$addnews->metadesc = $metadesc;
					$addnews->metakeyword = $metakeyword;
					$addnews->featimage = $featimage;
					$addnews->featvideo = $featvideo;
					$addnews->featuredtype = $featuredtype;
					$addnews->status = $status;
					$addnews->datepublished = $datepublished . " ". date('H:i:s');
					$addnews->dateupdated = date('Y-m-d H:i:s');
					if($addnews->create()) {
						foreach($categories as $category) {
							$addcat = new Newsselectedcategories();
							$addcat->categoryid = $category;
							$addcat->newsid = $newsid;
							$addcat->datecreated = date('Y-m-d H:i:s');

							if($addcat->create()) {
								$data['type'] = "success";
								$data['cat_msg'] = "Category has been added";
							} else {
								foreach($addcat->getMessages() as $message) {
									$errors[] = $message->getMessage();
								}
								$data['type'] = "danger";
								$data['cat_msg'] = $errors;
							}
						}

						foreach($tags as $tag) {
							$tag = ucwords(strtolower($tag));
							$checktag = Newstags::findFirst(array(
								"conditions" => "tagname = ?1",
								"bind"       => array(1 => $tag)
								));

							if($checktag == false) { //if tag is NOT in the "newstags" table. it will be added first;
								$newtagid =  $guid->GUID();
								$addtotags = new Newstags();
								$addtotags->id = $newtagid;
								$addtotags->tagname = $tag;
								$addtotags->datecreated = date('Y-m-d');
								$addtotags->dateupdated = date('Y-m-d H:i:s');
								if($addtotags->create()) {
									$data['type'] = "success";
									$data['newtag_msg'] = "Tag has been created";
								} else {
									$data['danger'] = "danger";
									$data['newtag_msg'] = "Error adding new instant tag";
								}

								$addtag = new Newsselectedtags();
								$addtag->tagid = $newtagid;
								$addtag->newsid = $newsid;
								$addtag->datecreated = date('Y-m-d H:i:s');
							} else { //else if tag is ALREADY EXISTING ing the "newstags" table;
								$addtag = new Newsselectedtags();
								$addtag->tagid = $checktag->id;
								$addtag->newsid = $newsid;
								$addtag->datecreated = date('Y-m-d H:i:s');
							}

							if($addtag->create()) {
								$data['type'] = "success";
								$data['tag_msg'] = "Tag has been added";
							} else {
								foreach($addtag->getMessages() as $message) {
									$errors[] = $message->getMessage();
								}
								$data['type'] = "danger";
								$data['tag_msg'] = $errors;
							}
						}

						$data['type'] = "success";
						if($status == 1) { $data['msg'] = "News has been successfully saved."; }
						elseif($status == 0) { $data['msg'] = "News has been successfully saved as draft."; }

				} //$addnews->create ENDS
				else {
					foreach($addnews->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					$data['type'] = "danger";
					$data['msg'] = $errors[0];
				}
			} //isPost ENDS
			echo json_encode($data);
	}

	public function felistallnewsAction($categoryz,$archieve,$tags){
		$datenow = date('Y-m-d H:i:s');
		if($categoryz!=undefined && $archieve==undefined && $tags==undefined){
			$selected = Newsselectedcategories::find('categoryid="'.$categoryz.'"');
			if(count($selected)==0){
				$data['error'] = "nodata";
			}
			else{
				foreach ($selected as $s) {
					$news = News::find('status=1 AND id="'.$s->newsid.'" ORDER by datepublished DESC');
					if(count($news)==0){
						$data['error'] = "nodata";
					}else{
						foreach ($news as $n) {
							$category ='';
							if($n->featuredtype == 'img'){
								$image = Images::findFirst('id="'.$n->featimage.'"');
								$banner = $image->imagename;
							}else{
								$vid = Videocollection::findFirst('id="'.$n->featvideo.'"');
								$x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$vid->embed,$match);
								$img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
								$banner = $img;
							}
							$categories = Newsselectedcategories::find('newsid="'.$n->id.'"');
							foreach ($categories as $cat) {
								$findcategory = Newscategory::findFirst('id="'.$cat->categoryid.'"');
								$category.= $findcategory->categoryname.', ';
							}

							$author = Authors::findFirst('id="'.$n->author.'"');
							$data[] =array(
								'id'			=>$n->id,
								'title'			=>$n->title,
								'slugs'			=>$n->slugs,
								'authorid' =>$author->id,
								'author'		=>$author->name,
								'datepublished' =>$n->datepublished,
								'featuredbanner'=>$banner,
								'type' 			=>$n->featuredtype,
								'category'		=>rtrim($category,', '),
								'summary' => $n->summary
								);
						}
					}
			}
			}

		}
		else if($categoryz==undefined && $archieve!=undefined && $tags==undefined){
			$news = News::find(array("status=1 AND datepublished LIKE '%" . $archieve . "%'","ORDER"=>"datepublished DESC"));
			if(count($news)==0){
				$data['error'] = "nodata";
			}else
			{
				foreach ($news as $n) {
					$category ='';
					if($n->featuredtype == 'img'){
						$image = Images::findFirst('id="'.$n->featimage.'"');
						$banner = $image->imagename;
					}else{
						$vid = Videocollection::findFirst('id="'.$n->featvideo.'"');
						$x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$vid->embed,$match);
						$img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
						$banner = $img;
					}
					$categories = Newsselectedcategories::find('newsid="'.$n->id.'"');
					foreach ($categories as $cat) {
						$findcategory = Newscategory::findFirst('id="'.$cat->categoryid.'"');
						$category.= $findcategory->categoryname.', ';
					}


					$author = Authors::findFirst('id="'.$n->author.'"');
					$data[] =array(
						'id'			=>$n->id,
						'title'			=>$n->title,
						'slugs'			=>$n->slugs,
						'authorid' =>$author->id,
						'author'		=>$author->name,
						'datepublished' =>$n->datepublished,
						'featuredbanner'=>$banner,
						'type' 			=>$n->featuredtype,
						'category'		=>rtrim($category,', '),
						'summary' => $n->summary
						);
				}
			}

		}
		else if($categoryz==undefined && $archieve==undefined && $tags!=undefined){
			$selected = Newsselectedtags::find('tagid="'.$tags.'"');
			if(count($selected)==0){
				$data['error'] = "nodata";
			}
			else{
				foreach ($selected as $s) {
					$news = News::find(array('status=1 AND id="'.$s->newsid.'"','ORDER'=>'datepublished DESC'));
					if(count($news)==0){
						$data['error'] = "nodata";
					}else{
						foreach ($news as $n) {
							$category ='';
							if($n->featuredtype == 'img'){
								$image = Images::findFirst('id="'.$n->featimage.'"');
								$banner = $image->imagename;
							}else{
								$vid = Videocollection::findFirst('id="'.$n->featvideo.'"');
								$x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$vid->embed,$match);
								$img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
								$banner = $img;
							}
							$categories = Newsselectedcategories::find('newsid="'.$n->id.'"');
							foreach ($categories as $cat) {
								$findcategory = Newscategory::findFirst('id="'.$cat->categoryid.'"');
								$category.= $findcategory->categoryname.', ';
							}



							$author = Authors::findFirst('id="'.$n->author.'"');
							$data[] =array(
								'id'			=>$n->id,
								'title'			=>$n->title,
								'slugs'			=>$n->slugs,
								'authorid' =>$author->id,
								'author'		=>$author->name,
								'datepublished' =>$n->datepublished,
								'featuredbanner'=>$banner,
								'type' 			=>$n->featuredtype,
								'category'		=>rtrim($category,', '),
								'summary' => $n->summary
								);
						}
					}
			}
			}
		}
		else{
			$news = News::find('status=1 AND datepublished <= "'.$datenow.'" ORDER by datepublished DESC');
			if(count($news)==0){
				$data['error'] = "nodata";
			}else
			{
				foreach ($news as $n) {
					$category ='';
					if($n->featuredtype == 'img'){
						$image = Images::findFirst('id="'.$n->featimage.'"');
						$banner = $image->imagename;
					}else{
						$vid = Videocollection::findFirst('id="'.$n->featvideo.'"');
						$x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$vid->embed,$match);
						$img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
						$banner = $img;
					}
					$categories = Newsselectedcategories::find('newsid="'.$n->id.'"');
					foreach ($categories as $cat) {
						$findcategory = Newscategory::findFirst('id="'.$cat->categoryid.'"');
						$category.= $findcategory->categoryname.', ';
					}

					$author = Authors::findFirst('id="'.$n->author.'"');
					$data[] =array(
						'id'			=>$n->id,
						'title'			=>$n->title,
						'slugs'			=>$n->slugs,
						'authorid' =>$author->id,
						'author'		=>$author->name,
						'datepublished' =>$n->datepublished,
						'featuredbanner'=>$banner,
						'type' 			=>$n->featuredtype,
						'category'		=>rtrim($category,', '),
						'summary' => $n->summary
						);
				}
			}
		}
		echo json_encode($data);
	}

	public function rssfeedAction(){
		$news = News::find('status=1 order by datepublished DESC');
		foreach ($news as $value) {
			$data[]= array(
                'id'=>  $value->id,
                'title'=>$value->title,
                'slugs'=>$value->slugs,
                'date'=>$value->datepublished,
                'description'=>$value->summary,
                );
		}
		echo json_encode($data);
	}

	public function listarchiveAction() {
        $news = News::find(array('status' => 1, 'order' => 'datepublished DESC'));
        $dates = [];
        foreach ($news as $news)
        {

            if(!in_array(date('F Y', strtotime($news->datepublished)), $dates)){
                $data[] = array(
                    'month' => date('F', strtotime($news->datepublished)),
                    'year' => date('Y', strtotime($news->datepublished)),
                    'datepublished' =>date('Y', strtotime($news->datepublished)).'-'.date('m', strtotime($news->datepublished))
                    );
                array_push($dates, date('F Y', strtotime($news->datepublished)));
            }
        }
        echo json_encode($data);
    }


	public function validationnewsuniqueslugsAction() {
			$request = new \Phalcon\Http\Request();
			if($request->isPost()) {
				$slugs =  $request->getPost('field');
				$find = News::findFirst("slugs = '".$slugs."' ");
				if($find == true) {
					$data['isUnique'] = false;
				} else {
					$data['isUnique'] = true;
				}
				echo json_encode($data);
			}
		}

		public function newspaginationAction() {
			$db = \Phalcon\DI::getDefault()->get('db');
			$request = new Request();
  		if($request->isPost()) {
  			$items = $request->getPost('items');
  			$page = $request->getPost('page');
  			$keyword = $request->getPost('keyword');

  			$finaloffset = ($page * $items) - $items;

  			if($keyword == null || $keyword == "") {
  				$findall = News::find();
  				$data['TotalItems'] = count($findall);
  				if($page > ceil(count($findall)/$items) && $page > 1) {
  					$page = $page - 1;
  					$finaloffset = $finaloffset - $items;
  				}

        	$stmt = $db->prepare("SELECT news.*, authors.name FROM news LEFT JOIN authors ON news.author = authors.id ORDER BY news.datepublished DESC LIMIT ".$finaloffset.", ".$items." ");
        	$stmt->execute();
        	$find = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  				$data['news'] = $find;

  			} else {
					$stmt = $db->prepare("SELECT news.*, authors.name FROM news LEFT JOIN authors ON news.author = authors.id WHERE news.title LIKE '%".$keyword."%' OR authors.name LIKE '%".$keyword."%' ");
        	$stmt->execute();
        	$findall = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  				$data['TotalItems'] = count($findall);
  				if($page > ceil(count($findall)/$items) && $page > 1) {
  					$page = $page - 1;
  					$finaloffset = $finaloffset - $items;
  				}

					$stmt = $db->prepare("SELECT news.*, authors.name FROM news LEFT JOIN authors ON news.author = authors.id WHERE news.title LIKE '%".$keyword."%' OR authors.name LIKE '%".$keyword."%' ORDER BY news.datepublished DESC LIMIT ".$finaloffset.", ".$items." ");
        	$stmt->execute();
        	$find = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  				$data['news'] = $find;
  			}

  			$data['index'] = $page;
  			echo json_encode($data);
  		}
		}

		public function newsdeleteAction() {
			$request = new Request();
			if($request->isPost()) {
				$newsid = $request->getPost('newsid');
				$delete = News::findFirst("id = '".$newsid."'");
				if($delete) {
					if($delete->delete()) {
							$delcats = Newsselectedcategories::find("newsid = '".$newsid."'");
							$deltags = Newsselectedtags::find("newsid = '".$newsid."'");

							foreach($delcats as $cat) {
								if($cat->delete()) {
									$data['type'] = "success";
			  					$data['msg'] = "News was successfully deleted";
								} else {
									$data['type'] = "danger";
			  					$data['msg'] = "Something went wrong on removing selected category";
								}
							}

							foreach($deltags as $tag) {
								if($tag->delete()) {
									$data['type'] = "success";
			  					$data['msg'] = "News was successfully deleted";
								} else {
									$data['type'] = "danger";
			  					$data['msg'] = "Something went wrong on removing selected tag";
								}
							}
  				} else {
	  					foreach($delete->getMessages() as $message) {
	  						$error[] = $message->getMessage();
	  					}
	  					$data['type'] = "danger";
	  					$data['msg'] = $error[0];
  				}
				} else {
					$data['type'] = "danger";
					$data['msg'] = "Something went wrong. News not found";
				}
			}
			echo json_encode($data);
		}

		public function newsinfoAction() {
			$db = \Phalcon\DI::getDefault()->get('db');
			$request = new Request();
			if($request->isPost()) {
				$newsid = $request->getPost('newsid');
				$stmt = $db->prepare("SELECT news.*, authors.id as authorid FROM news LEFT JOIN authors ON news.author = authors.id WHERE news.id = '".$newsid."' ");
				$stmt->execute();
				$find = $stmt->fetch(\PDO::FETCH_ASSOC);

				$data['newsinfo'] = $find;
				$datepublished = date_create($find['datepublished']);
				$data['newsinfo']['datepublished'] = date_format($datepublished, "Y-m-d");

				$selcats = Newsselectedcategories::find("newsid = '".$newsid."' ");
				$categories = array();
				foreach($selcats as $cat) {
					array_push($categories,$cat->categoryid);
				}
				$data['categories'] = $categories;

				$stmt = $db->prepare("SELECT newsselectedtags.tagid, newstags.tagname FROM newsselectedtags LEFT JOIN newstags ON newsselectedtags.tagid = newstags.id WHERE newsselectedtags.newsid = '".$newsid."'");
				$stmt->execute();
				$seltags = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$tags = array();
				foreach($seltags as $tag) {
					array_push($tags,array('id'=>$tag['tagid'], 'tagname'=>$tag['tagname']));
				}
				$data['tags'] = $tags;

				if($find['featuredtype'] == 'img') {
					$findfeat = Images::findFirst("id = '".$find['featimage']."' AND category = 'news' ");
					if($findfeat == true) {
						$data['featimage'] = $findfeat->imagename;
					}
				} elseif ($find['featuredtype'] == 'vid') {
					$findfeat = Videocollection::findFirst("id = '".$find['featvideo']."' AND category = 'news' ");
					if($findfeat == true) {
						$data['featvideo'] = $findfeat->embed;
					}
				}

				echo json_encode($data);
			}
		}

		public function newsensureuniqueslugsAction() {
			$request = new Request();
			if($request->isPost()) {
				$newsid = $request->getPost('newsid');
				$newsslugs = $request->getPost('newsslugs');

				$find = News::findFirst("id != '".$newsid."' AND slugs = '".$newsslugs."' ");
				if($find == true) {
					$data['isUnique'] = false;
				} else {
					$data['isUnique'] = true;
				}
				echo json_encode($data);
			}
		}

		public function newssaveupdateAction() {
			$request = new Request();
			$guid = new Guid();
			if($request->isPost()) {
				$id = $request->getPost('id');
				$title = $request->getPost('title');
				$slugs = $request->getPost('slugs');
				$author = $request->getPost('author');
				$body = $request->getPost('body');
				$summary = $request->getPost('summary');
				$metatitle = $request->getPost('metatitle');
				$metadesc = $request->getPost('metadesc');
				$metakeyword = $request->getPost('metakeyword');
				$datepublished = $request->getPost('datepublished');
					// DATE FORMATTING
					/*if user used the default date and didn't change the date in frontend
					Already ('Y-m-d') format if unchange */
					if(strlen($datepublished) > 10) {
							$mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
							$dates = explode(" ", $datepublished);
							$datepublished = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
					}
					//DATE FORMATTING ends
				$featimage = $request->getPost('featimage');
				$featvideo = $request->getPost('featvideo');
				$featuredtype = $request->getPost('featuredtype');
				$categories = $request->getPost('categories');
				$tags = $request->getPost('tags');
				$status = $request->getPost('status');

				$update = News::findFirst("id = '".$id."' ");
				if($update == true) {
					$update->title = $title;
					$update->slugs = $slugs;
					$update->author = $author;
					$update->body = $body;
					$update->summary = $summary;
					$update->metatitle = $metatitle;
					$update->metadesc = $metadesc;
					$update->metakeyword = $metakeyword;
					$update->featimage = $featimage;
					$update->featvideo = $featvideo;
					$update->featuredtype = $featuredtype;
					$update->status = $status;
					$update->datepublished = $datepublished . " ". date('H:i:s');
					$update->dateupdated = date('Y-m-d H:i:s');

					if($update->save()) {
						//DELETE The old categories & tags
						$delcats = Newsselectedcategories::find("newsid = '".$id."'");
						$deltags = Newsselectedtags::find("newsid = '".$id."'");

						//LOL XD
						foreach($delcats as $cat) {
							if($cat->delete()) {
								$data['type'] = "success";
								$data['msg'] = "News was successfully deleted";
							} else {
								$data['type'] = "danger";
								$data['msg'] = "Something went wrong on removing selected category";
							}
						}

						foreach($deltags as $tag) {
							if($tag->delete()) {
								$data['type'] = "success";
								$data['msg'] = "News was successfully deleted";
							} else {
								$data['type'] = "danger";
								$data['msg'] = "Something went wrong on removing selected tag";
							}
						}
						//DELETE The old categories & tags ENDS

						//SAVE the NEW SET of categories & tags
						foreach($categories as $category) {
							$addcat = new Newsselectedcategories();
								$addcat->categoryid = $category;
								$addcat->newsid = $id;
								$addcat->datecreated = date('Y-m-d H:i:s');

								if($addcat->create()) {
									$data['type'] = "success";
									$data['msg'] = "Category has been added";
								} else {
									foreach($addcat->getMessages() as $message) {
										$errors[] = $message->getMessage();
									}
									$data['type'] = "danger";
									$data['msg'] = $errors;
								}
						}

						foreach($tags as $tag) {
							$tag = ucwords(strtolower($tag));
							$checktag = Newstags::findFirst(array(
								"conditions" => "tagname = ?1",
								"bind"       => array(1 => $tag)
								));

							if($checktag == false) { //if tag is NOT in the "newstags" table. it will be added first;
								$newtagid =  $guid->GUID();
								$addtotags = new Newstags();
								$addtotags->id = $newtagid;
								$addtotags->tagname = $tag;
								$addtotags->datecreated = date('Y-m-d');
								$addtotags->dateupdated = date('Y-m-d H:i:s');
								if($addtotags->create()) {
									$data['type'] = "success";
									$data['newtag_msg'] = "Tag has been created";
								} else {
									$data['danger'] = "danger";
									$data['newtag_msg'] = "Error adding new instant tag";
								}

								$addtag = new Newsselectedtags();
								$addtag->tagid = $newtagid;
								$addtag->newsid = $id;
								$addtag->datecreated = date('Y-m-d H:i:s');
							} else { //else if tag is ALREADY EXISTING ing the "newstags" table;
								$addtag = new Newsselectedtags();
								$addtag->tagid = $checktag->id;
								$addtag->newsid = $id;
								$addtag->datecreated = date('Y-m-d H:i:s');
							}

							if($addtag->create()) {
								$data['type'] = "success";
								$data['tag_msg'] = "Tag has been added";
							} else {
								foreach($addtag->getMessages() as $message) {
									$errors[] = $message->getMessage();
								}
								$data['type'] = "danger";
								$data['tag_msg'] = $errors;
							}
						}
						//SAVE the NEW SET of categories & tags ENDS

						$data['type'] = "success";
						if($status == 1) { $data['msg'] = "News has been successfully saved."; }
						elseif($status == 0) { $data['msg'] = "News has been successfully saved as draft."; }

					} else {
						foreach($update->getMessages() as $message) {
							$errors[] = $message->getMessage();
						}
						$data['type'] = "danger";
						$data['msg'] = $errors;
					}
				} else {
					$data['type'] = "danger";
					$data['msg'] = "Something went wrong. News not found";
				}
				echo json_encode($data);
			}
		}

		public function authordeleteimageAction() {
			$request = new Request();
			if($request->isPost()) {
				$imgid = $request->getPost('imgid');
				$find = Images::findFirst("id = '".$imgid."' ");
				if($find->delete()) {
					$data['type'] = "success";
					$data['msg'] = "Author image deleted";
				} else {
					$data['type'] = "danger";
					$data['msg'] = "Something went wrong deleting the author image";
				}
			}
			echo json_encode($data);
		}

		public function feblogviewbynewsslugsAction($newsslugs) {
			$db = \Phalcon\DI::getDefault()->get('db');
			$stmt = $db->prepare("SELECT news.*, authors.id as authorid, authors.name FROM news LEFT JOIN authors ON news.author = authors.id WHERE news.slugs = '".$newsslugs."' ");
			$stmt->execute();
			$find = $stmt->fetch(\PDO::FETCH_ASSOC);
				$datepublished = date_create($find['datepublished']); //Y-m-d H:i:s
				$find['datepublished'] = date_format($datepublished, "Y-m-d");

				if($find['featuredtype'] == 'img') {
					$findfeat = Images::findFirst("id = '".$find['featimage']."' AND category = 'news' ");
					if($findfeat == true) {
						$find['video'] = null;
						$find['image'] = $findfeat->imagename;
					}
				} elseif ($find['featuredtype'] == 'vid') {
					$findfeat = Videocollection::findFirst("id = '".$find['featvideo']."' AND category = 'news' ");
					if($findfeat == true) {
						$find['video'] = $findfeat->embed;
						$find['image'] = null;
					}
				}

				$stmt = $db->prepare("SELECT newscategory.id, newscategory.categoryname FROM newsselectedcategories LEFT JOIN newscategory ON newsselectedcategories.categoryid = newscategory.id WHERE newsid = '".$find['id']."' ");
				$stmt->execute();
				$selcats = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$find['categories'] = $selcats;

				$stmt = $db->prepare("SELECT newstags.id, newstags.tagname FROM newsselectedtags LEFT JOIN newstags ON newsselectedtags.tagid = newstags.id WHERE newsid = '".$find['id']."' ");
				$stmt->execute();
				$seltags = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$find['tags'] = $seltags;

				$author = Authors::findFirst("id = '".$find['author']."' ");
				$find['author'] = $author;

			echo json_encode($find);
		}

		public function homeblogsAction() {
			$datenow = date('Y-m-d H:i:s');

			$db = \Phalcon\DI::getDefault()->get('db');
			$stmt = $db->prepare("SELECT * FROM news  WHERE datepublished <= '".$datenow."' AND status = 1 ORDER BY datepublished DESC LIMIT 3");
			$stmt->execute();
			$blogs = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			foreach($blogs as $key => $value) {
				if($blogs[$key]['featuredtype'] == 'img') {
					$findfeat = Images::findFirst("id = '".$blogs[$key]['featimage']."' AND category = 'news' ");
					if($findfeat == true) {
						$blogs[$key]['featured'] = $findfeat->imagename;
					}
				} elseif ($blogs[$key]['featuredtype'] == 'vid') {
					$findfeat = Videocollection::findFirst("id = '".$blogs[$key]['featvideo']."' AND category = 'news' ");
					if($findfeat == true) {
						$blogs[$key]['featured'] = $findfeat->embed;
					}
				}

				$datepublished = date_create($blogs[$key]['datepublished']);
				$blogs[$key]['datepublished'] = date_format($datepublished, "F d, Y");
			}
			$data['blogs'] = $blogs;

			echo json_encode($data);
		}

		public function feblogauthorAction($authorid, $items) {
			$datenow = date('Y-m-d H:i:s');
			$db = \Phalcon\DI::getDefault()->get('db');

			$find = Authors::findFirst("id = '".$authorid."' ");
			$data['author'] = $find;

			$data['items'] = $items;

			// $myblogs = News::find("author = '"$find->author"' AND datepublished <= '".$datenow."' AND status = 1 ");
			$stmt = $db->prepare("SELECT news.*, authors.id as authorid, authors.name FROM news LEFT JOIN authors ON news.author = authors.id WHERE news.author = '".$find->id."' AND datepublished <= '".$datenow."' ");
			$stmt->execute();
			$mytotalblogs = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			$data['mytotalblogs'] = $mytotalblogs;



			$stmt = $db->prepare("SELECT news.*, authors.id as authorid, authors.name FROM news LEFT JOIN authors ON news.author = authors.id WHERE news.author = '".$find->id."' AND datepublished <= '".$datenow."' ORDER BY datepublished DESC LIMIT 0, ".$items." ");
			$stmt->execute();
			$myblogs = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			$data['myblogs'] = $myblogs;

			$data['featured'] = array();
			$data['datepublished'] = array();
			foreach($myblogs as $key => $value) {
				$datepublished = date_create($myblogs[$key]['datepublished']);
				$myblogs[$key]['datepublished'] = date_format($datepublished, "F d, Y");
				$data['datepublished'][] = date_format($datepublished, "F d, Y");

				if($myblogs[$key]['featuredtype'] == 'img') {
					$findfeat = Images::findFirst("id = '".$myblogs[$key]['featimage']."' AND category = 'news' ");
					if($findfeat == true) {
						$data['featured'][] = $findfeat->imagename;
					} else {
						$data['featured'][] = "atvlogo.png";
					}
				} elseif ($myblogs[$key]['featuredtype'] == 'vid') {
					$findfeat = Videocollection::findFirst("id = '".$myblogs[$key]['featvideo']."' AND category = 'news' ");
					if($findfeat == true) {
						// $data['featured'][] = $findfeat->embed;
						$x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$findfeat->embed,$match);
						$img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
						$data['featured'][] = $img;
					} else {
						$data['featured'][] = "atvlogo.png";
					}
				}

				$stmt = $db->prepare("SELECT * FROM newsselectedcategories LEFT JOIN newscategory ON newsselectedcategories.categoryid = newscategory.id WHERE newsselectedcategories.newsid =  '".$myblogs[$key]['id']."' ");
				$stmt->execute();
				$selcats = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				// $selcats = Newsselectedcategories::find("newsid = '".$myblogs[$key]['id']."' ");
				$categories[$key] = array();
				foreach($selcats as $cat) {
					array_push($categories[$key],array(id=>$cat['categoryid'], name=>$cat['categoryname']));
				}

			}
			$data['categories'] = $categories;

			echo json_encode($data);
		}

		public function previewAction() {
			$request = new Request();
			if($request->isPost()) {
				$authorid = $request->getPost('authorid');
				$categoriesid = $request->getPost('categoriesid');
				$tagsid = $request->getPost('tagsid');
				$featuredtype = $request->getPost('featuredtype');
				$imgid = $request->getPost('imgid');
				$vidid = $request->getPost('vidid');

				$author = Authors::findFirst("id = '" . $authorid."'");
				$data['author'] = $author;

				$categories = array();
				foreach($categoriesid as $catid) {
					$cat = Newscategory::findFirst("id = '".$catid."'");
					array_push($categories,array(id=>$cat->id, name=>$cat->categoryname));
				}
				$data['categories'] = $categories;

				$tags = array();
				foreach($tagsid as $tagid) {
					$tag = Newstags::findFirst("id = '".$tagid."'");
					array_push($tags,array(id=>$tag->id, name=>$tag->tagname));
				}
				$data['tags'] = $tags;

				if($featuredtype == 'img') {
					$findfeat = Images::findFirst("id = '".$imgid."' AND category = 'news' ");
					if($findfeat == true) {
						$data['featured'] = $findfeat->imagename;
					} else {
						$data['featured'] = "atvlogo.png";
					}
				} elseif ($featuredtype == 'vid') {
					$findfeat = Videocollection::findFirst("id = '".$vidid."' AND category = 'news' ");
					if($findfeat == true) {
						// $data['featured'][] = $findfeat->embed;
						$data['featured'] = $findfeat->embed;
					} else {
						$data['featured'] = "atvlogo.png";
					}
				}
			}
			echo json_encode($data);
		}

		public function authorattemptdeleteAction() {
			$db = \Phalcon\DI::getDefault()->get('db');
			$request = new Request();
			if($request->isPost()) {
				$page = $request->getPost("page");
				$authorid = $request->getPost("authorid");
				$items = 10; //static
				$finaloffset = ($page * $items) - $items;

				$stmt = $db->prepare("SELECT news.id, news.title, authors.id as authorid, authors.name FROM news LEFT JOIN authors ON news.author = authors.id WHERE news.author = '".$authorid."'");
				$stmt->execute();
				$findall = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$data['totalitems'] = count($findall);
				if($page > ceil(count($findall)/$items) && $page > 1) {
					$page = $page - 1;
					$finaloffset = $finaloffset - $items;
				}
				//
				$stmt = $db->prepare("SELECT news.id, news.title, authors.id as authorid, authors.name FROM news LEFT JOIN authors ON news.author = authors.id WHERE news.author = '".$authorid."' LIMIT ".$finaloffset.", ".$items);
				$stmt->execute();
				$news = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				if($news == true) {
					$data['authornews'] = $news;
				}

				$authorlist = Authors::find(array(columns=>"id, name"));
				$data['authorlist'] = $authorlist->toArray();
				$data['index'] = $page;
			}
			echo json_encode($data);
		}

		public function newschangestatusAction() {
			$request = new Request();
			if($request->isPost()) {
				$newsid = $request->getPost("id");
				$status = $request->getPost("status");

				$news = News::findFirst("id ='".$newsid."'");
				if($news == true) {
					$news->status = $status;
					if($news->save()) {
						$data['msg'] = "News status has been changed.";
					} else {
						$data['msg'] = "Something went wrong";
					}
				}
			}
			echo json_encode($data);
		}

		public function newsupdateauthorAction() {
			$request = new Request();
			if($request->isPost()) {
				$newsid = $request->getPost("id");
				$authorid = $request->getPost("authorid");

				$find = News::findFirst("id = '".$newsid."'");
				if($find == true) {
					$find->author = $authorid;
					if($find->save()) {
						$data['type'] = "success";
						$data['msg'] = "News' Author successfully updated";
					} else {
						$data['type'] = "danger";
						$data['msg'] = "Something went wrong!";
					}

				} else {
					$data['type'] = "danger";
					$data['msg'] = "News NOT found!";
				}
			} else {
				$data['type'] = "danger";
				$data['msg'] = "Something went wrong. Please try again";
			}
			echo json_encode($data);
		}

		public function categoryattemptdeleteAction() {
			$db = \Phalcon\DI::getDefault()->get('db');
			$request = new Request();
			if($request->isPost()) {
				$page = $request->getPost("page");
				$categoryid = $request->getPost("categoryid");
				$items = 10; //static
				$finaloffset = ($page * $items) - $items;

				$find_this_category = Newsselectedcategories::find("categoryid = '".$categoryid."'");
				if($find_this_category == true) { //USED category
					$editable_news = array();
					foreach($find_this_category as $category) {
						$newsid = $category->newsid; //get the NEWSID
						$find_this_news = Newsselectedcategories::find("newsid = '".$newsid."'");
						if(count($find_this_news) == 1) { //it will replace first its ONLY CATEGORY
							$conditions = "id = '".$newsid."'";
							$get_news = News::findFirst(array('conditions'=>$conditions, 'columns'=>"id, title"));
							array_push($editable_news, $get_news);
						}
					}

				}

				$data['totalitems'] = count($editable_news);
				if($page > ceil(count($editable_news)/$items) && $page > 1) {
					$page = $page - 1;
					$finaloffset = $finaloffset - $items;
				}

				//pagination -> array_slice
				$data['editable_news'] = array_slice($editable_news, $finaloffset, $items);

				$categorylist = Newscategory::find("id != '".$categoryid."'");
				$data['categorylist'] = $categorylist->toArray();
				$data['index'] = $page;
			}
			echo json_encode($data);
		}

		public function newsupdatecategoryAction() {
			$request = new Request();
			if($request->isPost()) {
				$id = $request->getPost("id");
				$categories = $request->getPost("categories");

				//DELETE The old categories of the news
				$delcats = Newsselectedcategories::find("newsid = '".$id."'");
				if($delcats == true) {
					if($delcats->delete()) {
						$data['type'] = "success";
						$data['del_msg'] = "Selected category has been deleted.";
					} else {
						$data['type'] = "danger";
						$data['del_msg'] = "Selected category NOT deleted!";
					}
				}

			//SAVE the NEW SET of news category
				foreach($categories as $category) {
					$addcat = new Newsselectedcategories();
						$addcat->categoryid = $category;
						$addcat->newsid = $id;
						$addcat->datecreated = date('Y-m-d H:i:s');

						if($addcat->create()) {
							$data['type'] = "success";
							$data['msg'] = "Successfully updated.";
						} else {
							foreach($addcat->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							$data['type'] = "danger";
							$data['msg'] = $errors;
						}
				}
			}
			echo json_encode($data);
		}

		public function tagattemptdeleteAction() {
			$db = \Phalcon\DI::getDefault()->get('db');
			$request = new Request();
			if($request->isPost()) {
				$page = $request->getPost("page");
				$tagid = $request->getPost("tagid");
				$items = 10; //static
				$finaloffset = ($page * $items) - $items;

				$find_this_tag = Newsselectedtags::find("tagid = '".$tagid."'");
				if($find_this_tag == true) { //USED category
					$editable_news = array();
					foreach($find_this_tag as $tag) {
						$newsid = $tag->newsid; //get the NEWSID
						$find_this_news = Newsselectedtags::find("newsid = '".$newsid."'");
						if(count($find_this_news) == 1) { //it will replace first its ONLY CATEGORY
							$conditions = "id = '".$newsid."'";
							$get_news = News::findFirst(array('conditions'=>$conditions, 'columns'=>"id, title"));
							array_push($editable_news, $get_news);
						}
					}

				}

				$data['totalitems'] = count($editable_news);
				if($page > ceil(count($editable_news)/$items) && $page > 1) {
					$page = $page - 1;
					$finaloffset = $finaloffset - $items;
				}

				//pagination -> array_slice
				$data['editable_news'] = array_slice($editable_news, $finaloffset, $items);

				$taglist = Newstags::find(array("id != '".$tagid."'", columns => "tagname"));
				$data['taglist'] = $taglist->toArray();
				$data['index'] = $page;
			}
			echo json_encode($data);
		}

		public function newsupdatetagAction() {
			$guid = new Guid();
			$request = new Request();
			if($request->isPost()) {
				$id = $request->getPost("id");
				$tags = $request->getPost("tags");

				$deltags = Newsselectedtags::find("newsid = '".$id."'");
				if($deltags == true) {
					if($deltags->delete()) {
						$data['type'] = "success";
						$data['del_msg'] = "Selected category has been deleted.";

						foreach($tags as $tag) {
							$tag = ucwords(strtolower($tag));
							$checktag = Newstags::findFirst(array(
								"conditions" => "tagname = ?1",
								"bind"       => array(1 => $tag)
							));

							if($checktag == false) { //if tag is NOT in the "newstags" table. it will be added first;
								$newtagid =  $guid->GUID();
								$addtotags = new Newstags();
								$addtotags->id = $newtagid;
								$addtotags->tagname = $tag;
								$addtotags->datecreated = date('Y-m-d');
								$addtotags->dateupdated = date('Y-m-d H:i:s');
								if($addtotags->create()) {
									$data['type'] = "success";
									$data['newtag_msg'] = "Tag has been created";
								} else {
									$data['danger'] = "danger";
									$data['newtag_msg'] = "Error adding new instant tag";
								}

								$addtag = new Newsselectedtags();
								$addtag->tagid = $newtagid;
								$addtag->newsid = $id;
								$addtag->datecreated = date('Y-m-d H:i:s');
							} else { //else if tag is ALREADY EXISTING ing the "newstags" table;
								$addtag = new Newsselectedtags();
								$addtag->tagid = $checktag->id;
								$addtag->newsid = $id;
								$addtag->datecreated = date('Y-m-d H:i:s');
							}

							if($addtag->create()) {
								$data['type'] = "success";
								$data['msg'] = "News tag has been successfully updated.";
							} else {
								foreach($addtag->getMessages() as $message) {
									$errors[] = $message->getMessage();
								}
								$data['type'] = "danger";
								$data['msg'] = $errors;
							}
						}

					} else {
						$data['type'] = "danger";
						$data['del_msg'] = "Selected category NOT deleted!";
					}
				}

			}
			echo json_encode($data);
		}
 }
