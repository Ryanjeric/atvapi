<?php

namespace Controllers;

use \Models\Users as Users;
use \Models\Forgotpassword as Forgotpasswords;
use \Models\Userimage as Image;
use \Models\Userrole as Userrole;
use \Controllers\ControllerBase as CB;

class UserController extends \Phalcon\Mvc\Controller {


    public function userExistAction($name) {
        if(!empty($name)) {
            $user = Users::findFirst('username="' . $name . '"');
            if ($user) {
                echo json_encode(array('exists' => true));
            } else {
                echo json_encode(array('exists' => false));
            }
        }

    }
    public function emailExistAction($name)
    {
        if (!empty($name)) {
            $user = Users::findFirst('email="' . $name . '"');
            if ($user) {
                echo json_encode(array('exists' => true));
            } else {
                echo json_encode(array('exists' => false));
            }
        }
    }
    public function activationAction()
    {
        if (!empty($_POST['code'])) {
            $code = $_POST['code'];
            $user = Users::findFirst('activation_code="' . $code . '"');
            if (isset($user->activation_code)) {
                $user->activation_code = '';
                $user->status = 1;
                if($user->save()){
                    echo json_encode(array('success' => 'Your account have been fully activated. You can now login.'));
                }else{
                    echo json_encode(array('error' => 'Cannot update record.'));
                }
            } else {
                echo json_encode(array('error' => 'No activation code found.'));
            }
        }
    }
    public function registerUserAction($user){
        $request = new \Phalcon\Http\Request();
        //var_dump($request->getPost('username'));
        if($request->isPost()){
            $username = $request->getPost('username');
            $email = $request->getPost('email');
            $password = $request->getPost('password');
            $firstname = $request->getPost('firstname');
            $lastname = $request->getPost('lastname');
            $gender = $request->getPost('gender');
            $task = $request->getPost('userrole');
            $country = $request->getPost('country');
            $state = $request->getPost('state');
            $month = $request->getPost('month');
            $day = $request->getPost('day');
            $year = $request->getPost('year');

            /* Register User*/
            $guid = new \Utilities\Guid\Guid();
            $usave = new Users();
            $usave->id = $guid->GUID();
            $usave->username = $username;
            $usave->email = $email;
            $usave->password = sha1($password);
            $usave->referal_code = $referalcode;
            $usave->source_info = $source;
            $usave->subscribe_newsletter = $newsletter;
            $usave->first_name = $firstname;
            $usave->last_name = $lastname;
            $usave->gender = $gender;
            $usave->country = $country;
            $usave->state = $state;
            $usave->status = 0;
            $usave->birthday = $year . '-' . $month . '-' . $day;
            $usave->activation_code = $code = $guid->GUID();
            $usave->created_at = date("Y-m-d H:i:s");
            $usave->updated_at = date("Y-m-d H:i:s");

            if(!$usave->create()){
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            }else{
                $app = new CB();
                $content = 'Welcome to Planet Impossible, '.$firstname. ' '. $lastname . ' <br>  <br> Please click the confirmation link below to complete and activate your account.
                <br><br>  Link: <br><a href="' . $app->getConfig()->application->baseURL . '/registration/activation/'.$code.'">' . $app->getConfig()->application->baseURL . '/registration/activation/'.$code.'</a><br><br> <br><br>Thanks,<br><br>PI Staff';
                $app->sendMail($email, 'PI Confirmation Email', $content);
                echo json_encode(array('success' => 'You are now successfuly registered.'));
            }
        }else{
            echo json_encode(array('error' => 'No post data.'));
        }
    }

    public function loginAction() {

        $request = new \Phalcon\Http\Request();
        $user = Users::findFirst('email="' .  $request->getPost('email').'" AND  password="'. sha1($request->getPost('password')).'" AND status=1');
        if($user) {
            $jwt = new \Security\Jwt\JWT();
            $payload = array(
                "id" => $user->id,
                "username" => $user->username,
                "lastname" => $user->last_name,
                "firstname" => $user->first_name,
                "exp" => time() + (60 * 60)); // time in the future
            $token = $jwt->encode($payload, $app->config->hashkey);
            echo json_encode(array('data' => $token));
        }else{
            echo json_encode(array('error' => 'Username or Password is invalid.'));
        }
    }

    //IMAGE UPLOAD
    public function saveimageAction() {

        $filename = $_POST['imgfilename'];

        $picture = new Image();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
     
    }
    //IMAGE DELETE
    public function deleteimageAction($imgid) {
        $img = Image::findFirst('id="'. $imgid.'"');
        $filename = $img->filename;
        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }


    public function listimageAction() {

        $getimages = Image::find(array("order" => "id DESC"));
        if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
        foreach ($getimages as $getimages) 
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
        }
        echo json_encode($data);

    }




    public function addUserAction($user){
        $request = new \Phalcon\Http\Request();
        //var_dump($request->getPost('username'));
        if($request->isPost()){
            $username = $request->getPost('username');
            $email = $request->getPost('email');
            $password = $request->getPost('password');
            $firstname = $request->getPost('fname');
            $lastname = $request->getPost('lname');
            $gender = $request->getPost('gender');
            $task = $request->getPost('userrole');
            $bday = $request->getPost('bday');
            $status = $request->getPost('status');
            $profilepic = $request->getPost('banner');
            $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
            $dates = explode(" ", $bday);
            $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

            /* Register User*/
            $guid = new \Utilities\Guid\Guid();
            $id =$guid->GUID();
            $usave = new Users();
            $usave->id = $id;
            $usave->username = $username;
            $usave->email = $email;
            $usave->password = sha1($password);
            $usave->first_name = $firstname;
            $usave->last_name = $lastname;
            $usave->task = 'ADMIN';
            $usave->profile_pic_name = $profilepic;
            $usave->gender = $gender;
            $usave->status = $status;
            $usave->birthday = $d;
            $usave->activation_code = $guid->GUID();
            $usave->created_at = date("Y-m-d H:i:s");
            $usave->updated_at = date("Y-m-d H:i:s");


            if($request->getPost('superadmin')!='false' && $request->getPost('superadmin')!=NULL)
            {
              $saverole = new Userrole();
              $saverole->assign(array(
                'userid' => $id,
                'role' => 'superadmin'
                ));
              if (!$saverole->save()) {
                  $data['error']=array('Something went wrong saving the data, please try again.');
              }
            }

            if($request->getPost('role1')!='false' && $request->getPost('role1')!=NULL)
            {
              $saverole = new Userrole();
              $saverole->assign(array(
                'userid' => $id,
                'role' => 'Users'
                ));
              if ($saverole->save()) {
                  $data['error']=array($request->getPost('role1'));
              }
             }

             if($request->getPost('role2')!='false' && $request->getPost('role2')!=NULL)
            {
              $saverole = new Userrole();
              $saverole->assign(array(
                'userid' => $id,
                'role' => 'Gallery'
                ));
              if (!$saverole->save()) {
                  $data['error']=array('Something went wrong saving the data, please try again.');
              }
             }

              if($request->getPost('role3')!='false' && $request->getPost('role3')!=NULL)
            {
              $saverole = new Userrole();
              $saverole->assign(array(
                'userid' => $id,
                'role' => 'News'
                ));
              if (!$saverole->save()) {
                  $data['error']=array('Something went wrong saving the data, please try again.');
              }
             }

              if($request->getPost('role4')!='false' && $request->getPost('role4')!=NULL)
            {
              $saverole = new Userrole();
              $saverole->assign(array(
                'userid' => $id,
                'role' => 'Contacts'
                ));
              if (!$saverole->save()) {
                  $data['error']=array('Something went wrong saving the data, please try again.');
              }
             }

              if($request->getPost('role5')!='false' && $request->getPost('role5')!=NULL)
            {
              $saverole = new Userrole();
              $saverole->assign(array(
                'userid' => $id,
                'role' => 'ATV'
                ));
              if (!$saverole->save()) {
                  $data['error']=array('Something went wrong saving the data, please try again.');
              }
             }

             if($request->getPost('role6')!='false' && $request->getPost('role6')!=NULL)
            {
              $saverole = new Userrole();
              $saverole->assign(array(
                'userid' => $id,
                'role' => 'Trails'
                ));
              if (!$saverole->save()) {
                  $data['error']=array('Something went wrong saving the data, please try again.');
              }
             }

             if($request->getPost('role7')!='false' && $request->getPost('role7')!=NULL)
            {
              $saverole = new Userrole();
              $saverole->assign(array(
                'userid' => $id,
                'role' => 'Reservations'
                ));
              if (!$saverole->save()) {
                  $data['error']=array('Something went wrong saving the data, please try again.');
              }
             }

              if($request->getPost('role8')!='false' && $request->getPost('role8')!=NULL)
            {
              $saverole = new Userrole();
              $saverole->assign(array(
                'userid' => $id,
                'role' => 'Meta'
                ));
              if (!$saverole->save()) {
                  $data['error']=array('Something went wrong saving the data, please try again.');
              }
             }

               if($request->getPost('role9')!='false' && $request->getPost('role9')!=NULL)
            {
              $saverole = new Userrole();
              $saverole->assign(array(
                'userid' => $id,
                'role' => 'Settings'
                ));
              if (!$saverole->save()) {
                  $data['error']=array('Something went wrong saving the data, please try again.');
              }
             }



            if(!$usave->create()){
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            }else{
                /*$app = new CB();
                $content = 'Welcome to Planet Impossible, '.$firstname. ' '. $lastname . ' <br>  <br> Please click the confirmation link below to complete and activate your account.
                <br><br>  Link: <br><a href="' . $app->getConfig()->application->baseURL . '/registration/activation/'.$code.'">' . $app->getConfig()->application->baseURL . '/registration/activation/'.$code.'</a><br><br> <br><br>Thanks,<br><br>PI Staff';
                $app->sendMail($email, 'PI Confirmation Email', $content);*/
                echo json_encode(array('success' => 'You are now successfuly registered...'));
            }
        }else{
            echo json_encode(array('error' => 'No post data.'));
        }
    }

     public function userListAction($num, $page, $keyword,$role) {
        $offsetfinal = ($page * $num) - $num;
        if ($keyword == 'undefined' && $role == 'undefined') {
            $query = "SELECT * FROM users ORDER by 'task' LIMIT $offsetfinal, $num";
            $userlist = CB::atvQuery($query);

            $totalNumber = count($userlist);

            if($page > ceil(count($totalNumber)/$num) && $page > 1) {
                $page = $page - 1;
                $offsetfinal = $offsetfinal - $num;
            }
        } 
        else if($role != 'undefined' && $keyword == 'undefined'){
            $query = "SELECT users.* FROM userrole LEFT JOIN users on userrole.userid = users.id WHERE role='".$role."'";
            $user = CB::atvQuery($query);

            $totalNumber = count($user);

            if($page > ceil(count($totalNumber)/$num) && $page > 1) {
                $page = $page - 1;
                $offsetfinal = $offsetfinal - $num;
            }
            $query1 = "SELECT users.* FROM userrole LEFT JOIN users on userrole.userid = users.id WHERE role='".$role."' LIMIT $offsetfinal, $num";
            $userlist = CB::atvQuery($query1);
        }
        else if($role != 'undefined' && $keyword != 'undefined'){
            $query = "SELECT users.* FROM userrole LEFT JOIN users on userrole.userid = users.id WHERE role='".$role."' AND username LIKE '%$keyword%' 
            or role='".$role."' AND first_name LIKE '%$keyword%'
            or role='".$role."' AND last_name LIKE '%$keyword%'
            or role='".$role."' AND email LIKE '%$keyword%'";
            $user = CB::atvQuery($query);

            $totalNumber = count($user);

            if($page > ceil(count($totalNumber)/$num) && $page > 1) {
                $page = $page - 1;
                $offsetfinal = $offsetfinal - $num;
            }
            $query1 = "SELECT users.* FROM userrole LEFT JOIN users on userrole.userid = users.id WHERE role='".$role."' AND username LIKE '%$keyword%' 
            or role='".$role."' AND first_name LIKE '%$keyword%'
            or role='".$role."' AND last_name LIKE '%$keyword%'
            or role='".$role."' AND email LIKE '%$keyword%' LIMIT $offsetfinal, $num";
            $userlist = CB::atvQuery($query1);
        }
        else if($role == 'undefined' && $keyword != 'undefined') {
            $query = "SELECT * FROM users WHERE username LIKE '%" . $keyword . "%' 
            or first_name LIKE '%" . $keyword . "%'
            or last_name LIKE '%" . $keyword . "%'
            or email LIKE '%" . $keyword . "%' ORDER by 'task'";
            $user = CB::atvQuery($query);

            $totalNumber = count($user);

            if($page > ceil(count($totalNumber)/$num) && $page > 1) {
                $page = $page - 1;
                $offsetfinal = $offsetfinal - $num;
            }

            $query1 = "SELECT * FROM users WHERE username LIKE '%$keyword%' 
            or first_name LIKE '%$keyword%'
            or last_name LIKE '%$keyword%'
            or email LIKE '%$keyword%' ORDER by 'task' LIMIT $offsetfinal, $num";
            $userlist = CB::atvQuery($query1);
        }
        echo json_encode(array('data' => $userlist,'index' => $page, 'before' => $page->before, 'next' => $page->next, 'last' => $offsetfinal, 'total_items' => $totalNumber));
    }


    public function changestatusAction($id,$status){
        $getInfo = Users::findFirst('id="'. $id .'"');
        $username = $getInfo->username;
        if($status == 1){
           $getInfo->status = 0;
           $getInfo->save();
           $data=array('success' => 'Deactivated');
        }
        else{
           $getInfo->status = 1;
           $getInfo->save();
           $data=array('success' => 'Activated');
        }
        echo json_encode($data);
    }

    public function deleteUserAction($id){
        $dlt = Users::findFirst('id="' . $id . '"');
        $deleteroles = Userrole::find('userid="'.$id.'"');
        if ($dlt) {
            if($dlt->delete() && $deleteroles->delete()){
                $data = array('success' => 'User has Been deleted');
            }else {
                $data = array('error' => 'Not Found');
            }
        }

    }

    public function userInfoction($id) {
        $getInfo = Users::findFirst('id="'. $id .'"');
        $data = array(
            'id' =>  $getInfo->id,
            'username' =>  $getInfo->username,
            'email' =>  $getInfo->email,
            'userrole' =>  trim($getInfo->task),
            'fname' =>  $getInfo->first_name,
            'lname' =>  $getInfo->last_name,
            'bday' =>  $getInfo->birthday,
            'gender' => $getInfo->gender,
            'status' => $getInfo->status,
            'banner' => $getInfo->profile_pic_name
            );

        $getroles = Userrole::find('userid="'.$id.'"');
        foreach($getroles as $value) {
            $data2[] = array(
                'id' => $value->id,
                'userid' => $value->userid,
                'role' => $value->role
            );
        }

        echo json_encode(array('data'=>$data,'data2'=>$data2));
    }

    public function userUpdateAction() {
        $request = new \Phalcon\Http\Request();
        //var_dump($request->getPost('username'));
        if($request->isPost()){
            //VARIABLE
            $id         = $request->getPost('id');
            $username   = $request->getPost('username');
            $email      = $request->getPost('email');
            $password   = $request->getPost('password');
            $firstname  = $request->getPost('fname');
            $lastname   = $request->getPost('lname');
            $bday       = $request->getPost('bday');
            $hiddendate= $request->getPost('hiddendate');
            $gender     = $request->getPost('gender');
            $status     = $request->getPost('status');
            $banner     = $request->getPost('banner');
            $oldpassword= $request->getPost('oldpassword');
            $newpassword= $request->getPost('newpassword');
            $password_c = $request->getPost('password_c');

            $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
            if(strlen($bday) <= 11){
                $d = $hiddendate;
            }else{
                $dates = explode(" ", $bday);
                $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
            }


            if($oldpassword != '' && sha1($newpassword) == sha1($password_c))
            {
                    $getInfo = Users::findFirst('id="'. $id .'" and password="'. sha1($oldpassword).'"');
                    if(strlen($newpassword)<5){
                        $data=array('success' => 'TOOSHORT');
                    }
                    elseif($getInfo){
                        //SAVE
                        $usave = Users::findFirst('id="' . $id . '"');
                        $usave->username        = $username;
                        $usave->email           = $email;
                        $usave->task            = 'ADMIN';
                        $usave->first_name      = $firstname;
                        $usave->last_name       = $lastname;
                        $usave->birthday        = $d;
                        $usave->gender          = $gender;
                        $usave->status          = $status ;
                        $usave->profile_pic_name          = $banner ;
                        $usave->password        = sha1($newpassword);
                        //$usave->profile_pic_name = $banner;

                        if($request->getPost('profile') == 'FALSE'){
                            $deleteroles = Userrole::find('userid="'.$id.'"');
                            if($deleteroles->delete())
                            {

                                if($request->getPost('superadmin')!='false' && $request->getPost('superadmin')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'superadmin'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role1')!='false' && $request->getPost('role1')!=NULL)
                                {
                                    $saverole = new Userrole();
                                    $saverole->assign(array(
                                        'userid' => $id,
                                        'role' => 'Users'
                                        ));
                                    if ($saverole->save()) {
                                        $data['error']=array($request->getPost('role1'));
                                    }
                                }

                                if($request->getPost('role2')!='false' && $request->getPost('role2')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'Gallery'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role3')!='false' && $request->getPost('role3')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'News'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role4')!='false' && $request->getPost('role4')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'Contacts'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role5')!='false' && $request->getPost('role5')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'ATV'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role6')!='false' && $request->getPost('role6')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'Trails'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role7')!='false' && $request->getPost('role7')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'Reservations'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role8')!='false' && $request->getPost('role8')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'Meta'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role9')!='false' && $request->getPost('role9')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'Settings'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }
                            }
                        }



                        if(!$usave->save()){
                            $errors = array();
                            foreach ($usave->getMessages() as $message) {
                                $errors[] = $message->getMessage();
                            }
                            $data[]=array('error' => $errors);
                        }else{
                            $data=array('success' => 'UPDATED');
                        }
                    }
                    else{
                        $data=array('success' => 'OLDPASSWORDNOTMATCH');
                    }
            }
            elseif($oldpassword != '' && sha1($newpassword) != sha1($password_c))
            {
                $data=array('success' => 'CONFIRMPASSWORDNOTMATCH');
            }
            else{
                        $usave = Users::findFirst('id="' . $id . '"');
                        $usave->username        = $username;
                        $usave->email           = $email;
                        $usave->task            = 'ADMIN';
                        $usave->first_name      = $firstname;
                        $usave->last_name       = $lastname;
                        $usave->birthday        = $d;
                        $usave->gender          = $gender;
                        $usave->status          = $status ;
                        $usave->profile_pic_name          = $banner ;
                        //$usave->profile_pic_name = $banner;


                        if($request->getPost('profile') == 'FALSE'){
                            $deleteroles = Userrole::find('userid="'.$id.'"');
                            if($deleteroles->delete())
                            {

                                if($request->getPost('superadmin')!='false' && $request->getPost('superadmin')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'superadmin'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role1')!='false' && $request->getPost('role1')!=NULL)
                                {
                                    $saverole = new Userrole();
                                    $saverole->assign(array(
                                        'userid' => $id,
                                        'role' => 'Users'
                                        ));
                                    if ($saverole->save()) {
                                        $data['error']=array($request->getPost('role1'));
                                    }
                                }

                                if($request->getPost('role2')!='false' && $request->getPost('role2')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'Gallery'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role3')!='false' && $request->getPost('role3')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'News'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role4')!='false' && $request->getPost('role4')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'Contacts'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role5')!='false' && $request->getPost('role5')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'ATV'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role6')!='false' && $request->getPost('role6')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'Trails'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role7')!='false' && $request->getPost('role7')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'Reservations'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role8')!='false' && $request->getPost('role8')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'Meta'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }

                                if($request->getPost('role9')!='false' && $request->getPost('role9')!=NULL)
                                {
                                  $saverole = new Userrole();
                                  $saverole->assign(array(
                                    'userid' => $id,
                                    'role' => 'Settings'
                                    ));
                                  if (!$saverole->save()) {
                                      $data['error']=array('Something went wrong saving the data, please try again.');
                                  }
                                }
                            }
                        }
                        

                        if(!$usave->save()){
                            $errors = array();
                            foreach ($usave->getMessages() as $message) {
                                $errors[] = $message->getMessage();
                            }
                            $data[]=array('error' => $errors);
                        }else{
                            $data=array('success' => 'UPDATED');
                        }
            }   
        }
        echo json_encode($data);
    }


    public function skipAction($name) {
        echo "auth skipped ($name)";
    }

    public function forgotpasswordAction($email) {
     $data = array();
     $NMSemail = $email;

     $SubsEmail = Users::findFirst("email='" . $NMSemail . "'");
     $username = $SubsEmail->username;
     if ($SubsEmail == true) 
     {
        $a = '';
            for ($i = 0; $i < 6; $i++) {
                $a .= mt_rand(0, 9);
            }
            $token = sha1($a);

                $forgotEmail = Forgotpasswords::findFirst("email='" . $NMSemail . "'");
                 if ($forgotEmail == true) 
                 {
                    $forgotEmail->delete();
                 }

                   $forgotpassword = new Forgotpasswords();
                   $forgotpassword->assign(array(
                    'email' => $NMSemail,
                    'token' => $token,
                    'date' => date('y-m-d')
                    ));
                   if (!$forgotpassword->save()) 
                   {
                    $data['msg'] = "Something went wrong saving the data, please try again.";

                    } 
                    else 
                    {
                        //mail
                         $dc = new CB();
                         $body = '<div style="background-color: #eee;padding:20px;margin-bottom:10px;">You&#39;re receiving this e-mail because you requested a password reset for your account at RioVerdeATV.com.
                            <br>
                            <br>
                            Your Username : '.$username.'<br>

                            Please click the Reset Password link and choose a new password:
                            <br>
                            <a href="'.$GLOBALS["baseURL"].'/atvadmin/forgotpassword/changepassword/'.$NMSemail.'/'.$token.'" target="_blank">Reset Password</a>
                            <br>
                            <br>
                            <br></div>';

                        $send = $dc->sendMail($NMSemail,'ATV : Password Reset',$body);
                        $data['msg'] = 'Password Reset has been sent to your Email '. $NMSemail;

                    }
            }
     else
     {
        $data = array('msg' => 'No account found with that email address.');

     }
     echo json_encode($data);
    }

     public function checktokenAction($email,$token) {
         $data = array();
         $forgottoken = Forgotpasswords::findFirst("token='" . $token . "' and email ='". $email ."'");
                 if ($forgottoken == true) 
                 {
                    $data['msg'] = 'valid';
                 }
                 else
                 {
                    $data['msg'] = 'invalid';
                 }
        echo json_encode($data);
     }

     public function updatepasswordtokenAction() {

        $email = $_POST['email'];
        $token = $_POST['token'];
        $repassword = $_POST['repassword'];

         $data = array();
         $emailcheck = Users::findFirst("email='" . $email . "'");
         $username = $emailcheck->username;
                 if ($emailcheck == true) 
                 {
                    $emailcheck->password = sha1($_POST['repassword']);
                    if (!$emailcheck->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                    } else 
                    {

                       $forgotEmail = Forgotpasswords::findFirst("token='" . $token . "'");
                       if ($forgotEmail == true) 
                       {
                        if ($forgotEmail->delete()) {
                           
                        }
                       }
                        $data['msg'] = "Password Change Success.";
                    }
                 }
                 else
                 {
                 }
        echo json_encode($data); 
     }
}