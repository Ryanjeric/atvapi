<?php

namespace Controllers;
use \Models\Reservationnotice as Reservationnotice;
use \Controllers\ControllerBase as CB;

class ReservationnoticeController extends \Phalcon\Mvc\Controller {

    public function saveAction(){
        $request = new \Phalcon\Http\Request();
        $guid = new \Utilities\Guid\Guid();
        $id = $guid->GUID();
        $add = new Reservationnotice();
        $notice = $request->getPost('reqasset');

        $find = Reservationnotice::findFirst('notice="'.$notice.'"');
        if($find){
            echo json_encode(array("type" => "danger", "msg" => "Warning! That Reservation notice already exist!"));
        }else{
            $add->assign(array(
                'id' =>$id,
                'notice' => $notice,
                'datecreated' => date('Y-m-d H:i:s'),
                'dateupdated' => date('Y-m-d H:i:s')
                ));
                    // $add->save();
            if (!$add->save()) {
                $errors = array();
                foreach ($add->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } 
            else{
                echo json_encode(array("type" => "success", "msg" => "Reservation notice successfully added!"));
            }
        }
    }

    public function listAction($num,$off,$keyword) {
        $db = \Phalcon\DI::getDefault()->get('db');
        $offsetfinal = ($off * $num) - $num;
        if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {
            $stmt = $db->prepare("SELECT * FROM reservationnotice ORDER BY datecreated LIMIT $offsetfinal, $num");
            $stmt->execute();
            $list = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            $totallist = Reservationnotice::find();
            $totalNumber = count($totallist);

        } else {
            $stmt = $db->prepare("SELECT * FROM reservationnotice WHERE notice LIKE '%$keyword%' ORDER BY datecreated LIMIT $offsetfinal, $num");
            $stmt->execute();
            $list = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            $stmt = $db->prepare("SELECT * FROM reservationnotice WHERE notice LIKE '%$keyword%' ");
            $stmt->execute();
            $totallist = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $totalNumber = count($totallist);
        }

        echo json_encode(array('list' => $list, 'index' => $off, 'total_items' => $totalNumber));
    }
    public function felistAction(){
         $list = Reservationnotice::find(array('order'=>'datecreated'));
        if(count($list) == 0){
            $data['error']=array('No!');
        }else{
        foreach ($list as $li) 
        {
            $data[] = array(
                'id'=>$li->id,
                'notice'=>$li->notice
                );
        }
        }
        echo json_encode($data);
    }
    public function updateAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $id = $request->getPost('id');
            $Reservationnotice = $request->getPost('name');

            $find = Reservationnotice::findFirst('id != "'.$id.'" AND notice = "'.$Reservationnotice.'"');
            if($find) { //return false
                echo json_encode(array("type" => "danger", "msg" => "Warning! That Reservationnotice already exist!"));
            } else {
                $update = Reservationnotice::findFirst("id = '$id' ");
                if($update) {
                    $update->assign(array(
                        'notice' => $Reservationnotice,
                        'dateupdated' => date('Y-m-d H:i:s')
                        ));
                    if($update->save()) {
                        echo json_encode(array("type" => "success", "msg" => "Reservationnotice successfully updated!"));
                    } else {
                        $errors = array();
                        foreach($update->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        echo json_encode(array("type" => "danger", "msg" => $errors));
                    }
                }
            }
        }
    }
    public function deleteAction($id){
        $find = Reservationnotice::findFirst("id = '$id'");
        if($find->delete()){
            echo json_encode(array("type" => "success", "msg" => "Reservationnotice Deleted"));
        }else{
            $errors = array();
            foreach($find->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array("type" => "danger", "msg" => $errors));
        }
    }
}