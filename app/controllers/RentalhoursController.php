<?php

namespace Controllers;

use \Phalcon\Http\Request;
use \Utilities\Guid\Guid;
use \Models\Rentalhours as Rentalhours;
use \Models\Rentalhours_ext as Rentalhours_ext;
use \Models\Rentalhoursmsg as Rentalhoursmsg;
use \Models\Atvrentalprices as Atvrentalprices;
use \Controllers\ControllerBase as CB;

class RentalhoursController extends \Phalcon\Mvc\Controller {

    public function saveAction(){
        $request = new \Phalcon\Http\Request();
        $guid = new \Utilities\Guid\Guid();
        $rentalhours_ext = new Rentalhours_ext();
        $id = $guid->GUID();

        $session = ucwords(strtolower($request->getPost('session')));
        $start = $request->getPost('start');
        $end = $request->getPost('end');
        $desc = $request->getPost('desc');
        $duration = $request->getPost('duration');

        $checksession = Rentalhours::findFirst("session = '".$session."'");
        if($checksession == true) {
          $rentalhoursid = $checksession->id;

          $checksession->duration = $duration;
          if($checksession->save()){} //update the duration of the session

            $rentalhours_ext->id = $guid->GUID();
            $rentalhours_ext->rentalhoursid = $rentalhoursid;
            $rentalhours_ext->starttime = $start;
            $rentalhours_ext->endtime =$end;
            $rentalhours_ext->description =$desc;
            $rentalhours_ext->datecreated = date('Y-m-d H:i:s');
            $rentalhours_ext->dateupdated = date('Y-m-d H:i:s');
        } else {
          //KAPAG BAGONG BAGONG RENTAL SESSION ("Half Day, Full Day, Sunset, ETC")
          $add = new Rentalhours();
          $add->assign(array(
              'id' =>$id,
              'session' => $session,
              'starttime' => $start,
              'endtime' => $end,
              'description' => $desc,
              'duration' => $duration,
              'datecreated' => date('Y-m-d H:i:s'),
              'dateupdated' => date('Y-m-d H:i:s')
              ));

          // $add->save();
          if (!$add->save()) {
              $errors = array();
              foreach ($add->getMessages() as $message) {
                  $errors[] = $message->getMessage();
              }
              $data['type'] = "danger";
              $data['msg'] = $errors[0];
          } else {
            $rentalhours_ext->id = $guid->GUID();
            $rentalhours_ext->rentalhoursid = $id;
            $rentalhours_ext->starttime = $start;
            $rentalhours_ext->endtime =$end;
            $rentalhours_ext->description =$desc;
            $rentalhours_ext->datecreated = date('Y-m-d H:i:s');
            $rentalhours_ext->dateupdated = date('Y-m-d H:i:s');
          }
        }

        if($rentalhours_ext->create()) {
          $data['type'] = "success";
          $data['msg'] = "Rentalhours successfully added!";
        } else {
          foreach ($rentalhours_ext->getMessages() as $message) {
              $errors[] = $message->getMessage();
          }
          $data['type'] = "danger";
          $data['msg'] = $errors[0];
        }

        echo json_encode($data);
    }

    public function computetotalAction($start,$end){
        $time = new CB();
        echo json_encode($time->timeconverter($start,$end));
    }

    public function listrAction($num,$off,$keyword) {
         $time = new CB();
         $finalnotification = array();
        $db = \Phalcon\DI::getDefault()->get('db');
        $offsetfinal = ($off * $num) - $num;
        if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {
            // $stmt = $db->prepare("SELECT rentalhours.session, rentalhours.duration, rentalhours_ext.* FROM rentalhours RIGHT JOIN rentalhours_ext ON rentalhours.id = rentalhours_ext.rentalhoursid ORDER BY rentalhours.session, rentalhours_ext.datecreated DESC LIMIT $offsetfinal, $num");
            $stmt = $db->prepare("SELECT rentalhours.session, rentalhours.duration, rentalhours_ext.*, CASE WHEN rentalhours_ext.starttime LIKE '%AM' THEN 1 ELSE 2 END AS ampm, CASE WHEN rentalhours_ext.starttime LIKE '12%' THEN 1 ELSE 2 END AS twelve FROM rentalhours RIGHT JOIN rentalhours_ext ON rentalhours.id = rentalhours_ext.rentalhoursid ORDER BY rentalhours.duration, rentalhours.session, ampm, twelve, rentalhours_ext.starttime, rentalhours_ext.datecreated DESC LIMIT $offsetfinal, $num");
            $stmt->execute();
            $list = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($list as $noti) {
                        array_push($noti, array("hours"=> $time->timeconverter($noti['starttime'],$noti['endtime'])));
                        $finalnotification[]=$noti;
            } //end foreach

            $totallist = Rentalhours::find();
            $totalNumber = count($totallist);

        } else {
            // $stmt = $db->prepare("SELECT * FROM rentalhours WHERE session LIKE '%$keyword%' ORDER BY session ASC, datecreated DESC LIMIT $offsetfinal, $num");
            $stmt = $db->prepare("SELECT rentalhours.session, rentalhours.duration, rentalhours_ext.*, CASE WHEN rentalhours_ext.starttime LIKE '%AM' THEN 1 ELSE 2 END AS ampm, CASE WHEN rentalhours_ext.starttime LIKE '12%' THEN 1 ELSE 2 END AS twelve FROM rentalhours RIGHT JOIN rentalhours_ext ON rentalhours.id = rentalhours_ext.rentalhoursid WHERE session LIKE '%$keyword%' ORDER BY rentalhours.duration, rentalhours.session, ampm, twelve, rentalhours_ext.starttime, rentalhours_ext.datecreated DESC LIMIT $offsetfinal, $num");
            $stmt->execute();
            $list = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            $stmt = $db->prepare("SELECT * FROM rentalhours WHERE session LIKE '%$keyword%' ");
            $stmt->execute();
            $totallist = $stmt->fetchAll(\PDO::FETCH_ASSOC);

             foreach ($list as $noti) {
                        array_push($noti, array("hours"=> $time->timeconverter($noti['starttime'],$noti['endtime'])));
                        $finalnotification[]=$noti;
            } //end foreach


            $totalNumber = count($totallist);
        }

        echo json_encode(array('list' => $finalnotification, 'index' => $off, 'total_items' => $totalNumber));

    }

    public function editAction($id){
        $time = new CB();
        $data = array();

        $query = "SELECT rentalhours.session, rentalhours.duration, rentalhours_ext.* FROM rentalhours RIGHT JOIN rentalhours_ext ON rentalhours.id = rentalhours_ext.rentalhoursid WHERE rentalhours_ext.id = '".$id."'";
        $rental = CB::atvQueryFirst($query);

        if ($rental == true) {
            $data = array(
                'id' => $rental['id'],
                'session' => $rental['session'],
                'start' => CB::dateTimeFormat($rental['starttime']),
                'end' => $rental['endtime'],
                'desc' => $rental['description'],
                'duration' => $rental['duration']
                );
        }
        $total = str_replace(':00', '', ltrim($time->timeconverter($rental['starttime'],$rental['endtime']), '0'));
        echo json_encode(array("data"=>$data,"total"=>$total));
        // echo json_encode($rental);
    }


    public function updateAction() {
        $request = new \Phalcon\Http\Request();
        $guid = new \Utilities\Guid\Guid();
        if($request->isPost()) {
            $id = $request->getPost('id');
            $session = ucwords(strtolower($request->getPost('session')));
            $start = $request->getPost('start');
            $end = $request->getPost('end');
            $desc = $request->getPost('desc');
            $duration = $request->getPost('duration');

            //UPDATE ROOT QUERY
            $update = Rentalhours_ext::findFirst("id = '$id' ");
            $oldsession = $update->rentalhoursid;

            $findsession = Rentalhours::findFirst("session = '".$session."'");
            if($findsession == true) {
              $sessionid = $findsession->id;

              $findsession->duration = $duration;
              if($findsession->save()){} //update the duration of the session

              if($update == true) {
                  $update->assign(array(
                      'rentalhoursid' => $sessionid,
                      'starttime' => $start,
                      'endtime' =>$end,
                      'description' =>$desc,
                      'dateupdated' => date('Y-m-d H:i:s')
                      ));
              }
            } else {
              $sessionid = $guid->GUID();
              $add = new Rentalhours();
              $add->assign(array(
                  'id' => $sessionid,
                  'session' => $session,
                  'starttime' => $start,
                  'endtime' => $end,
                  'description' => $desc,
                  'datecreated' => date('Y-m-d H:i:s'),
                  'dateupdated' => date('Y-m-d H:i:s')
                  ));
              // $add->save();
              if (!$add->save()) {
                  $errors = array();
                  foreach ($add->getMessages() as $message) {
                      $errors[] = $message->getMessage();
                  }
                  $data['type'] = "danger";
                  $data['msg'] = $errors[0];
              } else {
                if($update == true) {
                    $update->assign(array(
                        'rentalhoursid' => $sessionid,
                        'starttime' => $start,
                        'endtime' =>$end,
                        'description' =>$desc,
                        'dateupdated' => date('Y-m-d H:i:s')
                        ));
                }
              }
            }

            if($update->save()) {
              $data['type'] = "success";
              $data['msg'] = "Rental Hour successfully updated!";

              $findoldsession = Rentalhours_ext::findFirst("rentalhoursid = '".$oldsession."'");
              if($findoldsession == false) {
                $removesession = Rentalhours::findFirst("id = '".$oldsession."'");
                if($removesession->delete()){}

                $removesessionprices = Atvrentalprices::find("rentalid = '".$oldsession."'");
                if($removesessionprices->delete()){}
              }

            } else {
                $errors = array();
                foreach($update->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data['type'] = "danger";
                $data['msg'] = $errors[0];
            }
        }
        echo json_encode($data);
    }
    public function deleteAction($id){
        $find = Rentalhours_ext::findFirst("id = '$id'");
        $rentalhoursid = $find->rentalhoursid;

        if($find->delete()){
            $data['type'] = "success";
            $data['msg'] = "Rentalhours Deleted";

            $findall = Rentalhours_ext::findFirst("rentalhoursid = '".$rentalhoursid."'");
            if($findall == false) {
              $removesession = Rentalhours::findFirst("id = '".$rentalhoursid."'");
              if($removesession->delete()) {

                //DELETE records in "atvrentalprices" table
                $removesessionprices = Atvrentalprices::find("rentalid = '".$rentalhoursid."'");
                if($removesessionprices->delete()) {}

                $data['msg'] = "Rentalhours Deleted together with the Session";

              } else {
                $data['msg'] = "Rentalhours Deleted together with the Session BUT session is NOT deleted";
              }
            }
        } else {
            $errors = array();
            foreach($find->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            $data['type'] = "danger";
            $data['msg'] = $errors[0];
        }

        echo json_encode($data);
    }




    public function savemsgAction(){
        $request = new \Phalcon\Http\Request();
        $msg = Rentalhoursmsg::find();
        $content= $request->getPost('content');
        if(count($msg)==0){
            $add = new Rentalhoursmsg();
            $add->assign(array(
                'content' => $content,
                'datecreated' => date('Y-m-d H:i:s'),
                'dateupdated' => date('Y-m-d H:i:s')
                ));
                    // $add->save();
            if (!$add->save()) {
                $errors = array();
                foreach ($add->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] ="!SAVE";
            }

            else{
                $data['success'] ="SAVE";
            }
        }else{
            $save = Rentalhoursmsg::findFirst(array("id=1"));
            $save->content    = $content;
            $save->dateupdated   = date("Y-m-d H:i:s");

            if(!$save->save()){
                $errors = array();
                foreach ($save->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data[]=array('error' => $errors);
            }else{
                $data[]=array('success' => 'UPDATED');
            }
        }
        echo json_encode(array($data));
    }

    public function getMsgAction() {
       $msg = Rentalhoursmsg::find(array("id=1"));

        if(count($msg)!=0){

            $data=array(
                'content'  => $msg[0]->content
                );
        }else{
         $data=array(
            'content'  => ""
            );
        }
   echo json_encode($data);
    }

    public function sessionsAction() {
      $query = "SELECT session FROM rentalhours RIGHT JOIN rentalhours_ext ON rentalhours.id = rentalhours_ext.rentalhoursid GROUP BY session ORDER BY session ";
      $list = CB::atvQuery($query);

      $data['sessions'] = array();
      foreach($list as $n) {
        array_push($data['sessions'], $n['session']);
      }
      echo json_encode($data);
    }

    public function sessiondurationAction() {
      $request = new Request();
      if($request->isPost()) {
        $session = $request->getPost('session');
        $rentalhour = Rentalhours::findFirst("session = '".$session."'");
        if($rentalhour == true) {
          $data['duration'] = $rentalhour->duration;
        } else {
          $data['duration'] = 1;
        }
      }
      echo json_encode($data);
    }


}
