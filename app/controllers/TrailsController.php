<?php

namespace Controllers;
use \Phalcon\Http\Request;
use \Models\Trails as Trails;
use \Models\Trailsmeta as Metadata;
use \Models\Trailsmap as Trailsmap;
use \Models\Rentalhours as Rentalhours;
use \Controllers\ControllerBase as CB;

class TrailsController extends \Phalcon\Mvc\Controller {


    public function trailsExistAction($title) {
        if(!empty($title)) {
            $user = Trails::findFirst('title="' . $title . '"');
            if ($user) {
                echo json_encode(array('exists' => true));
            } else {
                echo json_encode(array('exists' => false));
            }
        }
    }

    public function validationtrailuniqueslugsAction() {
      $request = new \Phalcon\Http\Request();
      if($request->isPost()) {
        $slugs =  $request->getPost('field');
        $find = Trails::findFirst("slugs = '".$slugs."' ");
        if($find == true) {
          $data['isUnique'] = false;
        } else {
          $data['isUnique'] = true;
        }
        echo json_encode($data);
      }
    }

    public function saveAction(){
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $details = $request->getPost('trails');
            $featured = $request->getPost('featured');
            $stimage = $request->getPost('stpicture');
            $ndimage = $request->getPost('ndpicture');
            $rentalhr = $request->getPost('rentalhr');
            $map = $request->getPost('map');

             $guid = new \Utilities\Guid\Guid();
             $id = $guid->GUID();
             $usave = new Trails();
             $usave->id = $id;
             $usave->category = $details['category'];
             $usave->title = $details['title'];
             $usave->slugs = $details['slugs'];
             $usave->distance = $details['distance'];
             $usave->elevation = $details['elevation'];
             $usave->metatitle = $details['metatitle'];
             $usave->metakeyword = $details['metakeyword'];
             $usave->metadesc = $details['metadesc'];
             $usave->dlf = $details['dlf'];
             $usave->dlt = $details['dlt'];
             $usave->wtb = $details['wtb'];
             $usave->description = $details['desc'];
             $usave->featuredimage = $featured;
             $usave->firstimage = $stimage;
             $usave->secondimage = $ndimage;
             $usave->datecreated = date("Y-m-d H:i:s");
             $usave->dateupdated = date("Y-m-d H:i:s");

             if (!$usave->save()) {
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                $data['error'] =  $errors;
            } else {
                $rh = array();
                $mp = array();

                $rh = $rentalhr;
                $mp = $map;


                if($rh == NULL || $mp == NULL){
                    $data['error'] = "BOOM";
                }else{
                    foreach($rh as $key => $value){
                        if($rh[$key] == 'false' || $mp[$key]==''){
                            $data['WEW'] = "Success";
                        }
                        else{
                            $save = new Trailsmap();
                            $save->assign(array(
                                'trailid' => $id,
                                'rentalid' => $rh[$key],
                                'filename' =>  $mp[$key]
                                ));
                            if (!$save->save()) {
                                $errors = array();
                                foreach ($save->getMessages() as $message) {
                                    $errors[] = $message->getMessage();
                                }
                                $data['error'] =  $errors;
                            } else {

                                $data['success'] = "Success";
                            }
                        }
                    }
                }

           }
        }else{
            $data['error'] =  "NO POST DATA";
        }

        echo json_encode($data);
    }

     public function ListAction($num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined') {
            $list = Trails::find(array("order"=>"datecreated"));
        } else {
            $conditions = "title LIKE '%" . $keyword . "%'
            or dlf LIKE '%" . $keyword . "%'
            or dlt LIKE '%" . $keyword . "%'
            or distance LIKE '%" . $keyword . "%'";
            $list= Trails::find(array($conditions,"order"=>"datecreated"));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $list,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'title' => $m->title,
                'distance' => $m->distance,
                'dlf' => $m->dlf,
                'dlt' => $m->dlt
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }



    public function deleteAction($id){
        $dlt = Trails::findFirst('id="' . $id . '"');
        if ($dlt) {
            if($dlt->delete()){
                $del = Trailsmap::find('trailid="'.$id.'"');
                $del->delete();
                echo json_encode(array("type" => "success", "msg" => "Trails Deleted"));
            }else {
                $errors = array();
                foreach($dlt->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array("type" => "danger", "msg" => $errors));
            }
        }

    }

    public function Infoction($id) {
        $getInfo = Trails::findFirst('id="'. $id .'"');
        $data = array(
            'category'=>$getInfo->category,
            'id' =>  $getInfo->id,
            'title' =>  $getInfo->title,
            'slugs' => $getInfo->slugs,
            'distance' =>  $getInfo->distance,
            'elevation' =>  $getInfo->elevation,
            'dlf' =>  $getInfo->dlf,
            'dlt' =>  $getInfo->dlt,
            'wtb' =>  $getInfo->wtb,
            'metatitle' => $getInfo->metatitle,
            'metakeyword' => $getInfo->metakeyword,
            'metadesc' => $getInfo->metadesc,
            'desc' =>  $getInfo->description,
            'featuredimage' =>  $getInfo->featuredimage,
            'firstimage' =>  $getInfo->firstimage,
            'secondimage' =>  $getInfo->secondimage,
            );
        $getInfo2 = Trailsmap::find('trailid="'.$id.'"');

        foreach ($getInfo2 as $get2)
        {
            $data2[] = array(
             'rentalid' =>$get2->rentalid,
             'trailid'=>$value->trailid,
             'map' =>$get2->filename,
            );
        }

        echo json_encode(array("data"=>$data,"data2"=>$data2));
    }

    public function trailensureuniqueslugsAction() {
      $request = new Request();
      if($request->isPost()) {
        $trailid = $request->getPost('trailid');
        $slugs = $request->getPost('slugs');

        $find = Trails::findFirst("id != '".$trailid."' AND slugs = '".$slugs."' ");
        if($find == true) {
          $data['isUnique'] = false;
        } else {
          $data['isUnique'] = true;
        }
        echo json_encode($data);
      }
    }
    public function UpdateAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            //VARIABLE
            $details = $request->getPost('trails');
            $featured = $request->getPost('featured');
            $stpicture = $request->getPost('stpicture');
            $ndpicture = $request->getPost('ndpicture');
            $rentalhr = $request->getPost('rentalhr');
            $map = $request->getPost('map');

            $usave = Trails::findFirst('id="' . $details['id'] . '"');
            $usave->category = $details['category'];
            $usave->title = $details['title'];
            $usave->slugs = $details['slugs'];
            $usave->distance = $details['distance'];
            $usave->elevation = $details['elevation'];
            $usave->dlf = $details['dlf'];
            $usave->dlt = $details['dlt'];
            $usave->wtb = $details['wtb'];
            $usave->description = $details['desc'];
            $usave->metatitle = $details['metatitle'];
            $usave->metakeyword = $details['metakeyword'];
            $usave->metadesc = $details['metadesc'];
            $usave->featuredimage = $featured;
            $usave->firstimage = $stpicture;
            $usave->secondimage = $ndpicture;
            $usave->dateupdated = date("Y-m-d H:i:s");

            if(!$usave->save()){
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
               $data['error'] =  $errors;
            }else{
                $del = Trailsmap::find('trailid="'.$details['id'].'"');
                $del->delete();
                $rh = array();
                $mp = array();

                $rh = $rentalhr;
                $mp = $map;

                if($map == undefined){
                    $data['success'] = "Success";
                }else{
                       foreach($rh as $key => $value){
                         if($rh[$value] == 'false' || $mp[$value]==''){
                            $data['WEW'] = "Success";
                        }else{
                            $save = new Trailsmap();
                            $save->assign(array(
                                'trailid' => $details['id'],
                                'rentalid' => $rh[$value],
                                'filename' =>  $mp[$value]
                                ));
                            if (!$save->save()) {
                                $errors = array();
                                foreach ($save->getMessages() as $message) {
                                    $errors[] = $message->getMessage();
                                }
                                $data['error'] =  $errors;
                            } else {

                                $data['success'] = "Success";
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($data);
    }


     public function feInfoAction() {
        $getInfo = Trails::find();
        $time = new CB();
        foreach ($getInfo as $key => $value){

            $data[] = array(
            'id' =>  $value->id,
            'category' => $value->category,
            'title' =>  $value->title,
            'slugs' =>  $value->slugs,
            'distance' =>  $value->distance,
            'elevation' =>  $value->elevation,
            'dlf' =>  $value->dlf,
            'dlt' =>  $value->dlt,
            'wtb' =>  $value->wtb,
            'desc' =>  $value->description,
            'featuredimage' =>  $value->featuredimage,
            'firstimage' =>  $value->firstimage,
            'secondimage' =>  $value->secondimage,
            );

            $getInfo2 = Trailsmap::find('trailid="'.$value->id.'"');
            foreach ($getInfo2 as $get2)
            {
                $rental = Rentalhours::findFirst('id="' . $get2->rentalid . '"');
                $data2[] = array(
                    'mapid' =>$get2->id,
                    'trailid'=>$value->id,
                    'map' =>$get2->filename,
                    'totalhr' => $rental->duration
                    // 'totalhr'=> str_replace(':00', '', ltrim($time->timeconverter($rental->starttime,$rental->endtime), '0'))
                    );
            }
        }
        echo json_encode(array("data"=>$data,"data2"=>$data2));
    }


    public function fecatinfoAction($category) {
        $getInfo = Trails::find('category="'.$category.'"');

        if(count($getInfo)!=0){
               $time = new CB();
               foreach ($getInfo as $key => $value){

                $data[] = array(
                    'id' =>  $value->id,
                    'category' => $value->category,
                    'title' =>  $value->title,
                    'slugs' => $value->slugs,
                    'distance' =>  $value->distance,
                    'elevation' =>  $value->elevation,
                    'dlf' =>  $value->dlf,
                    'dlt' =>  $value->dlt,
                    'wtb' =>  $value->wtb,
                    'desc' =>  $value->description,
                    'featuredimage' =>  $value->featuredimage,
                    'firstimage' =>  $value->firstimage,
                    'secondimage' =>  $value->secondimage,
                    );

                $getInfo2 = Trailsmap::find('trailid="'.$value->id.'"');
                foreach ($getInfo2 as $get2)
                {
                    $rental = Rentalhours::findFirst('id="' . $get2->rentalid . '"');
                    $data2[] = array(
                        'mapid' =>$get2->id,
                        'trailid'=>$value->id,
                        'map' =>$get2->filename,
                        'totalhr' => $rental->duration
                        // 'totalhr'=> str_replace(':00', '', ltrim($time->timeconverter($rental->starttime,$rental->endtime), '0'))
                        );
                }
            }
        }else{
            $data ="NODATA";
            $data2 ="NODATA";
        }

        echo json_encode(array("data"=>$data,"data2"=>$data2));
    }

    ////////META


    public function METAListAction($num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined') {
            $list = Metadata::find();
        } else {

        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $list,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'trailcategory' => strtoupper($m->trailcategory),
                'title'=> $m->title,
                'metatitle'=> $m->metatitle,
                'metakeyword' => $m->metakeyword,
                'metadescription' => $m->metadescription,
                'dateupdated' => $m->dateupdated
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }


    public function METAeditAction($id){
        $m = Metadata::findFirst('id="' . $id . '"');
        if ($m) {
            $data = array(
               'id' => $m->id,
                'trailcategory' => $m->trailcategory,
                'title'=> $m->title,
                'metatitle'=> $m->metatitle,
                'metakeyword' => $m->metakeyword,
                'metadescription' => $m->metadescription,
                'dateupdated' => $m->dateupdated
                );
        }else{
            $data = 'nodata';
        }

        echo json_encode($data);
    }

    public function METAupdateAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $id = $request->getPost('id');
            $title = $request->getPost('title');
            $metatitle = $request->getPost('metatitle');
            $metakeyword = $request->getPost('metakeyword');
            $metadescription = $request->getPost('metadescription');

            $update = Metadata::findFirst("id = '$id' ");
            if($update) {
                $update->assign(array(
                    'metatitle' => $metatitle,
                    'title' => $title,
                    'metakeyword' => $metakeyword,
                    'metadescription' =>$metadescription,
                    'dateupdated' => date('Y-m-d H:i:s')
                    ));
                if($update->save()) {
                    echo json_encode(array("type" => "success", "msg" => "Successfully updated!"));
                } else {
                    $errors = array();
                    foreach($update->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array("type" => "danger", "msg" => $errors));
                }
                }
            }
    }

    public function fetrailinfobyslugsAction($slugs) {
      $time = new CB();
      $getInfo = Trails::findFirst("slugs = '".$slugs."' ");
       $trailid = $getInfo->id;
          $data = array(
          'id' =>  $getInfo->id,
          'category' => $getInfo->category,
          'title' =>  $getInfo->title,
          'distance' =>  $getInfo->distance,
          'elevation' =>  $getInfo->elevation,
          'dlf' =>  $getInfo->dlf,
          'dlt' =>  $getInfo->dlt,
          'wtb' =>  $getInfo->wtb,
          'desc' =>  $getInfo->description,
          'featuredimage' =>  $getInfo->featuredimage,
          'firstimage' =>  $getInfo->firstimage,
          'secondimage' =>  $getInfo->secondimage,
          'metatitle'=> $getInfo->metatitle,
          'metakeyword' => $getInfo->metakeyword,
          'metadescription' => $getInfo->metadesc,
          );

      $getInfo2 = Trailsmap::find('trailid="'.$trailid.'"');

      foreach ($getInfo2 as $get2) {
          $rental = Rentalhours::findFirst('id="' . $get2->rentalid . '"');
          $data2[] = array(
              'mapid' =>$get2->id,
              'trailid'=>$getInfo->id,
              'map' =>$get2->filename,
              'totalhr'=> $rental->duration .' '
              );
      }
      echo json_encode(array("data"=>$data,"data2"=>$data2));
    }

}
