<?php

namespace Controllers;

use Models\Rentalhours;
use Models\Contactinfo;
use Models\Settings;
use \Controllers\ControllerBase as CB;

class ExampleController extends \Phalcon\Mvc\Controller {

	public function pingAction() {
        $sample = new CB;
        var_dump($sample->getConfig()->application->apiURL);
         $data = $sample->modelsManager("Delete from table1");
	}

    public function testAction() {
        // echo json_encode(CB::getConfig()->email[0]);
        // echo CB::sendMail('neilmaledev@gmail.com','test response', 'wew')['Message'];
        echo Settings::findFirst()->logo;
    }

    public function skipAction($name) {
        echo "auth skipped ($name)";
    }
}
