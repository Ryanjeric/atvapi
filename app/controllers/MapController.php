<?php

namespace Controllers;

use \Models\Users as Users;
use \Models\Maps as Maps;
use \Models\Markers as Markers;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class MapController extends \Phalcon\Mvc\Controller {
    public function uploadPicsAction() {
        $request = new \Phalcon\Http\Request();
            //Check if the user has uploaded files
            if ($request->hasFiles() == true) {
                    //Print the real file names and their sizes
                    foreach ($request->getUploadedFiles() as $file){
                            echo $file->getName(), " ", $file->getSize(), "\n";
                            $file->moveTo('files/' . $file->getName());
                    }
            }
    }
    public function saveMapMarkerAction(){
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            try{

                  $transactionManager = new TransactionManager();

                  $transaction = $transactionManager->get();

             $maps = new Maps();
             $maps->setTransaction($transaction);
             $guid = new \Utilities\Guid\Guid();
             $maps->id = $guid->GUID();
             $maps->title = $request->getPost('title');
             $maps->agent = $request->getPost('userid');
             $maps->description = $request->getPost('title');
             $maps->status=1;
             $maps->hide_agent = ($request->getPost('showauthor') ? 1 : 0); 
             $maps->created_at = date("Y-m-d H:i:s");
             $maps->updated_at = date("Y-m-d H:i:s");
             if ($maps->save() == false) {
                  $array = [];
                  foreach ($maps->getMessages() as $message) {
                    $array[] = $message;
                  }
                  $transaction->rollback('Cannot save markers.');
             }

             $mk = $request->getPost('markersInfo');
             foreach($mk as $m => $k){
                $markers = new Markers();
                $markers->setTransaction($transaction);
                //$markers->id = $guid->GUID();
                 $markers->id = $k['idKey'];
                $markers->map_id = $maps->id;
                $markers->description = $k['description'];
                $markers->video = $k['videolink'];
                $markers->long = $k['longitude'];
                $markers->lat = $k['latitude'];
                 $markers->created_at = date("Y-m-d H:i:s");
                 $markers->updated_at = date("Y-m-d H:i:s");
                if($markers->save()==false){
                    $array = [];
                    foreach ($markers->getMessages() as $message) {
                      $array[] = $message;
                    }
                    var_dump($array);
                    die();
                  $transaction->rollback('Cannot save markers.');
                } 
             }


                $transaction->commit();

            }catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
                die( json_encode(array('401' => $e->getMessage())) );
            }

            $response['200'] = "Success.";
            $response['data'] = array('id' => $maps->id);
            die(json_encode($response));
        }
    }
}