-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 28, 2015 at 02:45 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbatv`
--

-- --------------------------------------------------------

--
-- Table structure for table `api`
--

CREATE TABLE IF NOT EXISTS `api` (
`client_id` int(10) unsigned NOT NULL,
  `public_id` char(64) NOT NULL DEFAULT '',
  `private_key` char(64) NOT NULL DEFAULT '',
  `status` enum('ACTIVE','INACTIVE') DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `api`
--

INSERT INTO `api` (`client_id`, `public_id`, `private_key`, `status`) VALUES
(1, '', '593fe6ed77014f9507761028801aa376f141916bd26b1b3f0271b5ec3135b989', 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `atvimages`
--

CREATE TABLE IF NOT EXISTS `atvimages` (
`id` int(11) NOT NULL,
  `filename` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `atvimages`
--

INSERT INTO `atvimages` (`id`, `filename`) VALUES
(1, 'donk4kj4i14410950077970.png'),
(2, 'gofqh4u0udi14410950226340.png'),
(3, 'zf65ycfpqfr14410950999180.png');

-- --------------------------------------------------------

--
-- Table structure for table `atvprices`
--

CREATE TABLE IF NOT EXISTS `atvprices` (
  `id` varchar(50) NOT NULL,
  `title` varchar(200) NOT NULL,
  `subtitle` varchar(200) NOT NULL,
  `features` text NOT NULL,
  `description` text NOT NULL,
  `picture` text NOT NULL,
  `datecreated` datetime NOT NULL,
  `dateupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `atvprices`
--

INSERT INTO `atvprices` (`id`, `title`, `subtitle`, `features`, `description`, `picture`, `datecreated`, `dateupdated`) VALUES
('05BC636C-ECB3-44F8-B7B5-0155C667BEE4', 'POLARIZ RZR 2 (900)', '2 Seaters', 'EPS / 75 HP / 50â€ width', '<p><span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">This vehicle goes off-road where other Side x Sides can&rsquo;t, and has more power, better suspension, and lets you enjoy your adventure with greater comfort.</span></p>\n', 'gofqh4u0udi14410950226340.png', '2015-09-03 13:01:14', '2015-09-03 06:23:30'),
('E5DA801F-3C13-41F4-BDFC-367B0425F784', 'POLARIZ RZR 2 (570)', '2 Seaters', 'EPS / 45 HP / 50â€ width', '<p><span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">With a 50&quot; width, this vehicle can go off-roading where other Side x Sides can&rsquo;t. Lightweight, yet efficient, and with a durable CVT automatic transmission.</span></p>\n', 'donk4kj4i14410950077970.png', '2015-09-03 13:02:15', '2015-09-03 05:02:15'),
('E957A194-87F8-44A3-A332-3EA5B0D423C6', 'POLARIZ RZR 4 (4800)', '4 Seaters', 'EPS / 55 HP / 60.5â€ width', '<p><span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">This is perfect for your entire family or a group of friends. With this RZR, safely create an unforgettable experience with many happy memories!</span></p>\n', 'zf65ycfpqfr14410950999180.png', '2015-09-03 13:09:51', '2015-09-03 05:09:51');

-- --------------------------------------------------------

--
-- Table structure for table `atvrentalprices`
--

CREATE TABLE IF NOT EXISTS `atvrentalprices` (
`id` int(11) NOT NULL,
  `atvid` varchar(50) NOT NULL,
  `rentalid` varchar(50) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `atvrentalprices`
--

INSERT INTO `atvrentalprices` (`id`, `atvid`, `rentalid`, `price`) VALUES
(12, 'E5DA801F-3C13-41F4-BDFC-367B0425F784', 'ABB1DA25-9EAE-435D-88A8-8F94775A77EE', 160),
(13, 'E5DA801F-3C13-41F4-BDFC-367B0425F784', '1261888F-9ABC-4D26-90B6-9FD383C145EF', 215),
(14, 'E5DA801F-3C13-41F4-BDFC-367B0425F784', '16AAEDED-4CC9-499C-994C-2B1CA04401E0', 315),
(15, 'E957A194-87F8-44A3-A332-3EA5B0D423C6', 'ABB1DA25-9EAE-435D-88A8-8F94775A77EE', 185),
(16, 'E957A194-87F8-44A3-A332-3EA5B0D423C6', '1261888F-9ABC-4D26-90B6-9FD383C145EF', 275),
(17, 'E957A194-87F8-44A3-A332-3EA5B0D423C6', '16AAEDED-4CC9-499C-994C-2B1CA04401E0', 380),
(18, '05BC636C-ECB3-44F8-B7B5-0155C667BEE4', 'ABB1DA25-9EAE-435D-88A8-8F94775A77EE', 345),
(19, '05BC636C-ECB3-44F8-B7B5-0155C667BEE4', '1261888F-9ABC-4D26-90B6-9FD383C145EF', 245),
(20, '05BC636C-ECB3-44F8-B7B5-0155C667BEE4', '16AAEDED-4CC9-499C-994C-2B1CA04401E0', 205),
(21, '05BC636C-ECB3-44F8-B7B5-0155C667BEE4', '5D23AD17-461F-499E-B76F-C019956EC6F0', 500);

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE IF NOT EXISTS `authors` (
  `id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `location` varchar(500) NOT NULL,
  `occupation` varchar(200) NOT NULL,
  `since` date NOT NULL,
  `photo` varchar(500) DEFAULT NULL,
  `about` text,
  `datecreated` date NOT NULL,
  `dateupdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `name`, `location`, `occupation`, `since`, `photo`, `about`, `datecreated`, `dateupdated`) VALUES
('227885A4-D6EB-418D-B02A-C7AE2EE4EEF9', 'Jacinto', 'Asingan', 'Testes', '2015-09-22', NULL, NULL, '2015-09-22', '2015-09-22 03:32:57'),
('4216952F-851A-4C78-8E02-4CB2F09B5701', 'Heaven', 'Earth', 'Tester', '2015-09-24', 'dmwccys0pb914428855559380.jpg', NULL, '2015-09-22', '2015-09-22 03:31:16'),
('7AA6EE71-58C5-4EB3-9107-05ABA8EC8F4A', 'Batman', 'Moon', 'Tubero', '2015-09-22', NULL, NULL, '2015-09-22', '2015-09-22 04:54:21'),
('8ACD4E6A-2218-45D5-B73A-72B4FF22BD2E', 'Master Uson', 'Asingan Village', 'Taga Bantay', '2015-09-22', NULL, NULL, '2015-09-22', '2015-09-22 04:20:40'),
('C80E6CCE-9160-46E6-9216-96BD0A4BC2B1', 'Kyben', 'Outworld', 'Smoker', '2015-09-22', NULL, NULL, '2015-09-22', '2015-09-22 03:33:42'),
('F5781230-00B4-4C7D-99C9-529D9A340028', 'Jeevon', 'Mercury', 'Custodian', '2015-09-22', NULL, NULL, '2015-09-22', '2015-09-22 04:19:46'),
('F6AB4E4B-0D1D-48CD-A930-0F0AB581E1D5', 'Hazel', 'Jan', 'Jan', '2015-09-22', NULL, NULL, '2015-09-22', '2015-09-22 03:18:45');

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE IF NOT EXISTS `contactus` (
  `id` varchar(50) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `email` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `datesubmitted` date NOT NULL,
  `datecreated` datetime NOT NULL,
  `dateupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`id`, `fname`, `lname`, `content`, `email`, `status`, `datesubmitted`, `datecreated`, `dateupdated`) VALUES
('EEB2C375-2B98-47E9-B21B-0F8FCE658346', 'TEST', 'TEST', 'TESTTEST TESTTEST TESTTEST', 'TEST@TEST', 0, '2015-08-24', '2015-08-24 18:16:47', '2015-08-24 10:16:47'),
('FE71D19F-11F1-4FD1-ACA6-3607520F3783', 'vbnvbnv', 'bnvbn', 'qweqwe', 'vbnv@qwe', 2, '2015-08-24', '2015-08-24 16:17:16', '2015-08-24 08:17:16');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` varchar(50) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `phonenum` varchar(30) NOT NULL,
  `email` varchar(200) NOT NULL,
  `numpeople` int(11) NOT NULL,
  `numpeopledrive` int(11) NOT NULL,
  `kids` text NOT NULL,
  `request` text NOT NULL,
  `deliver` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `fname`, `lname`, `phonenum`, `email`, `numpeople`, `numpeopledrive`, `kids`, `request`, `deliver`) VALUES
('C22C21CF-ABA0-4214-87E2-F6100BA85002', 'Ryan jeric', 'Sabado', '090909345', 'rjsaturday19@gmail.com', 33, 44, 'qweqweqwe', 'qweqwe', 'qweqwe'),
('D9C975EB-3403-4D9D-9B5C-A185B4A91270', 'Ryanjeric', 'Sabado', '89675674', 'rjsaturday19@gmail.com', 3, 2, 'qweqe', 'zxczxc', 'ewrwer');

-- --------------------------------------------------------

--
-- Table structure for table `deliver`
--

CREATE TABLE IF NOT EXISTS `deliver` (
`id` int(11) NOT NULL,
  `content` text NOT NULL,
  `datecreated` datetime NOT NULL,
  `dateupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deliver`
--

INSERT INTO `deliver` (`id`, `content`, `datecreated`, `dateupdated`) VALUES
(1, '<p><span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 15px; line-height: 21.4286px;">We offer delivery service in the Verde</span><br style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 15px; line-height: 21.4286px;" />\n<span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 15px; line-height: 21.4286px;">Valley area including Sedona without extra</span><br style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 15px; line-height: 21.4286px;" />\n<span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 15px; line-height: 21.4286px;">charge for your convenience.asd</span></p>\n', '2015-09-10 14:16:26', '2015-09-11 03:16:40'),
(2, '<p>wew</p>\n', '2015-09-11 10:03:38', '2015-09-11 02:03:38'),
(3, '<p>zxcxzc</p>\n', '2015-09-11 10:04:24', '2015-09-11 02:04:24'),
(4, '<p>qweqwe</p>\n', '2015-09-11 10:05:47', '2015-09-11 02:05:47');

-- --------------------------------------------------------

--
-- Table structure for table `forgotpassword`
--

CREATE TABLE IF NOT EXISTS `forgotpassword` (
`id` int(11) NOT NULL,
  `email` varchar(500) NOT NULL,
  `token` varchar(500) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `freebies`
--

CREATE TABLE IF NOT EXISTS `freebies` (
`id` int(11) NOT NULL,
  `content` text NOT NULL,
  `datecreated` datetime NOT NULL,
  `dateupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `freebies`
--

INSERT INTO `freebies` (`id`, `content`, `datecreated`, `dateupdated`) VALUES
(1, '<p><span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">Helmet, Goggles, Cooler, Water, Map&nbsp;</span><br style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;" /><span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">For safety, helmet and eye protection are strongly&nbsp;</span><br style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;" /><span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">recommended. They are provided for&nbsp;</span><label class="clr_orange" style="box-sizing: border-box; display: inline-block; max-width: 100%; margin-bottom: 5px; font-weight: 700; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: rgb(232, 78, 26) !important;">FREE!</label></p><p><label class="clr_orange" style="box-sizing: border-box; display: inline-block; max-width: 100%; margin-bottom: 5px; font-weight: 700; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: rgb(232, 78, 26) !important;">TESTWEW</label></p><p>&nbsp;</p><p>&nbsp;</p>', '2015-08-25 16:46:26', '2015-09-18 07:47:17');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
`id` int(11) NOT NULL,
  `filename` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `filename`) VALUES
(11, '7eow8aug14i1441263319861.JPG'),
(12, '549fc9mgqfr1441263329070.JPG'),
(13, '95elwgq4cxr1441263703797.JPG'),
(14, 'wjyj33gcik91441263713155.JPG'),
(15, 'o1thlbrcnmi1441263720253.JPG'),
(16, '5c44f3cjtt91441263747207.JPG'),
(17, 'v4ucrnqaor1441263753585.JPG'),
(18, '59v3j21q0k91441263756961.JPG'),
(20, '0ngz3x9wwmi1441263793454.JPG'),
(21, 'zciav0od2t91441263804944.JPG'),
(22, '4corfnu3di1441263819521.JPG'),
(23, 'vsz38ppsyvi1441263828046.JPG'),
(24, '84zio09hpvi1441263834034.JPG'),
(25, 'wl2caw0zfr1441263840464.JPG'),
(26, 'qd1hn2ep14i1441263966737.JPG'),
(27, 'h1arod2t91441263991969.JPG'),
(28, 'h3b30gujtt91441264011069.JPG'),
(29, 's08ts3piudi1441264140625.JPG'),
(30, '09n6hvt2o6r1441264151228.JPG'),
(31, 'tzw43x47vi1441267521538.JPG'),
(32, 'rtb58qia4i1441353169534.jpg'),
(33, 'v3mrec4ygb91441353218964.png'),
(34, 'xepmfrjxlxr1441353231490.png');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` varchar(100) NOT NULL,
  `imagename` varchar(200) NOT NULL,
  `category` varchar(50) NOT NULL,
  `datecreated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `imagename`, `category`, `datecreated`) VALUES
('0EFAFE02-7E31-4AB9-B43D-1B4DD29BC8B3', 'dmwccys0pb914428855559380.jpg', 'author', '2015-09-22 00:00:00'),
('1714178E-4C3A-4216-B75B-3ED8E71059CB', 'i5ws8msfw2914434109799150.jpg', 'news', '2015-09-28 05:29:42'),
('178748ED-C002-4302-B9CF-5C08188DE6C3', 'wmlcxpkqpvi14434110117910.png', 'news', '2015-09-28 05:30:12'),
('3882CBA6-BE90-4B21-95C2-9235C62E240F', 'kcvogz257b914428860202360.png', 'author', '2015-09-22 03:40:22'),
('41E2614B-434F-471A-B7D4-0DDC7E62FC02', 'zukhntgldi14434110059860.png', 'news', '2015-09-28 05:30:06'),
('48E1F20F-30C2-4AF9-8E9E-2FADD5C3A988', '5h2sh22csor14434109512350.png', 'news', '2015-09-28 05:29:17'),
('532F4857-9852-4AA5-91D7-0442B695F473', 'pggyfjhh0k914434109865620.jpg', 'news', '2015-09-28 05:29:47'),
('5DA8EAD7-941A-47D3-A32F-5F0B8E2B5713', 'x005qmsfw2914428190844440.png', 'author', '2015-09-21 00:00:00'),
('94E9104F-9F47-44FD-86C0-97B5526352AA', 'bf4yz00be2914428856034670.jpg', 'author', '2015-09-22 00:00:00'),
('9C02594C-40AA-40CD-971A-491FFDA48FB6', 'fdzuzph9f6r14434109915030.png', 'news', '2015-09-28 05:29:52'),
('9C1E9323-8E21-44E4-A53E-00686BF9335F', '435cow2914428852776700.png', 'author', '2015-09-22 00:00:00'),
('A4E7A4E9-8494-47E6-AE2A-D6B405955D82', 'mw8it62mx6r14428861878900.png', 'author', '2015-09-22 03:43:10'),
('ABEF6D46-671A-4724-A7F8-15CEB2E4F3B4', 'vzfwam50zfr14429829357920.png', 'author', '2015-09-23 06:35:40'),
('AD1EC405-EBC7-40E8-AF16-52F33445DD90', 'z7cqiz1if6r14428857157700.png', 'author', '2015-09-22 00:00:00'),
('D3A09630-52FE-45EC-92A8-76DC4A0E6CC5', 'dv6yhgjh5mi14428233139370.png', 'author', '2015-09-21 00:00:00'),
('D6495C12-09D0-41EB-A76D-31DEC3339648', 'j1vvdlyds4i14434109693660.png', 'news', '2015-09-28 05:29:32'),
('E84DB78E-8558-4053-B13B-E42E66D1FE4A', 'wgqx6wp14i14428189353160.jpg', 'author', '2015-09-21 00:00:00'),
('E8725279-49AB-4E76-B0B4-52F30D99188C', 'pdxjipv6lxr14428941256410.jpg', 'author', '2015-09-22 05:55:29'),
('EB639B93-8D89-4DF7-BBBE-06206095D240', 'tvyhk3mcxr14434109996420.png', 'news', '2015-09-28 05:30:02'),
('FEA1D31E-9B3D-43E5-8FAE-B095D175BB32', 'na4dwmte2914428889241230.png', 'author', '2015-09-22 04:28:49');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slugs` varchar(500) NOT NULL,
  `author` varchar(100) NOT NULL,
  `body` text NOT NULL,
  `summary` text NOT NULL,
  `metatitle` varchar(100) NOT NULL,
  `metadesc` varchar(500) NOT NULL,
  `metakeyword` varchar(100) NOT NULL,
  `featimage` varchar(100) DEFAULT NULL,
  `featvideo` varchar(100) DEFAULT NULL,
  `featuredtype` varchar(100) NOT NULL,
  `status` int(1) NOT NULL,
  `datepublished` datetime NOT NULL,
  `dateupdated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `slugs`, `author`, `body`, `summary`, `metatitle`, `metadesc`, `metakeyword`, `featimage`, `featvideo`, `featuredtype`, `status`, `datepublished`, `dateupdated`) VALUES
('283ACAC8-6179-42CB-AA79-28EBC6044420', 'mar', 'mar', 'F5781230-00B4-4C7D-99C9-529D9A340028', '<p>mar</p>\n', 'mar', 'mar', 'mar', 'mar', 'EB639B93-8D89-4DF7-BBBE-06206095D240', '', 'img', 1, '2015-09-28 13:32:52', '2015-09-28 13:32:52'),
('3A604CBF-E728-41B0-B575-AFB4C2C787E7', 'May', 'may', 'F5781230-00B4-4C7D-99C9-529D9A340028', '<p>May</p>\n', 'May', 'May', 'May', 'May', '', 'BC9E5487-8340-4001-9172-0F55D10C2087', 'vid', 1, '2015-09-18 14:37:35', '2015-09-28 14:37:35'),
('585A388C-5E8D-4FDE-A9F1-B37D4B5A4D5A', 'apr', 'apr', 'F5781230-00B4-4C7D-99C9-529D9A340028', '<p>apr</p>\n', 'apr', 'apr', 'apr', 'apr', '1714178E-4C3A-4216-B75B-3ED8E71059CB', '', 'img', 0, '2015-09-28 14:28:54', '2015-09-28 14:28:54'),
('5B5A958F-A858-4061-A3B3-FCFFD1D899A2', 'Sep', 'sep', '7AA6EE71-58C5-4EB3-9107-05ABA8EC8F4A', '<p>Sep</p>\n', 'Sep', 'Sep', 'Sep', 'Sep', '', '1F9F7BDE-36E6-4D40-BFF8-9AD23284D01A', 'vid', 0, '2015-09-28 14:41:59', '2015-09-28 14:41:59'),
('64F91323-E0B5-4BE2-A222-C4D295DE6B1A', 'Oct', 'oct', 'C80E6CCE-9160-46E6-9216-96BD0A4BC2B1', '<p>Oct</p>\n', 'Oct', 'Oct', 'Oct', 'Oct', '41E2614B-434F-471A-B7D4-0DDC7E62FC02', '', 'img', 1, '2015-09-28 14:43:13', '2015-09-28 14:43:13'),
('75906738-8BBF-4F3E-909A-4500BCD950CE', 'Aug', 'aug', '8ACD4E6A-2218-45D5-B73A-72B4FF22BD2E', '<p>Aug</p>\n', 'Aug', 'Aug', 'Aug', 'Aug', '532F4857-9852-4AA5-91D7-0442B695F473', '', 'img', 1, '2015-09-28 14:41:15', '2015-09-28 14:41:15'),
('75DE313D-2D69-456A-B355-107A2BDF8F87', 'feb', 'feb', 'F5781230-00B4-4C7D-99C9-529D9A340028', '<p>feb</p>\n', 'feb', 'feb', 'feb', 'feb', '', '1F9F7BDE-36E6-4D40-BFF8-9AD23284D01A', 'vid', 1, '2015-09-29 00:00:00', '2015-09-28 13:31:44'),
('AC846FE0-6B4B-4857-8B03-A46AE1EDFDD7', 'Jun', 'jun', 'F5781230-00B4-4C7D-99C9-529D9A340028', '<p>Jun</p>\n', 'Jun', 'Jun', 'Jun', 'Jun', '', '44A58B61-F1C0-46A5-9BA1-883D4C5E14DA', 'vid', 1, '2015-09-28 14:38:41', '2015-09-28 14:38:41'),
('CBAA82A8-8B73-459A-BF58-6F73263BDF2C', 'Jul', 'jul', '8ACD4E6A-2218-45D5-B73A-72B4FF22BD2E', '<p>Jul</p>\n', 'Jul', 'Jul', 'Jul', 'Jul', '41E2614B-434F-471A-B7D4-0DDC7E62FC02', '', 'img', 1, '2015-09-28 14:40:07', '2015-09-28 14:40:07'),
('D3AEF286-1673-4AD1-8227-66F63593B5E2', 'Nov', 'nov', 'F5781230-00B4-4C7D-99C9-529D9A340028', '<p>Nov</p>\n', 'Nov', 'Nov', 'Nov', 'Nov', 'EB639B93-8D89-4DF7-BBBE-06206095D240', '', 'img', 1, '2015-09-28 14:44:55', '2015-09-28 14:44:55'),
('FBA3B2B1-1386-4876-A41B-B60C82C23920', 'jan', 'jan', 'F5781230-00B4-4C7D-99C9-529D9A340028', '<p>jan</p>\n', 'jan', 'jan', 'jan', 'jan', '41E2614B-434F-471A-B7D4-0DDC7E62FC02', '', 'img', 1, '2015-09-28 00:00:00', '2015-09-28 13:21:09');

-- --------------------------------------------------------

--
-- Table structure for table `newscategory`
--

CREATE TABLE IF NOT EXISTS `newscategory` (
  `id` varchar(50) NOT NULL,
  `categoryname` varchar(50) NOT NULL,
  `datecreated` date NOT NULL,
  `dateupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newscategory`
--

INSERT INTO `newscategory` (`id`, `categoryname`, `datecreated`, `dateupdated`) VALUES
('00520C3F-4C86-4A15-AF9F-FD732A021BF5', 'Tao', '2015-09-24', '2015-09-28 00:52:40'),
('08BE8BC5-15DA-4E8B-8AFC-96F969C6E41B', 'Lugar', '2015-09-28', '2015-09-28 00:53:11'),
('1F6DECA1-799F-45B1-A3D9-92DD2772D421', 'Pusa', '2015-09-28', '2015-09-28 00:53:35'),
('20E9241B-FBB8-441D-A026-648E9B3F5F4D', 'Wesdf', '2015-09-23', '2015-09-24 03:56:17'),
('227D2BAE-7FAF-4C32-85CA-AD3C39C6EB43', 'Medium', '2015-09-23', '2015-09-23 02:41:12'),
('669BDB30-0056-479F-9BE2-A7640D8827BE', 'Easy', '2015-09-23', '2015-09-23 02:40:56'),
('AF8DA5B0-4B13-4CCB-B917-5B72D7982431', 'Wew', '2015-09-23', '2015-09-23 02:51:00'),
('B315F99B-90D6-414A-B81B-5901C84615DC', 'Hayop', '2015-09-24', '2015-09-28 00:52:50'),
('D3BC0F72-69DC-4780-BCC5-93A2D9263B56', 'Panahon', '2015-09-23', '2015-09-28 00:53:17'),
('D65E9527-7204-4653-9CE4-83525956FEBA', 'Pagkain', '2015-09-24', '2015-09-28 00:53:04'),
('DC75930A-9413-4318-966E-1FD449646001', 'Hard', '2015-09-23', '2015-09-23 02:41:17'),
('E13BD074-223B-4B77-87C1-BDF56A6F74C5', 'Insane', '2015-09-02', '2015-09-23 02:41:21'),
('F654D72B-BED0-46AA-8676-66C307467CA9', 'Bagay', '2015-09-23', '2015-09-28 00:52:57');

-- --------------------------------------------------------

--
-- Table structure for table `newsselectedcategories`
--

CREATE TABLE IF NOT EXISTS `newsselectedcategories` (
`id` int(11) NOT NULL,
  `categoryid` varchar(100) NOT NULL,
  `newsid` varchar(100) NOT NULL,
  `datecreated` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsselectedcategories`
--

INSERT INTO `newsselectedcategories` (`id`, `categoryid`, `newsid`, `datecreated`) VALUES
(4, 'DC75930A-9413-4318-966E-1FD449646001', 'FBA3B2B1-1386-4876-A41B-B60C82C23920', '2015-09-28 13:21:10'),
(5, 'B315F99B-90D6-414A-B81B-5901C84615DC', 'FBA3B2B1-1386-4876-A41B-B60C82C23920', '2015-09-28 13:21:10'),
(6, 'B315F99B-90D6-414A-B81B-5901C84615DC', '75DE313D-2D69-456A-B355-107A2BDF8F87', '2015-09-28 13:31:44'),
(7, 'E13BD074-223B-4B77-87C1-BDF56A6F74C5', '75DE313D-2D69-456A-B355-107A2BDF8F87', '2015-09-28 13:31:44'),
(8, '669BDB30-0056-479F-9BE2-A7640D8827BE', '283ACAC8-6179-42CB-AA79-28EBC6044420', '2015-09-28 13:32:52'),
(9, 'D65E9527-7204-4653-9CE4-83525956FEBA', '585A388C-5E8D-4FDE-A9F1-B37D4B5A4D5A', '2015-09-28 14:28:54'),
(10, '00520C3F-4C86-4A15-AF9F-FD732A021BF5', '3A604CBF-E728-41B0-B575-AFB4C2C787E7', '2015-09-28 14:37:35'),
(11, 'DC75930A-9413-4318-966E-1FD449646001', 'AC846FE0-6B4B-4857-8B03-A46AE1EDFDD7', '2015-09-28 14:38:41'),
(12, 'E13BD074-223B-4B77-87C1-BDF56A6F74C5', 'AC846FE0-6B4B-4857-8B03-A46AE1EDFDD7', '2015-09-28 14:38:41'),
(13, '08BE8BC5-15DA-4E8B-8AFC-96F969C6E41B', 'CBAA82A8-8B73-459A-BF58-6F73263BDF2C', '2015-09-28 14:40:07'),
(14, '1F6DECA1-799F-45B1-A3D9-92DD2772D421', '75906738-8BBF-4F3E-909A-4500BCD950CE', '2015-09-28 14:41:15'),
(15, 'D3BC0F72-69DC-4780-BCC5-93A2D9263B56', '5B5A958F-A858-4061-A3B3-FCFFD1D899A2', '2015-09-28 14:41:59'),
(16, '08BE8BC5-15DA-4E8B-8AFC-96F969C6E41B', '64F91323-E0B5-4BE2-A222-C4D295DE6B1A', '2015-09-28 14:43:13'),
(17, 'E13BD074-223B-4B77-87C1-BDF56A6F74C5', 'D3AEF286-1673-4AD1-8227-66F63593B5E2', '2015-09-28 14:44:55');

-- --------------------------------------------------------

--
-- Table structure for table `newsselectedtags`
--

CREATE TABLE IF NOT EXISTS `newsselectedtags` (
`id` int(11) NOT NULL,
  `tagid` varchar(100) NOT NULL,
  `newsid` varchar(100) NOT NULL,
  `datecreated` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsselectedtags`
--

INSERT INTO `newsselectedtags` (`id`, `tagid`, `newsid`, `datecreated`) VALUES
(4, '404A27C0-2B50-4652-86CF-2F39835A1E35', 'FBA3B2B1-1386-4876-A41B-B60C82C23920', '2015-09-28 13:21:10'),
(5, 'F4A8DC1C-BF7A-4E01-B608-7DD550363573', 'FBA3B2B1-1386-4876-A41B-B60C82C23920', '2015-09-28 13:21:10'),
(6, '404A27C0-2B50-4652-86CF-2F39835A1E35', '75DE313D-2D69-456A-B355-107A2BDF8F87', '2015-09-28 13:31:44'),
(7, '39DAF80C-E515-4941-890F-EA864DFED785', '75DE313D-2D69-456A-B355-107A2BDF8F87', '2015-09-28 13:31:44'),
(8, 'F4A8DC1C-BF7A-4E01-B608-7DD550363573', '283ACAC8-6179-42CB-AA79-28EBC6044420', '2015-09-28 13:32:52'),
(9, '7A6CBD6E-7582-46B6-8C09-9743410D429D', '585A388C-5E8D-4FDE-A9F1-B37D4B5A4D5A', '2015-09-28 14:28:54'),
(10, '230B3E54-7F69-4E8F-97E2-8C4E2BCCFCA0', '585A388C-5E8D-4FDE-A9F1-B37D4B5A4D5A', '2015-09-28 14:28:54'),
(11, 'F4A8DC1C-BF7A-4E01-B608-7DD550363573', '3A604CBF-E728-41B0-B575-AFB4C2C787E7', '2015-09-28 14:37:35'),
(12, 'F4A8DC1C-BF7A-4E01-B608-7DD550363573', 'AC846FE0-6B4B-4857-8B03-A46AE1EDFDD7', '2015-09-28 14:38:41'),
(13, '090440B3-6063-464C-9A2D-FCDEA7FC6CEA', 'AC846FE0-6B4B-4857-8B03-A46AE1EDFDD7', '2015-09-28 14:38:41'),
(14, '39DAF80C-E515-4941-890F-EA864DFED785', 'CBAA82A8-8B73-459A-BF58-6F73263BDF2C', '2015-09-28 14:40:07'),
(15, 'FB9ABC39-714A-48AF-B637-2A905A18C131', '75906738-8BBF-4F3E-909A-4500BCD950CE', '2015-09-28 14:41:15'),
(16, '090440B3-6063-464C-9A2D-FCDEA7FC6CEA', '5B5A958F-A858-4061-A3B3-FCFFD1D899A2', '2015-09-28 14:41:59'),
(17, 'F4A8DC1C-BF7A-4E01-B608-7DD550363573', '64F91323-E0B5-4BE2-A222-C4D295DE6B1A', '2015-09-28 14:43:13'),
(18, '404A27C0-2B50-4652-86CF-2F39835A1E35', 'D3AEF286-1673-4AD1-8227-66F63593B5E2', '2015-09-28 14:44:55');

-- --------------------------------------------------------

--
-- Table structure for table `newstags`
--

CREATE TABLE IF NOT EXISTS `newstags` (
  `id` varchar(50) NOT NULL,
  `tagname` varchar(50) NOT NULL,
  `datecreated` date NOT NULL,
  `dateupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newstags`
--

INSERT INTO `newstags` (`id`, `tagname`, `datecreated`, `dateupdated`) VALUES
('090440B3-6063-464C-9A2D-FCDEA7FC6CEA', 'Double Dutch', '2015-09-23', '2015-09-23 02:40:28'),
('230B3E54-7F69-4E8F-97E2-8C4E2BCCFCA0', 'Ube', '2015-09-23', '2015-09-23 02:40:48'),
('39DAF80C-E515-4941-890F-EA864DFED785', 'Cookies & Cream', '2015-09-23', '2015-09-23 02:37:30'),
('404A27C0-2B50-4652-86CF-2F39835A1E35', 'Choco Na Gatas', '2015-09-23', '2015-09-24 03:37:31'),
('5C20D757-DE30-4D0E-A8B1-1A046BA9247C', 'Cheese', '2015-09-28', '2015-09-28 00:58:52'),
('7A6CBD6E-7582-46B6-8C09-9743410D429D', 'Pastillas', '2015-09-28', '2015-09-28 00:58:23'),
('F4A8DC1C-BF7A-4E01-B608-7DD550363573', 'Coffee', '2015-09-23', '2015-09-24 03:37:53'),
('FB9ABC39-714A-48AF-B637-2A905A18C131', 'Rocky Road', '2015-09-23', '2015-09-23 02:37:22');

-- --------------------------------------------------------

--
-- Table structure for table `queryerror`
--

CREATE TABLE IF NOT EXISTS `queryerror` (
`error_id` int(10) unsigned NOT NULL,
  `query` text,
  `file` varchar(1024) DEFAULT '',
  `line` int(10) unsigned DEFAULT NULL,
  `error_string` varchar(1024) DEFAULT '',
  `error_no` int(10) unsigned DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `execution_script` varchar(1024) DEFAULT '',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(16) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rentalhours`
--

CREATE TABLE IF NOT EXISTS `rentalhours` (
  `id` varchar(50) NOT NULL,
  `session` varchar(50) NOT NULL,
  `starttime` varchar(50) NOT NULL,
  `endtime` varchar(50) NOT NULL,
  `datecreated` datetime NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `dateupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rentalhours`
--

INSERT INTO `rentalhours` (`id`, `session`, `starttime`, `endtime`, `datecreated`, `description`, `dateupdated`) VALUES
('03031545-CC34-4C16-8BFA-8A61C941E608', 'Wew', '1:00 AM', '3:30 AM', '2015-09-10 11:54:54', NULL, '2015-09-10 03:54:54'),
('1261888F-9ABC-4D26-90B6-9FD383C145EF', 'Tuknining', '7:00 AM', '8:30 AM', '2015-08-28 18:20:42', '', '2015-09-10 03:54:45'),
('16AAEDED-4CC9-499C-994C-2B1CA04401E0', 'Special', '7:00 AM', '11:30 AM', '2015-08-28 18:20:50', NULL, '2015-08-28 10:20:50'),
('5D23AD17-461F-499E-B76F-C019956EC6F0', 'Da', '7:00 AM', '8:30 AM', '2015-09-02 16:57:56', NULL, '2015-09-02 08:57:56'),
('ABB1DA25-9EAE-435D-88A8-8F94775A77EE', 'Dudun Wave', '7:00 AM', '10:00 AM', '2015-08-28 18:20:33', 'wew', '2015-09-10 03:54:30');

-- --------------------------------------------------------

--
-- Table structure for table `rentalhoursmsg`
--

CREATE TABLE IF NOT EXISTS `rentalhoursmsg` (
`id` int(11) NOT NULL,
  `content` text NOT NULL,
  `datecreated` datetime NOT NULL,
  `dateupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rentalhoursmsg`
--

INSERT INTO `rentalhoursmsg` (`id`, `content`, `datecreated`, `dateupdated`) VALUES
(1, '<p><span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">Check-in anytime before noon to ride half a day.&nbsp;</span><br style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;" />\n<span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">Call 928-634-5990 Now to check availability! </span></p>\n\n<p>&nbsp;</p>\n\n<p><span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px;">TEST</span></p>\n', '2015-09-01 10:43:05', '2015-09-03 04:58:22');

-- --------------------------------------------------------

--
-- Table structure for table `repliedmessage`
--

CREATE TABLE IF NOT EXISTS `repliedmessage` (
`id` int(11) NOT NULL,
  `rid` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `datereplied` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `repliedmessage`
--

INSERT INTO `repliedmessage` (`id`, `rid`, `message`, `datereplied`) VALUES
(1, 'FE71D19F-11F1-4FD1-ACA6-3607520F3783', 'wew', '2015-08-24 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `requirements`
--

CREATE TABLE IF NOT EXISTS `requirements` (
  `id` varchar(50) NOT NULL,
  `requirements` text NOT NULL,
  `datecreated` datetime NOT NULL,
  `dateupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `requirements`
--

INSERT INTO `requirements` (`id`, `requirements`, `datecreated`, `dateupdated`) VALUES
('026D6AD3-E779-4C8E-9101-D9169ABDFC0A', 'Driver Minimum Age Is 18 Years Old', '2015-08-27 11:32:27', '2015-08-28 06:11:34'),
('59D1973B-9EC2-448F-947D-E1B6EAEFDF26', 'Security Deposit ($500), Require To Purchase Renter''s Insurance ($18) At Our Office', '2015-08-27 11:33:34', '2015-08-27 05:02:45'),
('5F31FB71-B34A-4E10-8F0C-99C6402A3E29', 'Primary Renter Must be 21 years Old', '2015-08-27 11:32:33', '2015-08-27 03:32:33'),
('A02F15C8-9409-4176-B13A-392342D41963', 'Valid Driver''s License', '2015-08-27 11:33:20', '2015-08-27 05:02:23'),
('B6BD2D62-02BF-403D-AA13-FC1B64A29E6E', 'Please check in 30 minutes before your riding for paperwork and orientation', '2015-08-27 11:33:43', '2015-08-27 03:33:43'),
('CAE6E627-2824-4BF0-99C1-4298F50E9584', 'TEST', '2015-09-03 12:57:48', '2015-09-03 04:57:48');

-- --------------------------------------------------------

--
-- Table structure for table `reservationlist`
--

CREATE TABLE IF NOT EXISTS `reservationlist` (
`id` int(11) NOT NULL,
  `atvid` varchar(50) NOT NULL,
  `customerid` varchar(50) NOT NULL,
  `rentalid` varchar(50) NOT NULL,
  `trailid` varchar(50) NOT NULL,
  `reservationdate` text NOT NULL,
  `status` int(11) NOT NULL,
  `datecreated` datetime NOT NULL,
  `dateupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservationlist`
--

INSERT INTO `reservationlist` (`id`, `atvid`, `customerid`, `rentalid`, `trailid`, `reservationdate`, `status`, `datecreated`, `dateupdated`) VALUES
(1, '05BC636C-ECB3-44F8-B7B5-0155C667BEE4', 'D9C975EB-3403-4D9D-9B5C-A185B4A91270', '1261888F-9ABC-4D26-90B6-9FD383C145EF', '672F6EF4-7757-489D-B11E-FB17D2214AA5', '2015-09-25', 1, '2015-09-18 16:35:20', '2015-09-18 08:51:11'),
(2, '05BC636C-ECB3-44F8-B7B5-0155C667BEE4', 'D9C975EB-3403-4D9D-9B5C-A185B4A91270', '1261888F-9ABC-4D26-90B6-9FD383C145EF', '672F6EF4-7757-489D-B11E-FB17D2214AA5', '2015-09-23', 1, '2015-09-18 16:35:20', '2015-09-18 08:51:16'),
(3, '05BC636C-ECB3-44F8-B7B5-0155C667BEE4', 'C22C21CF-ABA0-4214-87E2-F6100BA85002', '16AAEDED-4CC9-499C-994C-2B1CA04401E0', '2AF61A94-45B5-44D1-AD77-26D14DF27DDA', '2015-09-24', 0, '2015-09-21 10:25:39', '2015-09-21 02:25:39'),
(4, 'E5DA801F-3C13-41F4-BDFC-367B0425F784', 'C22C21CF-ABA0-4214-87E2-F6100BA85002', 'ABB1DA25-9EAE-435D-88A8-8F94775A77EE', '2AF61A94-45B5-44D1-AD77-26D14DF27DDA', '2015-09-23', 1, '2015-09-21 10:25:39', '2015-09-21 02:27:05');

-- --------------------------------------------------------

--
-- Table structure for table `runtimeerror`
--

CREATE TABLE IF NOT EXISTS `runtimeerror` (
`error_id` int(10) unsigned NOT NULL,
  `title` varchar(2048) NOT NULL DEFAULT '',
  `file` varchar(1024) DEFAULT '',
  `line` int(10) unsigned DEFAULT NULL,
  `error_type` int(10) unsigned NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `server_name` varchar(100) DEFAULT NULL,
  `execution_script` varchar(1024) NOT NULL DEFAULT '',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(16) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `specialoffers`
--

CREATE TABLE IF NOT EXISTS `specialoffers` (
`id` int(11) NOT NULL,
  `content` text NOT NULL,
  `datecreated` datetime NOT NULL,
  `dateupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specialoffers`
--

INSERT INTO `specialoffers` (`id`, `content`, `datecreated`, `dateupdated`) VALUES
(1, '<p>Solar Body Campaign Special 10% off<br />\nGet your FREE BOOK Now, Get 10% off.<br />\n<a href="http://www.changeyourenergy.com/the-solar-body?ref=153" target="_blank">&gt;&gt; Click to see more</a><br />\n<br />\nRio Verde RV Park Guests 10% off<br />\nWe offer 10% off for Rio Verde RV Park guests.<br />\n<br />\nHappy Birthday Special 30% off<br />\nYou can get 30% off for your birthday, starting from a week before your birthday to a week after your birthday.<br />\n<br />\n* These offers cannot be combined with the Sunset Special. Multiple offers cannot be applied to your rental.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>wewzzzz</p>\n', '2015-09-11 10:07:38', '2015-09-11 03:16:49');

-- --------------------------------------------------------

--
-- Table structure for table `trails`
--

CREATE TABLE IF NOT EXISTS `trails` (
  `id` varchar(50) NOT NULL,
  `title` varchar(500) NOT NULL,
  `distance` varchar(200) NOT NULL,
  `elevation` varchar(200) NOT NULL,
  `dlf` varchar(200) NOT NULL,
  `dlt` varchar(200) NOT NULL,
  `wtb` text NOT NULL,
  `description` text NOT NULL,
  `featuredimage` text NOT NULL,
  `firstimage` text NOT NULL,
  `secondimage` text NOT NULL,
  `datecreated` date NOT NULL,
  `dateupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trails`
--

INSERT INTO `trails` (`id`, `title`, `distance`, `elevation`, `dlf`, `dlt`, `wtb`, `description`, `featuredimage`, `firstimage`, `secondimage`, `datecreated`, `dateupdated`) VALUES
('2AF61A94-45B5-44D1-AD77-26D14DF27DDA', 'test1', '1', 'test', 'easy', 'easy', 'test', '<p>test</p>\n', '1.jpg', '1.jpg', '1.jpg', '2015-09-14', '2015-09-14 08:15:39'),
('672F6EF4-7757-489D-B11E-FB17D2214AA5', 'test', '2', 'test', 'moderate', 'moderate', 'test', '<p>test</p>\n', '1.jpg', '1.jpg', '1.jpg', '2015-09-14', '2015-09-14 08:14:48'),
('AE4B115A-5B4E-4ACA-96AA-34BA53D124DE', 'test2', 'test2', 'test2', 'easy', 'difficult', 'test2', '<p>test2</p>\n', '1c52c7e399628d50f5c9711110809dca.png', '1.jpg', '1.jpg', '2015-09-14', '2015-09-14 08:16:24');

-- --------------------------------------------------------

--
-- Table structure for table `trailsmap`
--

CREATE TABLE IF NOT EXISTS `trailsmap` (
`id` int(11) NOT NULL,
  `filename` text NOT NULL,
  `rentalid` varchar(50) NOT NULL,
  `trailid` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trailsmap`
--

INSERT INTO `trailsmap` (`id`, `filename`, `rentalid`, `trailid`) VALUES
(23, '1.jpg', 'ABB1DA25-9EAE-435D-88A8-8F94775A77EE', '672F6EF4-7757-489D-B11E-FB17D2214AA5'),
(24, '1.jpg', '1261888F-9ABC-4D26-90B6-9FD383C145EF', '2AF61A94-45B5-44D1-AD77-26D14DF27DDA'),
(25, '1.jpg', '03031545-CC34-4C16-8BFA-8A61C941E608', 'AE4B115A-5B4E-4ACA-96AA-34BA53D124DE');

-- --------------------------------------------------------

--
-- Table structure for table `userimage`
--

CREATE TABLE IF NOT EXISTS `userimage` (
`id` int(11) NOT NULL,
  `filename` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userimage`
--

INSERT INTO `userimage` (`id`, `filename`) VALUES
(2, 'dg4dnk1q0k914395409085430.png'),
(3, 'zg99jhy3nmi14395422133150.jpg'),
(4, 'cx9hot21emi14395422485720.jpg'),
(5, 'qril766r14395467243400.jpg'),
(6, 'z4me4tjra4i14395468358310.png'),
(7, 'lomwa02j4i14397887257500.png'),
(8, 't982bsdobt914416919077620.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(250) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `birthday` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `profile_pic_name` varchar(250) DEFAULT NULL,
  `activation_code` varchar(150) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `task` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `first_name`, `last_name`, `birthday`, `gender`, `profile_pic_name`, `activation_code`, `status`, `task`, `created_at`, `updated_at`) VALUES
('03686d5c-bd04-45a4-d8ff-5f7a00ee23df', 'superagent', 'efrenbautistajr@geeksnest.com', '7c222fb2927d828af22f592134e8932480637c0d', 'efren', 'bautista', '1986-02-15', 'Male', 'z4me4tjra4i14395468358310.png', NULL, 1, 'ADMIN', '2015-04-20 16:09:57', '2015-08-17 05:02:00'),
('2E96F422-EF29-4CA4-93A1-55E5DC113D8A', 'heaven', 'leih123@mailinator.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'leih', 'mojica', '2015-08-07', 'Female', 'qril766r14395467243400.jpg', '58359C75-B182-44A0-A03B-83DF10847C0B', 1, 'CUSTOMER', '2015-08-18 10:58:57', '2015-08-18 02:58:57'),
('889D6CAB-C8A9-4A2F-8BDA-D60E2BF3C046', 'qweqwe', 'qweqe@qweqwe', 'b33b5e3e04dae7c04d1e4dc759ca5c80e26e576a', 'xcvx', 'cvxcvxcv', '2015-08-19', 'Female', 'lomwa02j4i14397887257500.png', 'F4385340-AC2E-4568-87BB-79CD7F2EC599', 0, 'CUSTOMER', '2015-08-18 13:13:24', '2015-08-18 05:13:24'),
('9F07A2F5-BB1D-4DF2-884D-C23004819956', 'heaven1', 'heaven@yahoo.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'heaven', 'mojica', '2015-08-20', 'Female', 'qril766r14395467243400.jpg', 'A8CE9F29-6948-46DB-8FF0-3B70DCE99DD5', 1, 'CUSTOMER', '2015-08-18 13:32:11', '2015-08-18 05:32:11'),
('C62DEE80-CA17-432D-AE51-6AFC57CEBB90', 'vbbvnv', 'bnvbnbnv@WQqwe', '3d72095e42f81fc9cf54c8c18ee6ddeb997dd78a', 'dfgdfgd', 'fgdfgdfg', '2015-08-28', 'Female', 'dg4dnk1q0k914395409085430.png', '47D84ABA-8C0E-4216-BBB0-58BFB850384C', 1, 'CUSTOMER', '2015-08-14 17:56:29', '2015-08-14 09:56:29'),
('EB6BC48E-CBDD-4113-8EDC-78114ADAD1A2', 'heavenleih', 'heavenmojica@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'heaven', 'mojica', '2015-08-21', 'Female', 'zg99jhy3nmi14395422133150.jpg', 'EB907C87-D651-4C29-8BA5-0B77747CEEB5', 1, 'CUSTOMER', '2015-08-18 10:57:34', '2015-08-18 02:57:34');

-- --------------------------------------------------------

--
-- Table structure for table `videocollection`
--

CREATE TABLE IF NOT EXISTS `videocollection` (
  `id` varchar(200) NOT NULL,
  `embed` text NOT NULL,
  `category` varchar(50) NOT NULL,
  `datecreated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videocollection`
--

INSERT INTO `videocollection` (`id`, `embed`, `category`, `datecreated`) VALUES
('1F9F7BDE-36E6-4D40-BFF8-9AD23284D01A', '<iframe width="420" height="315" src="https://www.youtube.com/embed/DyAxDBO_Jnc" frameborder="0" allowfullscreen></iframe>', 'news', '2015-09-28 07:26:43'),
('44A58B61-F1C0-46A5-9BA1-883D4C5E14DA', '<iframe width="560" height="315" src="https://www.youtube.com/embed/hcsFNyrUPBA" frameborder="0" allowfullscreen></iframe>', 'news', '2015-09-28 07:36:11'),
('BC9E5487-8340-4001-9172-0F55D10C2087', '<iframe width="560" height="315" src="https://www.youtube.com/embed/Btg5PjyO_UU" frameborder="0" allowfullscreen></iframe>', 'news', '2015-09-28 14:27:42');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
`id` int(11) NOT NULL,
  `embed` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `embed`) VALUES
(21, '<iframe width="420" height="315" src="https://www.youtube.com/embed/r5lKwIZT2vY" frameborder="0" allowfullscreen></iframe>'),
(22, '<iframe width="560" height="315" src="https://www.youtube.com/embed/rSB15oq-DME" frameborder="0" allowfullscreen></iframe>'),
(23, '<iframe width="560" height="315" src="https://www.youtube.com/embed/hVmHX55XDYk" frameborder="0" allowfullscreen></iframe>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api`
--
ALTER TABLE `api`
 ADD PRIMARY KEY (`client_id`), ADD UNIQUE KEY `private_key` (`private_key`), ADD UNIQUE KEY `public_id` (`public_id`);

--
-- Indexes for table `atvimages`
--
ALTER TABLE `atvimages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atvprices`
--
ALTER TABLE `atvprices`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atvrentalprices`
--
ALTER TABLE `atvrentalprices`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deliver`
--
ALTER TABLE `deliver`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forgotpassword`
--
ALTER TABLE `forgotpassword`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `freebies`
--
ALTER TABLE `freebies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newscategory`
--
ALTER TABLE `newscategory`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsselectedcategories`
--
ALTER TABLE `newsselectedcategories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsselectedtags`
--
ALTER TABLE `newsselectedtags`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newstags`
--
ALTER TABLE `newstags`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `queryerror`
--
ALTER TABLE `queryerror`
 ADD PRIMARY KEY (`error_id`);

--
-- Indexes for table `rentalhours`
--
ALTER TABLE `rentalhours`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rentalhoursmsg`
--
ALTER TABLE `rentalhoursmsg`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `repliedmessage`
--
ALTER TABLE `repliedmessage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requirements`
--
ALTER TABLE `requirements`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservationlist`
--
ALTER TABLE `reservationlist`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `runtimeerror`
--
ALTER TABLE `runtimeerror`
 ADD PRIMARY KEY (`error_id`);

--
-- Indexes for table `specialoffers`
--
ALTER TABLE `specialoffers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trails`
--
ALTER TABLE `trails`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trailsmap`
--
ALTER TABLE `trailsmap`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userimage`
--
ALTER TABLE `userimage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videocollection`
--
ALTER TABLE `videocollection`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api`
--
ALTER TABLE `api`
MODIFY `client_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `atvimages`
--
ALTER TABLE `atvimages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `atvrentalprices`
--
ALTER TABLE `atvrentalprices`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `deliver`
--
ALTER TABLE `deliver`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `forgotpassword`
--
ALTER TABLE `forgotpassword`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `freebies`
--
ALTER TABLE `freebies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `newsselectedcategories`
--
ALTER TABLE `newsselectedcategories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `newsselectedtags`
--
ALTER TABLE `newsselectedtags`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `queryerror`
--
ALTER TABLE `queryerror`
MODIFY `error_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rentalhoursmsg`
--
ALTER TABLE `rentalhoursmsg`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `repliedmessage`
--
ALTER TABLE `repliedmessage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reservationlist`
--
ALTER TABLE `reservationlist`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `runtimeerror`
--
ALTER TABLE `runtimeerror`
MODIFY `error_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `specialoffers`
--
ALTER TABLE `specialoffers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `trailsmap`
--
ALTER TABLE `trailsmap`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `userimage`
--
ALTER TABLE `userimage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
