-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 17, 2015 at 09:52 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbatv`
--

-- --------------------------------------------------------

--
-- Table structure for table `api`
--

CREATE TABLE IF NOT EXISTS `api` (
`client_id` int(10) unsigned NOT NULL,
  `public_id` char(64) NOT NULL DEFAULT '',
  `private_key` char(64) NOT NULL DEFAULT '',
  `status` enum('ACTIVE','INACTIVE') DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `api`
--

INSERT INTO `api` (`client_id`, `public_id`, `private_key`, `status`) VALUES
(1, '', '593fe6ed77014f9507761028801aa376f141916bd26b1b3f0271b5ec3135b989', 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `queryerror`
--

CREATE TABLE IF NOT EXISTS `queryerror` (
`error_id` int(10) unsigned NOT NULL,
  `query` text,
  `file` varchar(1024) DEFAULT '',
  `line` int(10) unsigned DEFAULT NULL,
  `error_string` varchar(1024) DEFAULT '',
  `error_no` int(10) unsigned DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `execution_script` varchar(1024) DEFAULT '',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(16) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `runtimeerror`
--

CREATE TABLE IF NOT EXISTS `runtimeerror` (
`error_id` int(10) unsigned NOT NULL,
  `title` varchar(2048) NOT NULL DEFAULT '',
  `file` varchar(1024) DEFAULT '',
  `line` int(10) unsigned DEFAULT NULL,
  `error_type` int(10) unsigned NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `server_name` varchar(100) DEFAULT NULL,
  `execution_script` varchar(1024) NOT NULL DEFAULT '',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(16) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userimage`
--

CREATE TABLE IF NOT EXISTS `userimage` (
`id` int(11) NOT NULL,
  `filename` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userimage`
--

INSERT INTO `userimage` (`id`, `filename`) VALUES
(2, 'dg4dnk1q0k914395409085430.png'),
(3, 'zg99jhy3nmi14395422133150.jpg'),
(4, 'cx9hot21emi14395422485720.jpg'),
(5, 'qril766r14395467243400.jpg'),
(6, 'z4me4tjra4i14395468358310.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(250) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `birthday` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `profile_pic_name` varchar(250) DEFAULT NULL,
  `activation_code` varchar(150) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `task` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `first_name`, `last_name`, `birthday`, `gender`, `profile_pic_name`, `activation_code`, `status`, `task`, `created_at`, `updated_at`) VALUES
('03686d5c-bd04-45a4-d8ff-5f7a00ee23df', 'superagent', 'efrenbautistajr@geeksnest.com', '7c222fb2927d828af22f592134e8932480637c0d', 'efren', 'bautista', '1986-02-15', 'male', NULL, NULL, 1, 'ADMIN', '2015-04-20 16:09:57', '2015-08-14 09:57:00'),
('0F88F406-B118-41C9-B761-DF03BD7D972D', 'cvbcvbcvb', 'cvbcqweqwe@qwe', '7938c8d8a0e22c0f29a07793c101cb7f8a7f562f', 'sdfdsfsdfsdf', 'dfgdfgdfg', '2015-08-20', 'Female', 'z4me4tjra4i14395468358310.png', '254C08CD-73C5-4B9F-B4E1-7858F285C9C3', 1, 'ADMIN', '2015-08-14 18:07:23', '2015-08-14 10:07:23'),
('C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 'ebautistaj', 'efrenbautistajr@gmail.com', '13ec182672f07a51b64d88483486189000cc4d87', 'efren', 'bautista', '1999-03-15', 'male', NULL, '', 1, 'CUSTOMER', '2015-04-20 16:09:57', '2015-08-14 09:57:07'),
('C62DEE80-CA17-432D-AE51-6AFC57CEBB90', 'vbbvnv', 'bnvbnbnv@WQqwe', '3d72095e42f81fc9cf54c8c18ee6ddeb997dd78a', 'dfgdfgd', 'fgdfgdfg', '2015-08-28', 'Female', 'dg4dnk1q0k914395409085430.png', '47D84ABA-8C0E-4216-BBB0-58BFB850384C', 1, 'CUSTOMER', '2015-08-14 17:56:29', '2015-08-14 09:56:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api`
--
ALTER TABLE `api`
 ADD PRIMARY KEY (`client_id`), ADD UNIQUE KEY `private_key` (`private_key`), ADD UNIQUE KEY `public_id` (`public_id`);

--
-- Indexes for table `queryerror`
--
ALTER TABLE `queryerror`
 ADD PRIMARY KEY (`error_id`);

--
-- Indexes for table `runtimeerror`
--
ALTER TABLE `runtimeerror`
 ADD PRIMARY KEY (`error_id`);

--
-- Indexes for table `userimage`
--
ALTER TABLE `userimage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api`
--
ALTER TABLE `api`
MODIFY `client_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `queryerror`
--
ALTER TABLE `queryerror`
MODIFY `error_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `runtimeerror`
--
ALTER TABLE `runtimeerror`
MODIFY `error_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userimage`
--
ALTER TABLE `userimage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
