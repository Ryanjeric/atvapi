-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2015 at 08:09 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ryanjericdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `api`
--

CREATE TABLE IF NOT EXISTS `api` (
`client_id` int(10) unsigned NOT NULL,
  `public_id` char(64) NOT NULL DEFAULT '',
  `private_key` char(64) NOT NULL DEFAULT '',
  `status` enum('ACTIVE','INACTIVE') DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `api`
--

INSERT INTO `api` (`client_id`, `public_id`, `private_key`, `status`) VALUES
(1, '', '593fe6ed77014f9507761028801aa376f141916bd26b1b3f0271b5ec3135b989', 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `maps`
--

CREATE TABLE IF NOT EXISTS `maps` (
  `id` varchar(100) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` varchar(500) NOT NULL,
  `agent` varchar(100) NOT NULL,
  `hide_agent` int(11) NOT NULL,
  `status` int(11) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `maps`
--

INSERT INTO `maps` (`id`, `title`, `description`, `agent`, `hide_agent`, `status`, `created_at`, `updated_at`) VALUES
('07934B14-6DA4-44DC-A8C6-25BFEE13C0DD', 'adfasdfasdfa', 'adfasdfasdfa', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-18 03:00:15', '2015-05-17 19:00:15'),
('0F78001A-71D4-4337-B707-B940D721086B', 'adfadsfads', 'adfadsfads', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-18 02:58:15', '2015-05-17 18:58:15'),
('1F7011C9-BC23-482E-ADE2-484B760F2C23', 'adsfadsfad', 'adsfadsfad', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 04:16:57', '2015-05-07 20:16:57'),
('2BC56A40-BF5F-417B-B44C-17ECAFD5C6B9', 'fasdfasdfa', 'fasdfasdfa', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 04:26:28', '2015-05-07 20:26:28'),
('2E353BE4-311A-4F8B-974B-7391E28AB7DC', 'asdfasfa', 'asdfasfa', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 16:29:21', '2015-05-08 08:29:21'),
('423E5715-B765-4546-9CB4-C6E1B009677F', 'adsfadsfadsf', 'adsfadsfadsf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 04:24:29', '2015-05-07 20:24:29'),
('444FF584-8FF7-49D1-8BAB-28C4B966CF55', 'adsfdasadsfdasf', 'adsfdasadsfdasf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 16:47:44', '2015-05-08 08:47:44'),
('46B22175-8830-4722-9DE5-5F239F2BEC5F', 'asdfadsfasdf', 'asdfadsfasdf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 04:16:09', '2015-05-07 20:16:09'),
('4C6E7985-EF22-4B3A-A134-92B1A0F773E2', 'asdfasdfasdfasdfasdfasdfasdfasdfasdf', 'asdfasdfasdfasdfasdfasdfasdfasdfasdf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 02:58:42', '2015-05-07 18:58:42'),
('5394C2CB-FD3B-48BB-8C67-850373BBB84B', 'adfadsfadsfasdf', 'adfadsfadsfasdf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-14 19:37:52', '2015-05-14 11:37:52'),
('5BC0093E-DFAB-4985-BF5D-22FF66F9F88F', 'adsfasdfa', 'adsfasdfa', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-06-01 17:38:02', '2015-06-01 09:38:02'),
('5C3AC3F1-ACEB-4BFE-B0AA-2FD8B92471C1', 'fdgsdfsfdgsdfgsdfgsdfgsdf', 'fdgsdfsfdgsdfgsdfgsdfgsdf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 1, 1, '2015-05-08 15:50:42', '2015-05-08 07:50:42'),
('6610E2D1-3302-4B8F-8BF8-635F4E6341D8', 'adsfadsfasdfasd', 'adsfadsfasdfasd', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 15:59:40', '2015-05-08 07:59:40'),
('67EEAB50-563A-4C01-B600-FE4E453CB518', 'asdfasdfasdf', 'asdfasdfasdf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-21 13:28:46', '2015-05-21 05:28:46'),
('69743A7F-A026-43B7-A82B-2318F91D3431', 'asdfasdfasdfadsf', 'asdfasdfasdfadsf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-14 19:39:16', '2015-05-14 11:39:16'),
('6CEEACF8-1AAE-45D7-A013-2CEE00D00B2D', 'asdfasdfasdfasd', 'asdfasdfasdfasd', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-18 03:24:13', '2015-05-17 19:24:13'),
('6E797D2F-C680-43A6-A618-C27623001AD4', 'nfghnfnfngfh', 'nfghnfnfngfh', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 17:14:36', '2015-05-08 09:14:36'),
('72E6A1C8-B1C8-48CF-8ED8-6B403C57D9E3', 'adfadsfads', 'adfadsfads', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 16:42:18', '2015-05-08 08:42:18'),
('7857B682-64A0-4758-B650-25307798C80D', 'adfasfaf', 'adfasfaf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 16:50:50', '2015-05-08 08:50:50'),
('79E90B2F-5609-4750-85E7-5E25D0E9158D', 'asdfasdfasdfasdf', 'asdfasdfasdfasdf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-18 03:20:21', '2015-05-17 19:20:21'),
('7DFC24DF-ABE3-4133-BE60-9D723A392D99', 'adfasdfasd', 'adfasdfasd', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 1, 1, '2015-06-01 18:01:28', '2015-06-01 10:01:28'),
('897FDA88-53F4-47EB-9C99-85755B307EE6', 'adsf', 'adsf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 16:22:47', '2015-05-08 08:22:47'),
('8CA2F05A-0788-4BB8-BCFA-E4BE28294FD3', 'asdfadsf', 'asdfadsf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-18 03:28:53', '2015-05-17 19:28:53'),
('93F1D8FA-3C42-4D02-86F0-8E4A85402BC8', 'adsfasdfasd', 'adsfasdfasd', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-18 03:13:40', '2015-05-17 19:13:40'),
('9AED0B3C-B255-4FE6-AC0A-8637FBE7A098', 'asdfasd', 'asdfasd', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-14 19:36:24', '2015-05-14 11:36:24'),
('A7697029-7B46-4A84-B615-7F3C6CB0AEF5', 'adsfadsfadsfasdfas', 'adsfadsfadsfasdfas', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 04:22:03', '2015-05-07 20:22:03'),
('A7C333F3-6693-4FD6-A560-587BA7AF42A0', 'fgsdfgsdfg', 'fgsdfgsdfg', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 15:53:19', '2015-05-08 07:53:19'),
('B0A72476-EEE6-423B-A925-8F87FBCD8246', 'asdfasdf', 'asdfasdf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 04:23:21', '2015-05-07 20:23:21'),
('BD3BFB7E-3BAC-4179-8F6F-4F697A94922C', 'dsagasdgasdg', 'dsagasdgasdg', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-21 14:17:03', '2015-05-21 06:17:03'),
('BFDE175A-70EA-4568-BCB1-CCEBC754CBA4', 'adsfadsfadsfadsf', 'adsfadsfadsfadsf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 16:48:36', '2015-05-08 08:48:36'),
('D403221E-6EB3-4CD6-87EA-B3928C9EC8F1', 'asdfasdfasd', 'asdfasdfasd', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-18 02:59:00', '2015-05-17 18:59:00'),
('D6C52A06-2199-4FE0-9B2D-6B93C03D979A', 'adsfasdfasdf', 'adsfasdfasdf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-06-01 17:35:47', '2015-06-01 09:35:47'),
('E6B098E0-F540-46F3-9E2A-4143A0202AC7', 'asdfasdfasdf', 'asdfasdfasdf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-06-01 17:34:30', '2015-06-01 09:34:30'),
('E93EEC6D-FE9E-4CC4-8E55-43F801AD4130', 'adsfasdfasd', 'adsfasdfasd', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-18 03:38:37', '2015-05-17 19:38:37'),
('EF49855E-33E6-43C5-900C-FA5A1252629A', 'adsfasdfasdfasdf', 'adsfasdfasdfasdf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-18 02:33:44', '2015-05-17 18:33:44'),
('F1FD7294-F5E6-4135-BC63-B89E2CD75633', 'adsfasdfasdf', 'adsfasdfasdf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-21 14:25:21', '2015-05-21 06:25:21'),
('F85C8173-CB6C-41EE-84C1-01FB3FE64E5C', 'adsfadsadsfads', 'adsfadsadsfads', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 16:18:23', '2015-05-08 08:18:23'),
('F9A77AA7-56F8-41CB-B194-B5A6DDA220CA', 'fafdadsfadsfdasf', 'fafdadsfadsfdasf', 'C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 0, 1, '2015-05-08 17:00:36', '2015-05-08 09:00:36');

-- --------------------------------------------------------

--
-- Table structure for table `markerpics`
--

CREATE TABLE IF NOT EXISTS `markerpics` (
  `id` varchar(100) NOT NULL,
  `filename` varchar(150) DEFAULT NULL,
  `filesize` varchar(50) DEFAULT NULL,
  `filetype` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `markers`
--

CREATE TABLE IF NOT EXISTS `markers` (
  `id` varchar(100) NOT NULL,
  `map_id` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `video` text,
  `long` double NOT NULL,
  `lat` double NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `markers`
--

INSERT INTO `markers` (`id`, `map_id`, `description`, `video`, `long`, `lat`, `created_at`, `updated_at`) VALUES
('004f5a80-bdc2-48fb-d295-eb67a1c4cef4', 'E93EEC6D-FE9E-4CC4-8E55-43F801AD4130', 'sdfasdfasdfasdfasdf', NULL, -102.7001953125, 44.777935896316, '2015-05-18 03:38:37', '2015-05-17 19:38:37'),
('0159fbe8-345b-4894-ad81-13fe5a7c13f1', '69743A7F-A026-43B7-A82B-2318F91D3431', NULL, NULL, -92.3291015625, 39.232253141715, '2015-05-14 19:39:16', '2015-05-14 11:39:16'),
('01af5acd-ec40-44bf-9ab1-6d9f902eb3fb', '8CA2F05A-0788-4BB8-BCFA-E4BE28294FD3', 'adfadsfasfasdfas', NULL, -92.3291015625, 38.959408792454, '2015-05-18 03:28:53', '2015-05-17 19:28:53'),
('02ad7721-d93e-4d5e-abc7-17268dd5da4d', '67EEAB50-563A-4C01-B600-FE4E453CB518', 'asdfasdfasdfaf', NULL, -95.5810546875, 38.134556577054, '2015-05-21 13:28:46', '2015-05-21 05:28:46'),
('03686d5c-bd04-45a4-d8ff-5f7a00ee23df', '72E6A1C8-B1C8-48CF-8ED8-6B403C57D9E3', 'adfadsfadsfadsfadsf', NULL, -96.416015625, 37.439974052271, '2015-05-08 16:42:18', '2015-05-08 08:42:18'),
('063a7892-3de2-4ae6-8b61-d79ef12d000f', 'B0A72476-EEE6-423B-A925-8F87FBCD8246', 'asdfasdfasdfasdf', NULL, -86.220703125, 37.370157184058, '2015-05-08 04:23:21', '2015-05-07 20:23:21'),
('06ae90cd-4283-4be6-8d4a-7305f317deac', '444FF584-8FF7-49D1-8BAB-28C4B966CF55', 'adsfadsfasdfadsf', NULL, -97.734375, 35.460669951495, '2015-05-08 16:47:44', '2015-05-08 08:47:44'),
('085e66e0-1bc2-4fed-f268-2400dadd6fc9', '7DFC24DF-ABE3-4133-BE60-9D723A392D99', 'adsfasdfasdfasdf', '', -110.654296875, 39.571822237344, '2015-06-01 18:01:28', '2015-06-01 10:01:28'),
('09289c4b-3a4e-4a3a-d7fc-b3dc6808327d', '7857B682-64A0-4758-B650-25307798C80D', 'adsfasdfasdfasfsdfa', '', -96.240234375, 42.553080288956, '2015-05-08 16:50:50', '2015-05-08 08:50:50'),
('0a592e32-db23-4044-b41c-c93faef6558a', '423E5715-B765-4546-9CB4-C6E1B009677F', 'asdfasdfasdfadsf', NULL, -89.560546875, 36.59788913307, '2015-05-08 04:24:29', '2015-05-07 20:24:29'),
('0c160089-7cea-41c5-a522-8de7699b4436', '79E90B2F-5609-4750-85E7-5E25D0E9158D', 'asdfasdfasdfasdfdf', NULL, -93.8232421875, 37.090239803072, '2015-05-18 03:20:21', '2015-05-17 19:20:21'),
('0ff37d59-5be4-4793-9e10-cff063a20a3d', 'BD3BFB7E-3BAC-4179-8F6F-4F697A94922C', 'sdgasdgasdgasdg', NULL, -94.5263671875, 34.813803317113, '2015-05-21 14:17:03', '2015-05-21 06:17:03'),
('145832ef-cc0c-4a9f-a646-9e96b4f91dfa', '5BC0093E-DFAB-4985-BF5D-22FF66F9F88F', 'adsfadsfasdfasdf', '', -102.568359375, 36.668418918948, '2015-06-01 17:38:02', '2015-06-01 09:38:02'),
('15f06bc4-e365-4011-adb2-2d0935538362', 'F85C8173-CB6C-41EE-84C1-01FB3FE64E5C', 'adfadfadfasdfasdf', NULL, -114.08203125, 40.4469470596, '2015-05-08 16:18:23', '2015-05-08 08:18:23'),
('170a5043-20af-4291-a764-fdf408595842', '79E90B2F-5609-4750-85E7-5E25D0E9158D', 'asdfasdfasdfasdfasdf', NULL, -110.1708984375, 37.718590325588, '2015-05-18 03:20:21', '2015-05-17 19:20:21'),
('1966b4ae-41ea-402f-ed6d-7bfa449386db', '5C3AC3F1-ACEB-4BFE-B0AA-2FD8B92471C1', 'adfadfadsfadsfsdf', NULL, -89.12109375, 37.160316546737, '2015-05-08 15:50:42', '2015-05-08 07:50:42'),
('1ba108d8-99f5-420c-a791-b8f09b38e045', '72E6A1C8-B1C8-48CF-8ED8-6B403C57D9E3', 'adsfadsfasdfasdf', NULL, -84.375, 38.479394673276, '2015-05-08 16:42:18', '2015-05-08 08:42:18'),
('1c39d56c-3e18-44a2-dc02-55c8ab462d7d', '7857B682-64A0-4758-B650-25307798C80D', 'adsfadfadfadfasdf', NULL, -108.720703125, 36.315125147481, '2015-05-08 16:50:50', '2015-05-08 08:50:50'),
('20e1ba5b-5899-42ec-c31d-7f441f619dc6', '897FDA88-53F4-47EB-9C99-85755B307EE6', 'adsfadsfasdfasdfasdf', NULL, -88.06640625, 36.315125147481, '2015-05-08 16:22:47', '2015-05-08 08:22:47'),
('260fd416-589a-4070-cffe-474247c8a002', '6CEEACF8-1AAE-45D7-A013-2CEE00D00B2D', NULL, NULL, -111.2255859375, 37.78808138412, '2015-05-18 03:24:13', '2015-05-17 19:24:13'),
('285047f8-4b30-4945-ea77-890953f236fd', '2E353BE4-311A-4F8B-974B-7391E28AB7DC', 'adsfadsfdfasdfadsf', NULL, -101.07421875, 36.102376448736, '2015-05-08 16:29:21', '2015-05-08 08:29:21'),
('291e2718-1bd7-4e2b-f7fd-f01f16b138d1', '9AED0B3C-B255-4FE6-AC0A-8637FBE7A098', 'asdfadsfasdf', NULL, -95.0537109375, 40.044437584609, '2015-05-14 19:36:24', '2015-05-14 11:36:24'),
('2ad3edf8-f645-479f-ba56-c03cde73406c', '69743A7F-A026-43B7-A82B-2318F91D3431', NULL, NULL, -104.7216796875, 38.479394673276, '2015-05-14 19:39:16', '2015-05-14 11:39:16'),
('2b09399c-d511-4593-9e3a-fcc957e0ef69', '1F7011C9-BC23-482E-ADE2-484B760F2C23', 'asdgasdgasdg', NULL, -89.6484375, 37.300275281344, '2015-05-08 04:16:57', '2015-05-07 20:16:57'),
('2b70c131-ebf7-4406-f3a9-9ba98920a784', 'EF49855E-33E6-43C5-900C-FA5A1252629A', 'adfadsfasdfasdf', NULL, -104.9853515625, 34.885930940753, '2015-05-18 02:33:44', '2015-05-17 18:33:44'),
('2e795b13-54e4-471e-aafd-d00ec60e2239', '1F7011C9-BC23-482E-ADE2-484B760F2C23', 'adfadfadsfasdf', '', -113.115234375, 39.164141047687, '2015-05-08 04:16:57', '2015-05-07 20:16:57'),
('30871aa3-4ae6-4c61-e38d-9ab11ace9e26', '93F1D8FA-3C42-4D02-86F0-8E4A85402BC8', 'adfadsfasdfasdfasdf', '', -111.5771484375, 37.718590325588, '2015-05-18 03:13:40', '2015-05-17 19:13:40'),
('3158ba20-d293-47d7-8204-d932736e314d', 'E93EEC6D-FE9E-4CC4-8E55-43F801AD4130', 'asdfasdfasdfasdf', NULL, -111.4013671875, 37.718590325588, '2015-05-18 03:38:37', '2015-05-17 19:38:37'),
('32c3bfa6-e380-472a-9572-cf3a97794bb5', '6CEEACF8-1AAE-45D7-A013-2CEE00D00B2D', 'asefasefasefasefasefasefasefasef', NULL, -96.4599609375, 34.813803317113, '2015-05-18 03:24:13', '2015-05-17 19:24:13'),
('3c3d8d49-633f-42d1-82e4-ce25a272b399', '2BC56A40-BF5F-417B-B44C-17ECAFD5C6B9', 'asdfasdfasdfasdf', NULL, -87.978515625, 35.960222969297, '2015-05-08 04:26:28', '2015-05-07 20:26:28'),
('3dd98267-3820-4724-d824-f316444337f0', 'E93EEC6D-FE9E-4CC4-8E55-43F801AD4130', 'asdfasdfasdfasdfasdfa', NULL, -99.6240234375, 34.597041516144, '2015-05-18 03:38:37', '2015-05-17 19:38:37'),
('3e146741-f3bc-40a6-c47d-c31ad60681a2', '2BC56A40-BF5F-417B-B44C-17ECAFD5C6B9', 'asdfasdfasdfasdf', '', -105.1171875, 37.439974052271, '2015-05-08 04:26:28', '2015-05-07 20:26:28'),
('40da0f81-ce34-416f-ee6e-6fbbb34a8f9f', '46B22175-8830-4722-9DE5-5F239F2BEC5F', 'adsfasdfasdff', NULL, -82.79296875, 38.754083275791, '2015-05-08 04:16:09', '2015-05-07 20:16:09'),
('43dab0d5-3435-4b60-bf28-6bc9b2d35501', 'E6B098E0-F540-46F3-9E2A-4143A0202AC7', 'asdfasdfasdfasdf', NULL, -108.896484375, 41.310823880918, '2015-06-01 17:34:30', '2015-06-01 09:34:30'),
('456e70db-7427-433e-8214-acb575b298dc', 'B0A72476-EEE6-423B-A925-8F87FBCD8246', 'asdfasdfasdfasdf', '', -108.28125, 42.811521745098, '2015-05-08 04:23:21', '2015-05-07 20:23:21'),
('487b16d0-3996-4d5a-d7c2-75db1bf8632e', '07934B14-6DA4-44DC-A8C6-25BFEE13C0DD', NULL, NULL, -107.0068359375, 39.571822237344, '2015-05-18 03:00:15', '2015-05-17 19:00:15'),
('49d92154-735c-4c8b-a4aa-eeaf216374d2', 'BFDE175A-70EA-4568-BCB1-CCEBC754CBA4', 'adsfadsfadsfasfsdf', NULL, -93.251953125, 38.134556577054, '2015-05-08 16:48:36', '2015-05-08 08:48:36'),
('4f7224fb-6a2d-4e43-aa52-cd37f2c074fb', '444FF584-8FF7-49D1-8BAB-28C4B966CF55', 'adfadsfadsfasdfsadf', '', -113.73046875, 41.442726377672, '2015-05-08 16:47:44', '2015-05-08 08:47:44'),
('583069f0-0648-4856-f404-36ee109db907', '07934B14-6DA4-44DC-A8C6-25BFEE13C0DD', NULL, NULL, -103.7548828125, 35.101934057246, '2015-05-18 03:00:15', '2015-05-17 19:00:15'),
('5d5dd2c5-0a95-4ed1-d12f-dab86d1b111b', 'A7C333F3-6693-4FD6-A560-587BA7AF42A0', 'sdfgsdfgsdfgsdfgdfgsd', '', -105.29296875, 43.96119063892, '2015-05-08 15:53:19', '2015-05-08 07:53:19'),
('5eb79a94-cdf9-406d-9203-ecefa10597c2', 'EF49855E-33E6-43C5-900C-FA5A1252629A', 'adsfadsfasdfadsf', NULL, -88.2861328125, 37.230328387604, '2015-05-18 02:33:44', '2015-05-17 18:33:44'),
('5ed18478-fce4-44d5-88e5-3f6d6b832eb9', 'BD3BFB7E-3BAC-4179-8F6F-4F697A94922C', 'adsfadsfasdgasdgasdga', '', -94.3505859375, 42.358543917497, '2015-05-21 14:17:03', '2015-05-21 06:17:03'),
('5edde56e-4d04-4932-e53d-1cd8f3910360', '07934B14-6DA4-44DC-A8C6-25BFEE13C0DD', 'asdfadsfadsfasdf', NULL, -92.3291015625, 38.134556577054, '2015-05-18 03:00:15', '2015-05-17 19:00:15'),
('5faa2519-c1dc-4973-d4a5-2f4ea5b52054', 'D403221E-6EB3-4CD6-87EA-B3928C9EC8F1', NULL, NULL, -95.6689453125, 43.707593504053, '2015-05-18 02:59:00', '2015-05-17 18:59:00'),
('60405dc1-7775-4c68-b027-2eb42ff7780e', '46B22175-8830-4722-9DE5-5F239F2BEC5F', 'adfadsfadsfasdf', '', -113.291015625, 42.29356419217, '2015-05-08 04:16:09', '2015-05-07 20:16:09'),
('6361f2c4-60b4-4924-8906-4baf73f19118', '7DFC24DF-ABE3-4133-BE60-9D723A392D99', 'asdfasdfasdf', NULL, -88.59375, 39.504040705584, '2015-06-01 18:01:28', '2015-06-01 10:01:28'),
('639baa4d-8e37-42e8-9e87-2b8dbae48ad3', 'F9A77AA7-56F8-41CB-B194-B5A6DDA220CA', 'adsfadfadfadsfadsfa', NULL, -101.162109375, 38.61687046393, '2015-05-08 17:00:36', '2015-05-08 09:00:36'),
('660e1551-bf4e-4ea7-b7ab-effab6c0febd', 'BFDE175A-70EA-4568-BCB1-CCEBC754CBA4', 'adsfadsfasdfasdfsadf', NULL, -107.05078125, 38.685509760012, '2015-05-08 16:48:36', '2015-05-08 08:48:36'),
('68afdc00-7249-4b25-f75d-c97d7d28dd11', '2BC56A40-BF5F-417B-B44C-17ECAFD5C6B9', 'adsfasdfasdf', NULL, -96.50390625, 35.101934057246, '2015-05-08 04:26:28', '2015-05-07 20:26:28'),
('6c2912d4-ba5e-4616-c66f-817998cefc58', 'D403221E-6EB3-4CD6-87EA-B3928C9EC8F1', 'adsfasdfasdfasdf', NULL, -90.9228515625, 38.134556577054, '2015-05-18 02:59:00', '2015-05-17 18:59:00'),
('6f08e0cf-bcc1-446d-85e5-b02989ff0c8a', 'EF49855E-33E6-43C5-900C-FA5A1252629A', 'adfadsfasdfasdf', NULL, -101.3818359375, 37.090239803072, '2015-05-18 02:33:44', '2015-05-17 18:33:44'),
('70c26e6f-7d14-4803-b1bd-b913c6150aa2', '423E5715-B765-4546-9CB4-C6E1B009677F', 'asdfasdfasdfasdf', NULL, -104.150390625, 36.102376448736, '2015-05-08 04:24:29', '2015-05-07 20:24:29'),
('766434fc-9af5-4831-b467-e7e56ad2c686', 'E93EEC6D-FE9E-4CC4-8E55-43F801AD4130', 'adsfasdfasdfasdfasdf', NULL, -87.4951171875, 37.78808138412, '2015-05-18 03:38:37', '2015-05-17 19:38:37'),
('76826d76-6bcd-4f68-ca5b-1aa31ea8de9b', '7DFC24DF-ABE3-4133-BE60-9D723A392D99', 'adsfasdfasdfasdf', NULL, -99.404296875, 36.456636011596, '2015-06-01 18:01:28', '2015-06-01 10:01:28'),
('7ea26940-56ef-4d1b-81b2-cfba8d416dff', 'A7697029-7B46-4A84-B615-7F3C6CB0AEF5', 'asdfasdfsdfasf', NULL, -107.578125, 37.370157184058, '2015-05-08 04:22:03', '2015-05-07 20:22:03'),
('800ed450-46f2-442a-b4a9-ffb95971a4eb', '72E6A1C8-B1C8-48CF-8ED8-6B403C57D9E3', 'adfadsfadsfadsfadsf', '', -112.060546875, 42.617791432823, '2015-05-08 16:42:18', '2015-05-08 08:42:18'),
('8028399c-bb59-4a45-80a0-65b601d664ff', '6610E2D1-3302-4B8F-8BF8-635F4E6341D8', 'dsfsdfasdfasdfasdf', '', -100.283203125, 45.089035564831, '2015-05-08 15:59:40', '2015-05-08 07:59:40'),
('80f8ed6c-5065-4577-b33c-16cc3dda7281', '93F1D8FA-3C42-4D02-86F0-8E4A85402BC8', 'asdfasdfasdfasdfasdf', NULL, -101.2060546875, 34.307143856288, '2015-05-18 03:13:40', '2015-05-17 19:13:40'),
('84728f3c-aed3-41d8-f1bc-e712a88f2b10', '5C3AC3F1-ACEB-4BFE-B0AA-2FD8B92471C1', 'dfsasdfasdfasdfasdfasdf', '', -112.236328125, 41.112468789181, '2015-05-08 15:50:42', '2015-05-08 07:50:42'),
('885f208e-ca80-4943-dbc3-59cde80a0645', 'F85C8173-CB6C-41EE-84C1-01FB3FE64E5C', 'adsfadsfasdfaff', NULL, -92.548828125, 35.603718740697, '2015-05-08 16:18:23', '2015-05-08 08:18:23'),
('89a2b293-aa95-4d50-88be-19bdec839c2e', 'BFDE175A-70EA-4568-BCB1-CCEBC754CBA4', 'adfasdfadsfadsfads', '', -100.8984375, 35.460669951495, '2015-05-08 16:48:36', '2015-05-08 08:48:36'),
('8a0f9cd5-15cf-4d6c-af64-10b2902ff1df', 'F1FD7294-F5E6-4135-BC63-B89E2CD75633', 'adsfasdfasdfasdf', NULL, -101.5576171875, 34.885930940753, '2015-05-21 14:25:21', '2015-05-21 06:25:21'),
('8b0bbccd-6cb2-4b68-c29b-67cda3255627', '07934B14-6DA4-44DC-A8C6-25BFEE13C0DD', NULL, NULL, -100.5029296875, 45.089035564831, '2015-05-18 03:00:15', '2015-05-17 19:00:15'),
('8ba9bf39-3741-4bad-90f5-211728502525', '6CEEACF8-1AAE-45D7-A013-2CEE00D00B2D', 'asefasefasefasefasefsef', NULL, -98.8330078125, 44.024421519659, '2015-05-18 03:24:13', '2015-05-17 19:24:13'),
('8d31d717-90fe-4b25-c102-b0484120e40c', 'E6B098E0-F540-46F3-9E2A-4143A0202AC7', 'adsfasdfasdf', NULL, -91.845703125, 39.36827914916, '2015-06-01 17:34:30', '2015-06-01 09:34:30'),
('8eff3a78-0c3d-4b7c-95b1-3957fdb5b1b9', 'D403221E-6EB3-4CD6-87EA-B3928C9EC8F1', NULL, NULL, -103.9306640625, 38.341656192796, '2015-05-18 02:59:00', '2015-05-17 18:59:00'),
('9069d168-d024-4de7-96d9-d99c87253286', '6E797D2F-C680-43A6-A618-C27623001AD4', 'bggfdgfbg', NULL, -94.5703125, 39.909736234537, '2015-05-08 17:14:36', '2015-05-08 09:14:36'),
('90bbe7f2-8231-4e32-a1f3-74f8a1816e8b', '897FDA88-53F4-47EB-9C99-85755B307EE6', 'dfadsfasdfasdfasdf', '', -95.09765625, 47.576525713746, '2015-05-08 16:22:47', '2015-05-08 08:22:47'),
('91984a91-e4c1-4e5c-b907-06ff17ff80dc', '67EEAB50-563A-4C01-B600-FE4E453CB518', NULL, NULL, -112.7197265625, 41.574361305989, '2015-05-21 13:28:46', '2015-05-21 05:28:46'),
('93ff93f6-5cd2-40c1-eb3e-ee6148eede09', '0F78001A-71D4-4337-B707-B940D721086B', 'fasdfasdfsdaf', NULL, -105.3369140625, 38.272688535981, '2015-05-18 02:58:15', '2015-05-17 18:58:15'),
('94bc53d8-d3c3-4970-8736-e828516819fc', '6E797D2F-C680-43A6-A618-C27623001AD4', NULL, NULL, -109.6875, 36.385912772877, '2015-05-08 17:14:36', '2015-05-08 09:14:36'),
('96f42b00-3f99-4723-eee6-4e2a33c48736', '67EEAB50-563A-4C01-B600-FE4E453CB518', 'asdfasdfasfsadf', '', -104.6337890625, 37.370157184058, '2015-05-21 13:28:46', '2015-05-21 05:28:46'),
('986d070d-3d61-473b-cf57-667275f7ccbe', 'B0A72476-EEE6-423B-A925-8F87FBCD8246', 'adsfasdfasdfasdf', NULL, -98.173828125, 36.668418918948, '2015-05-08 04:23:21', '2015-05-07 20:23:21'),
('9873b1da-9444-4f2f-d4ba-d49d2b13f386', '69743A7F-A026-43B7-A82B-2318F91D3431', 'asdfasdfasdfasdfsadf', '', -103.6669921875, 33.943359946579, '2015-05-14 19:39:16', '2015-05-14 11:39:16'),
('9cd4b490-2c5f-478b-b4d2-e37e50d47ef3', '46B22175-8830-4722-9DE5-5F239F2BEC5F', 'adsfasdfasdfsadf', NULL, -97.822265625, 34.016241889667, '2015-05-08 04:16:09', '2015-05-07 20:16:09'),
('9cfb6aa1-ec5d-4c7e-9a1e-c2ba49a9311f', '93F1D8FA-3C42-4D02-86F0-8E4A85402BC8', 'asdfasdfasdfasdfasdfasdf', NULL, -88.1103515625, 36.59788913307, '2015-05-18 03:13:40', '2015-05-17 19:13:40'),
('9fedd2da-3aa6-4575-d44a-fbb8f6291e40', '2E353BE4-311A-4F8B-974B-7391E28AB7DC', 'adfadsfadfadfadsf', '', -112.060546875, 39.436192999314, '2015-05-08 16:29:21', '2015-05-08 08:29:21'),
('a2363bf2-1e30-46ab-c946-d043340d34a2', 'D6C52A06-2199-4FE0-9B2D-6B93C03D979A', 'asdfasdfasdfasdfasf', '', -104.326171875, 32.916485347314, '2015-06-01 17:35:47', '2015-06-01 09:35:47'),
('a48e2fdf-dbcd-42b4-e603-dfdbb7ac49b9', 'F9A77AA7-56F8-41CB-B194-B5A6DDA220CA', 'dsfdsfasdfasdfasdf', '', -109.599609375, 40.111688665596, '2015-05-08 17:00:36', '2015-05-08 09:00:36'),
('a6e04477-63a9-4e70-8765-41b9ab4ac651', '5394C2CB-FD3B-48BB-8C67-850373BBB84B', 'adsfadsfadsfasdf', '', -106.3916015625, 39.504040705584, '2015-05-14 19:37:52', '2015-05-14 11:37:52'),
('a84208e6-c1b9-4e3a-f7f7-a941a9f3f44d', 'BD3BFB7E-3BAC-4179-8F6F-4F697A94922C', NULL, NULL, -107.7099609375, 38.822590976177, '2015-05-21 14:17:03', '2015-05-21 06:17:03'),
('a8427c4f-0f4d-4d93-e176-7a456a68100c', '423E5715-B765-4546-9CB4-C6E1B009677F', 'asdfadsfasdfasdf', '', -113.994140625, 44.150681159781, '2015-05-08 04:24:29', '2015-05-07 20:24:29'),
('a9ccae3b-2fc6-45c9-8fef-d398b418108e', 'D6C52A06-2199-4FE0-9B2D-6B93C03D979A', 'adfasdfasdfasdf', NULL, -101.513671875, 43.2612061248, '2015-06-01 17:35:47', '2015-06-01 09:35:47'),
('ad742652-185b-4146-a9ba-b21d0a109bd2', '79E90B2F-5609-4750-85E7-5E25D0E9158D', 'asdfasdfasdfasdfasdfasdf', NULL, -101.2939453125, 35.675147436085, '2015-05-18 03:20:21', '2015-05-17 19:20:21'),
('afd70c99-2f31-449b-c6a5-1312767eee84', '8CA2F05A-0788-4BB8-BCFA-E4BE28294FD3', 'dfasdfasdfasdf', NULL, -105.0732421875, 38.065392351332, '2015-05-18 03:28:53', '2015-05-17 19:28:53'),
('b0c1f78f-31a4-4db2-f34c-11858c24702f', '4C6E7985-EF22-4B3A-A134-92B1A0F773E2', 'adsfadsfasdfasdfasdf', NULL, -84.375, 35.532226227703, '2015-05-08 02:58:42', '2015-05-07 18:58:42'),
('b58ef2a4-afc1-4767-ed2b-94012c4012c1', 'EF49855E-33E6-43C5-900C-FA5A1252629A', 'adsfadsfasdfasdfasdf', NULL, -109.3798828125, 39.504040705584, '2015-05-18 02:33:44', '2015-05-17 18:33:44'),
('b7392038-decd-4827-b311-52fb96be7266', 'E6B098E0-F540-46F3-9E2A-4143A0202AC7', 'asdfasdfasdfasdf', NULL, -106.787109375, 37.090239803072, '2015-06-01 17:34:30', '2015-06-01 09:34:30'),
('b8942a92-78fe-4b7b-e637-8880bcbcfbf9', '897FDA88-53F4-47EB-9C99-85755B307EE6', 'asdfasdfadsfasdfdsf', NULL, -108.80859375, 40.380028402512, '2015-05-08 16:22:47', '2015-05-08 08:22:47'),
('ba46cb9d-0765-4966-8a26-9ba5ed2b4d4f', 'A7C333F3-6693-4FD6-A560-587BA7AF42A0', 'adfadsfasdfasdfasdf', NULL, -112.763671875, 35.317366329238, '2015-05-08 15:53:19', '2015-05-08 07:53:19'),
('bbcc0927-cce9-4015-f018-def1d342137f', '1F7011C9-BC23-482E-ADE2-484B760F2C23', 'gasdgadsgdasg', NULL, -99.755859375, 37.439974052271, '2015-05-08 04:16:57', '2015-05-07 20:16:57'),
('c0104f8e-ce9e-4f45-a83a-b67ebbb29074', 'F85C8173-CB6C-41EE-84C1-01FB3FE64E5C', 'adfadsfadsfasfsdf', NULL, -103.271484375, 36.668418918948, '2015-05-08 16:18:23', '2015-05-08 08:18:23'),
('c0f68a14-a5f7-433b-e107-294b6f0a7965', '6E797D2F-C680-43A6-A618-C27623001AD4', 'vfdfgdgfdgfdfgdfg', '', -107.75390625, 43.389081939117, '2015-05-08 17:14:36', '2015-05-08 09:14:36'),
('c17f45aa-7faa-4549-9e18-1f132cac3cdd', 'F85C8173-CB6C-41EE-84C1-01FB3FE64E5C', 'adfadfadsfasdfadf', '', -98.0859375, 44.46515101352, '2015-05-08 16:18:23', '2015-05-08 08:18:23'),
('c183286e-5088-40b8-efe3-475cd50380b8', 'F1FD7294-F5E6-4135-BC63-B89E2CD75633', NULL, NULL, -103.1396484375, 38.134556577054, '2015-05-21 14:25:21', '2015-05-21 06:25:21'),
('c45215b2-f5f7-48f0-af9c-365bbe989dac', '6E797D2F-C680-43A6-A618-C27623001AD4', 'nfhgfnbgfghfgbf', NULL, -97.55859375, 35.746512259919, '2015-05-08 17:14:36', '2015-05-08 09:14:36'),
('c49df30c-851c-49bd-8a76-d93c4fbbd76b', '4C6E7985-EF22-4B3A-A134-92B1A0F773E2', 'asdfasdfasdfasdfsdf', '', -108.720703125, 38.685509760012, '2015-05-08 02:58:42', '2015-05-07 18:58:42'),
('c87531dc-1b30-442c-b68f-c976814b01f0', '0F78001A-71D4-4337-B707-B940D721086B', 'asdfasdfasdfasdfasd', NULL, -94.5263671875, 36.527294814546, '2015-05-18 02:58:15', '2015-05-17 18:58:15'),
('c9c7e640-bfab-4135-da16-84333e432135', '8CA2F05A-0788-4BB8-BCFA-E4BE28294FD3', 'asdfasdfasdfasdf', NULL, -100.9423828125, 37.160316546737, '2015-05-18 03:28:53', '2015-05-17 19:28:53'),
('ca59baca-cbd6-4ecb-de3e-0641f91a4f19', 'A7697029-7B46-4A84-B615-7F3C6CB0AEF5', 'asdfsdfasdfasdf', '', -97.3828125, 46.316584181822, '2015-05-08 04:22:03', '2015-05-07 20:22:03'),
('cc058d95-4a2d-4d31-eb0e-b8ebeb6dcc86', '5394C2CB-FD3B-48BB-8C67-850373BBB84B', NULL, NULL, -91.0107421875, 39.571822237344, '2015-05-14 19:37:52', '2015-05-14 11:37:52'),
('ceb7fd38-1f19-42c5-d108-29ad5adb05b3', 'D6C52A06-2199-4FE0-9B2D-6B93C03D979A', 'adsfasdfasdfasdfasdf', NULL, -87.626953125, 37.090239803072, '2015-06-01 17:35:47', '2015-06-01 09:35:47'),
('cff166ca-5ea5-47a1-958b-d33dcb631cdb', '6610E2D1-3302-4B8F-8BF8-635F4E6341D8', 'dsgagadgagagag', NULL, -95.009765625, 36.385912772877, '2015-05-08 15:59:40', '2015-05-08 07:59:40'),
('d00ff24c-1b32-4926-875b-58831801a11f', 'A7C333F3-6693-4FD6-A560-587BA7AF42A0', 'adfadsfasfasdf', NULL, -95.625, 36.879620605027, '2015-05-08 15:53:19', '2015-05-08 07:53:19'),
('d25a746b-72bf-45b6-c5a6-6f97ced34fb0', 'D403221E-6EB3-4CD6-87EA-B3928C9EC8F1', NULL, NULL, -99.7119140625, 34.452218472827, '2015-05-18 02:59:00', '2015-05-17 18:59:00'),
('d28a8988-f5fd-4cf6-d9df-df060d350fa7', '6610E2D1-3302-4B8F-8BF8-635F4E6341D8', 'adsgasdgasdgdasgasg', NULL, -109.072265625, 36.949891786813, '2015-05-08 15:59:40', '2015-05-08 07:59:40'),
('d5401f98-f0c3-4425-fe2a-9f0bf93b01ea', '5BC0093E-DFAB-4985-BF5D-22FF66F9F88F', 'adsfadsfasdfasdf', NULL, -80.859375, 35.889050079361, '2015-06-01 17:38:02', '2015-06-01 09:38:02'),
('dab74009-7cb1-4ec0-cdb5-cab92397cbf9', '897FDA88-53F4-47EB-9C99-85755B307EE6', 'asdfasdfasdfasdfasdf', NULL, -96.240234375, 35.029996369026, '2015-05-08 16:22:47', '2015-05-08 08:22:47'),
('dab741ff-67b2-48c8-e531-8ec9aa3f71ba', 'F9A77AA7-56F8-41CB-B194-B5A6DDA220CA', 'dsfadsfafadsf', NULL, -87.890625, 36.031331776332, '2015-05-08 17:00:36', '2015-05-08 09:00:36'),
('dcfe8df6-a599-4fc0-c90a-2360f88735e1', '6CEEACF8-1AAE-45D7-A013-2CEE00D00B2D', 'asefaesfasefasefasefasef', NULL, -84.8583984375, 37.509725842938, '2015-05-18 03:24:13', '2015-05-17 19:24:13'),
('dda53b45-2126-43f4-bf05-72197c623c27', '0F78001A-71D4-4337-B707-B940D721086B', 'asdfasdfasdfadsfasdf', NULL, -85.5615234375, 39.232253141715, '2015-05-18 02:58:15', '2015-05-17 18:58:15'),
('e1b607b1-c5bd-453e-d18e-93f86f3baf9d', '4C6E7985-EF22-4B3A-A134-92B1A0F773E2', 'adsfasdfsadfsadfadf', NULL, -98.96484375, 35.675147436085, '2015-05-08 02:58:42', '2015-05-07 18:58:42'),
('e3904602-4762-48e4-9bd4-29b83bc602ab', '7857B682-64A0-4758-B650-25307798C80D', 'sdfadsfasfadsfasdfasdf', NULL, -94.658203125, 36.59788913307, '2015-05-08 16:50:50', '2015-05-08 08:50:50'),
('ee2a05e4-0b3e-41c5-e339-bac073586adc', '444FF584-8FF7-49D1-8BAB-28C4B966CF55', 'dasfadsfadsfsdfdsaf', NULL, -87.802734375, 36.527294814546, '2015-05-08 16:47:44', '2015-05-08 08:47:44'),
('ee91efd9-df53-4578-e2b7-fb0cc8c86df5', '2E353BE4-311A-4F8B-974B-7391E28AB7DC', 'adsfdsfadsfadsfasdf', NULL, -92.197265625, 37.230328387604, '2015-05-08 16:29:21', '2015-05-08 08:29:21'),
('ef2fcc69-59dc-41b1-b754-a5f5dd183c6a', '0F78001A-71D4-4337-B707-B940D721086B', 'asdfasdfasdfasdf', '', -101.2939453125, 44.590467181309, '2015-05-18 02:58:15', '2015-05-17 18:58:15'),
('f0185125-4aee-4bdf-cafc-e900c55ecc4f', '5C3AC3F1-ACEB-4BFE-B0AA-2FD8B92471C1', 'adsfadsfadsfadf', NULL, -103.18359375, 38.822590976177, '2015-05-08 15:50:42', '2015-05-08 07:50:42'),
('f0592f4a-181e-4f93-a332-1f98ae9d21f4', 'F1FD7294-F5E6-4135-BC63-B89E2CD75633', 'adsfadsfasdfasdfdsafasdf', NULL, -87.3193359375, 36.102376448736, '2015-05-21 14:25:21', '2015-05-21 06:25:21'),
('f8f66c57-382b-4ceb-d458-6b9540823994', 'A7697029-7B46-4A84-B615-7F3C6CB0AEF5', 'asdfasdfasdfasdf', NULL, -91.318359375, 36.173356935222, '2015-05-08 04:22:03', '2015-05-07 20:22:03'),
('ff18f925-79b2-4f18-81f6-8d30e99e573c', '9AED0B3C-B255-4FE6-AC0A-8637FBE7A098', 'asdfasdfasdfasdfasdfasdf', '', -112.6318359375, 37.649034021579, '2015-05-14 19:36:24', '2015-05-14 11:36:24');

-- --------------------------------------------------------

--
-- Table structure for table `queryerror`
--

CREATE TABLE IF NOT EXISTS `queryerror` (
`error_id` int(10) unsigned NOT NULL,
  `query` text,
  `file` varchar(1024) DEFAULT '',
  `line` int(10) unsigned DEFAULT NULL,
  `error_string` varchar(1024) DEFAULT '',
  `error_no` int(10) unsigned DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `execution_script` varchar(1024) DEFAULT '',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(16) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `runtimeerror`
--

CREATE TABLE IF NOT EXISTS `runtimeerror` (
`error_id` int(10) unsigned NOT NULL,
  `title` varchar(2048) NOT NULL DEFAULT '',
  `file` varchar(1024) DEFAULT '',
  `line` int(10) unsigned DEFAULT NULL,
  `error_type` int(10) unsigned NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `server_name` varchar(100) DEFAULT NULL,
  `execution_script` varchar(1024) NOT NULL DEFAULT '',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(16) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userinformation`
--

CREATE TABLE IF NOT EXISTS `userinformation` (
`id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `userrole` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `images` longtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userinformation`
--

INSERT INTO `userinformation` (`id`, `username`, `password`, `email`, `userrole`, `fname`, `lname`, `mname`, `gender`, `status`, `images`) VALUES
(24, 'Ryanjericzz', 'ASdAszz', 'rqwqwezxczxc@yahoo.com', 'Admin', 'Ryanjeric', 'Sabado', 'Fernandez', 'MALE', 'ACTIVE', 'IMAGE'),
(43, 'qweqweqweqqewqewq', 'zxczxcxzcxc', 'zxczx@cxczzxczxczxc', 'SubAdmin', 'zxcxzczxc', 'cxzczxcxzc', 'zzxcxzczx', 'MALE', 'ACTIVE', 'IMAGE'),
(46, 'rqwrwqrqwrwqrz', 'wrwqrqwrwzzz', 'rqwrr@qwqwewqez', 'Admin', 'wrqwr', 'asdasdsdas', 'asdasdasd', 'FEMALE', 'ACTIVE', 'IMAGE'),
(48, 'Ryanjericz', 'rrrrrrrrrrrrrrrrrrrr', 'rrrrrrrrrrrrrrrr@wq', 'Admin', 'zzzzz', 'zzzz', 'zzzzzzzz', 'MALE', 'ACTIVE', 'IMAGE'),
(49, 'qwrqwqwrqwqw', 'rqwqwqwqwrqqw', 'qwrqwr@qwewqeqeqwe', 'ADMIN', 'qwewqeqweqeqw', 'qweeqweqe', 'eqweqewqeqe', 'MALE', 'ACTIVE', 'IMAGE'),
(50, 'zxczxccxc', 'xczcxzxczxczxzxc', 'zxczxc@asczxczxczxc', 'ADMIN', 'zczxczxc', 'zczczxczcx', 'ccxczxczxc', 'MALE', 'ACTIVE', 'IMAGE'),
(51, 'rqwrwqrqwrqwr', 'asdasdasdasd', 'qwrqwrqwrqwr@qweqwe', 'ADMIN', 'zxczxcxc', 'zxczxcxc', 'xcxcxczxc', 'MALE', 'ACTIVE', 'IMAGE');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(250) NOT NULL,
  `source_info` varchar(100) DEFAULT NULL,
  `referal_code` varchar(20) DEFAULT NULL,
  `subscribe_newsletter` varchar(3) DEFAULT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `birthday` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `country` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `profile_pic_name` varchar(250) DEFAULT NULL,
  `activation_code` varchar(150) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `source_info`, `referal_code`, `subscribe_newsletter`, `first_name`, `last_name`, `birthday`, `gender`, `country`, `state`, `profile_pic_name`, `activation_code`, `status`, `created_at`, `updated_at`) VALUES
('03686d5c-bd04-45a4-d8ff-5f7a00ee23df', 'superagent', 'efrenbautistajr@geeksnest.com', '7c222fb2927d828af22f592134e8932480637c0d', 'new', NULL, '1', 'efren', 'bautista', '1986-02-15', 'male', 'Philippines', 'Here', NULL, NULL, 1, '2015-04-20 16:09:57', '2015-05-13 06:47:41'),
('C071A47D-9D98-4DB6-ACE4-00A5BDE55AFC', 'ebautistaj', 'efrenbautistajr@gmail.com', '13ec182672f07a51b64d88483486189000cc4d87', 'news', NULL, '1', 'efren', 'bautista', '1999-03-15', 'male', 'Angola', 'Bie', NULL, '', 1, '2015-04-20 16:09:57', '2015-04-20 08:09:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api`
--
ALTER TABLE `api`
 ADD PRIMARY KEY (`client_id`), ADD UNIQUE KEY `private_key` (`private_key`), ADD UNIQUE KEY `public_id` (`public_id`);

--
-- Indexes for table `maps`
--
ALTER TABLE `maps`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `markerpics`
--
ALTER TABLE `markerpics`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `markers`
--
ALTER TABLE `markers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `queryerror`
--
ALTER TABLE `queryerror`
 ADD PRIMARY KEY (`error_id`);

--
-- Indexes for table `runtimeerror`
--
ALTER TABLE `runtimeerror`
 ADD PRIMARY KEY (`error_id`);

--
-- Indexes for table `userinformation`
--
ALTER TABLE `userinformation`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api`
--
ALTER TABLE `api`
MODIFY `client_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `queryerror`
--
ALTER TABLE `queryerror`
MODIFY `error_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `runtimeerror`
--
ALTER TABLE `runtimeerror`
MODIFY `error_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userinformation`
--
ALTER TABLE `userinformation`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
